from datetime import timedelta

import factory
from django.utils import timezone

import elections.models


class ElectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = elections.models.Election

    for_team = factory.SubFactory('person.factories.TeamFactory')
    constituents = factory.SubFactory('person.factories.TeamFactory')
    called_by = factory.SubFactory('person.factories.UserFactory')
    invite_from = (timezone.now() + timedelta(days=1)).date()
    accept_from = (timezone.now() + timedelta(days=2)).date()
    voting_from = (timezone.now() + timedelta(days=3)).date()
    finish_on = timezone.now() + timedelta(days=10)


class CandidateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = elections.models.Candidate

    election = factory.SubFactory(ElectionFactory)
    invitor = factory.SubFactory('person.factories.UserFactory')
    user = factory.SubFactory('person.factories.UserFactory')
