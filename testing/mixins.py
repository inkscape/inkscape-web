#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Mixins for tests."""
from forums.factories import UserFlagFactory
from person.factories import UserFactory
from person.models import User
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from django.urls import reverse

from .utils import setup_view


AUTH_FIXTURE = 'person/fixtures/test-auth'


class GetViewMixin:
    """A mixin that adds the ability to instantiate a view for unit testing its methods."""
    url_kwargs = {}

    def get_view(self, view, user=None, get_params=None, post_params=None):
        user = user or AnonymousUser()
        get_params = get_params or {}
        post_params = post_params or {}
        factory = RequestFactory()
        request = factory.get(self.view_url)
        request.user = user
        # 'all' satisfies ForumMixin
        request.GET = {**get_params, 'all': True}
        request.POST = post_params
        return setup_view(view(), request, **self.url_kwargs)


class GetTestUsersMixin:
    """A mixin that provides methods to get test users."""

    # Prepend the test auth fixture if it's not already present
    @classmethod
    def setupClass(cls):
        if hasattr(cls, 'fixtures') and AUTH_FIXTURE not in cls.fixtures:
            cls.fixtures = [AUTH_FIXTURE] + cls.fixtures
        super().setUpClass()

    @staticmethod
    def get_test_user():
        """Return a test user."""
        return UserFactory()

    @staticmethod
    def get_test_moderator():
        """Return a test moderator"""
        moderator = UserFactory(username="moderator", permissions=["can_moderate",])
        UserFlagFactory(user=moderator, moderator=True)
        return moderator


class TestUserLoginMixin(GetTestUsersMixin):
    """A mixin that logs in a test user for tests."""

    def setUp(self):
        super().setUp()
        self.user = self.get_test_user()
        self.client.force_login(self.user)


class ModeratorLoginMixin(GetTestUsersMixin):
    """A mixin that logs in the moderator for tests."""

    def setUp(self):
        super().setUp()
        self.moderator = self.get_test_moderator()
        self.client.force_login(self.moderator)


class TestTemplateUsedMixin:
    """A mixin that adds the ability to check that a view uses the correct template."""

    template_name = None

    def test_view_uses_correct_template(self):
        url = reverse(self.url_name, kwargs=self.url_kwargs)
        response = self.client.get(url)
        self.assertTemplateUsed(response, self.template_name)


class CheckFieldsExistMixin:
    """Check that the fields in a form are present."""

    field_names = [] 

    # No need to pass form data while checking that fields exist
    def test_form_has_fields(self):
        form = self.form(**self.form_args)
        for field in self.field_names:
            with self.subTest(field=field):
                self.assertNotEqual(form.fields[field], None)

