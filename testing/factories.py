import alerts.models
import factory


class AlertTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = alerts.models.AlertType

    default_email = True


class AlertSubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = alerts.models.AlertSubscription

    alert = factory.SubFactory(AlertTypeFactory)
    user = factory.SubFactory('person.factories.UserFactory')
