#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Collection of base classes to use in tests."""

from django.test import TestCase
from django.urls import reverse

from .mixins import GetViewMixin, TestTemplateUsedMixin, CheckFieldsExistMixin

from collections import namedtuple

# Set default case as '' (Empty string) when on >= Python 3.7
# Also set default expected as '' for tests that do things other
# than modifying strings
TestData = namedtuple("TestData", ["input", "expected", "case"])

# Set default help_text as '' (Empty string) when on >= Python 3.7
ModelFieldTestData = namedtuple(
    "ModelFieldTestData", ["field", "label", "help_text"])


class BaseViewTestCase(TestCase, GetViewMixin):
    """This class contains tests a view should check for."""
    fixtures = ['categories', 'licenses', 'forums', 'quota']

    view_url = None
    url_name = None
    url_kwargs = {}

    def setUp(self):
        super().setUp()
        self.view_url = reverse(self.url_name, kwargs=self.url_kwargs)

    def assertQuerysetEqual(self, qs, values, **kwargs):
        """A tiny wrapper that sets transform so that we don't have to set it on every call."""
        super().assertQuerysetEqual(qs, values, transform=lambda x: x, **kwargs)

    def test_view_exists_at_desired_location(self):
        response = self.client.get(self.view_url)
        self.assertEqual(response.status_code, 200)

    def test_view_accessible_by_url_name(self):
        url = reverse(self.url_name, kwargs=self.url_kwargs)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class ViewTestCase(BaseViewTestCase, TestTemplateUsedMixin):
    """The most common test case for views."""


class FormTestCase(TestCase, CheckFieldsExistMixin):
    """A test case for testing forms with some useful assertions."""

    form = None
    form_args = {}
    form_data = {}

    def assertOnlyThisFieldError(self, form, field_name, error_msg):
        """Assert that only a single field threw the error with the correct error message."""
        # Check that the error was because of this field
        self.assertIn(field_name, form.errors.keys())

        # Check that the error message is correct
        self.assertEqual(
            form.errors[field_name], [error_msg])

        # Also check that this is the only error
        self.assertEqual(len(form.errors), 1)


class ModelTestCase(TestCase):
    """A test case for testing models."""

    field_data = []

    def assertQuerysetEqual(self, qs, values, **kwargs):
        """A tiny wrapper that sets transform so that we don't have to set it on every call."""
        super().assertQuerysetEqual(qs, values, transform=lambda x: x, **kwargs)

    def get_model_instance(self):
        return self.instance

    def test_model_labels(self):
        """Test that the labels of fields are correct. Does not check whether the help text is correct or not."""
        instance = self.get_model_instance()
        for field in self.field_data:
            field_name = field.field
            expected_field_label = field.label

            with self.subTest(field=field):
                field_label = instance._meta.get_field(
                    field_name).verbose_name
                self.assertEqual(field_label, expected_field_label)
