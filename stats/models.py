#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Site statistics records
"""

from django.utils.translation import gettext_lazy as _
from django.db import transaction
from django.db.models import (
    Model, Manager, DateField, CharField, IntegerField, BigIntegerField
)
from inkscape.templatetags.i18n_fields import OTHER_LANGS
from .fields import MonthField
from .countries import COUNTRY_LIST

GLOBAL_CHOICES = (
    ('unreal', _('Redirects')),
    ('robots', _('Search Bots')),
    ('errors', _('Errors')),
    ('cached', _('Cached Media')),
    ('direct', _('Direct Media')),
    ('others', _('Other Content')),
)

class GlobalAccess(Model):
    """A count of server access (nginx), """
    cadence = DateField()
    category = CharField(max_length=6, choices=GLOBAL_CHOICES, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'category')

    def __str__(self):
        label = self.get_category_display()
        return f"{self.cadence} {label}: C:{self.count} S:{self.size_avg}"


class LanguageAccess(Model):
    """A count of each language accessed (per week)"""
    cadence = MonthField()
    language = CharField(max_length=8, choices=OTHER_LANGS, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'language')

    def __str__(self):
        label = self.get_language_display()
        return f"{self.cadence} {label}: C:{self.count}"


class CountryAccess(Model):
    """A count of each country accessed from (per week)"""
    cadence = MonthField()
    country = CharField(max_length=2, choices=COUNTRY_LIST, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'country')

    def __str__(self):
        label = self.get_country_display()
        return f"{self.cadence} {label}: C:{self.count}"

class SystemAccess(Model):
    """A count of systems accessing the website"""
    cadence = MonthField()
    os = CharField(max_length=48, db_index=True)
    version = CharField(max_length=16, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'os', 'version')

    def __str__(self):
        return f"{self.cadence} {self.os} {self.version}: C:{self.count}"


class BrowserAccess(Model):
    """A count of browsers accessing the website"""
    cadence = MonthField()
    browser = CharField(max_length=48, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'browser')

    def __str__(self):
        return f"{self.cadence} {self.browser}: C:{self.count}"


APP_CHOICES = (
    ('resources', _('Galleries')),
    ('releases', _('Releases')),
    ('forums', _('Forums')),
    ('person', _('Users and Teams')),
    ('news', _('News')),
    ('admin', _('Admin')),
    ('cmsdoc', _('Content')),
)

class ApplicationAccess(Model):
    """The Application of a page accessed"""
    cadence = DateField()
    application = CharField(max_length=16, choices=APP_CHOICES, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'application')

    def __str__(self):
        label = self.get_application_display()
        return f"{self.cadence} {label}: C:{self.count}"

class ReleaseAccess(Model):
    """Which releases are being downloaded"""
    cadence = MonthField()
    release = CharField(max_length=32, db_index=True)
    os = CharField(max_length=32, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'release', 'os')

    def __str__(self):
        return f"{self.cadence} {self.release} {self.os}: C:{self.count}"

class PageAccess(Model):
    """Content and Docs only access, monthly"""
    cadence = MonthField()
    page = CharField(max_length=255, db_index=True)
    count = IntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'page')

    def __str__(self):
        return f"{self.cadence} {self.page}: C:{self.count}"

MEDIA_ROUTES = (
    ('D', _('Downloads')),
    ('M', _('Media')),
    ('S', _('Static')),
)

class CachingAccess(Model):
    """Special caching information graph"""
    cadence = DateField()
    route = CharField(max_length=1, choices=MEDIA_ROUTES, db_index=True)
    download = CharField(max_length=255, db_index=True)
    network = CharField(max_length=22, db_index=True)

    partial_count = IntegerField(default=0)
    complete_count = IntegerField(default=0)
    size = BigIntegerField(default=0)

    class Meta:
        unique_together = ('cadence', 'route', 'download', 'network')

    def __str__(self):
        return f"{self.cadence} {self.route}/{self.download}<<{self.network}: "\
               f"C:{self.partial_count}+{self.complete_count} S:{self.size}"

