#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Methods for getting website statistics
"""

from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from stats.utils import get_stats
from stats.base import DateSumBase, CategorySumBase, WorldMapBase
from stats.countries import COUNTRIES, LANGUAGES
from stats.models import (
    GlobalAccess, LanguageAccess, CountryAccess, SystemAccess, BrowserAccess,
    ApplicationAccess, ReleaseAccess, PageAccess, CachingAccess,
)

class BarMixin(object):
    chart_type = 'bar'
    chart_options = {
        'stackBars': True,
    }

class GlobalWebsiteGraph(BarMixin, DateSumBase):
    """Count new users posting to the forum"""
    title = _('Global Access')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['category']

    def get_data(self):
        return GlobalAccess.objects.all()

class LanguageGraph(BarMixin, CategorySumBase):
    title = _('Languages')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['language']
    category_label = _('Hits')
    minimum_cadence = 'month'

    def get_data(self):
        return LanguageAccess.objects.all()

    def get_category_label(self, cat):
        return LANGUAGES.get(cat, cat)

class CountryGraph(WorldMapBase):
    title = _('Countries')

    def get_data(self):
        return CountryAccess.objects.all()

    def get_category_label(self, cat):
        return COUNTRIES.get(cat, cat)

class SystemGraph(BarMixin, DateSumBase):
    title = _('Operating System')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['os']
    minimum_cadence = 'month'

    def get_data(self):
        return SystemAccess.objects.all()

class BrowserGraph(BarMixin, DateSumBase):
    title = _('Browsers')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['browser']
    minimum_cadence = 'month'

    def get_data(self):
        return BrowserAccess.objects.all()

class ReleaseOsGraph(BarMixin, DateSumBase):
    title = _('Release Downloads by Operating System')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['os']
    minimum_cadence = 'month'

    def get_data(self):
        return ReleaseAccess.objects.all()

class ReleaseVerGraph(BarMixin, DateSumBase):
    title = _('Release Downloads by Version')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['release']
    minimum_cadence = 'month'

    def get_data(self):
        return ReleaseAccess.objects.all()

class ApplicationGraph(BarMixin, DateSumBase):
    title = _('Website Area')
    date_field = 'cadence'
    count_field = 'count'
    category_fields = ['application']

    def get_data(self):
        return ApplicationAccess.objects.all()

class CachingGraph(DateSumBase):
    title = _('Fastly Caching')
    date_field = 'cadence'
    count_field = 'size'
    category_fields = ['route']
    chart_options = {
        'units': 'fileSize',
    }

    def get_data(self):
        return CachingAccess.objects.all()

