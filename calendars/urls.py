# -*- coding: utf-8 -*-
#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Urls for Calendars"""

from django.urls import re_path
from inkscape.url_utils import url_tree
from person import user_urls, team_urls

from .views import EventList, TeamEventFeed, TeamEvent, TeamEventList,\
                   TeamEventOccurance, MeetingControl,\
                   EditAgendum, CompleteAgendum, DeferAgendum

app_name = 'calendars'

team_urls.urlpatterns += [
    re_path(r'^calendar.ics$', TeamEventFeed(), name='team_calendar_feed'),
    re_path(r'^calendar/$', TeamEventList.as_view(), name='team_calendar'),
]

urlpatterns = [
    re_path(r'^$', EventList.as_view(), name='full'),
    re_path(r'^inkscape.ics$', TeamEventFeed(), name='full_feed'),
    re_path(r'^event/(?P<pk>\d+)/$', TeamEvent.as_view(), name='event'),
    url_tree(r'^event/(?P<event_id>\d+)/',
        re_path(r'^(?P<pk>\d+)/$', TeamEventOccurance.as_view(), name='occurance'),
        re_path(r'^(?P<eoc>\d+)/(?P<pk>\d+)/edit/$', EditAgendum.as_view(), name='edit_agendum'),
        re_path(r'^(?P<eoc>\d+)/(?P<pk>\d+)/complete/$', CompleteAgendum.as_view(), name='complete_agendum'),
        re_path(r'^(?P<eoc>\d+)/(?P<pk>\d+)/defer/$', DeferAgendum.as_view(), name='defer_agendum'),
        re_path(r'^(?P<pk>\d+)/(?P<action>[^\/]+)/$', MeetingControl.as_view(), name='meeting'),
    ),
]
