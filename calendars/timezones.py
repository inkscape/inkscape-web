#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Timezone support utils
"""

from collections import defaultdict
from dateutil.tz import gettz
from dateutil.zoneinfo import get_zonefile_instance

ALL_ZONES = list(get_zonefile_instance().zones)
DST_CHOICES = [('UTC', 'UTC (No DST)')]
DST_UNIQUE = defaultdict(list)

def is_numeric(abbr):
    abbr = abbr.replace('-', '').replace('+', '') 
    return abbr.isnumeric()

# Make the choices a unique list using the smallest name
for zone in ALL_ZONES:
    tz = gettz(zone)
    (std, dst) = tz._ttinfo_std, tz._ttinfo_dst
    if not std or not dst:
        continue
    if is_numeric(std.abbr) or is_numeric(dst.abbr):
        continue
    DST_UNIQUE[(std.abbr, std.offset, dst.abbr, dst.offset)].append(zone)


#_ttinfo(offset=-14400, delta=datetime.timedelta(-1, 72000), isdst=1, abbr='EDT', isstd=False, isgmt=False, dstoffset=datetime.timedelta(0, 3600))
def get_sort(key):
    std_abbr, std_offset, dst_abbr, dst_offset = key
    zone = DST_UNIQUE[key]
    return std_offset

def get_label(offset):
    hour = abs(int(offset / 60 / 60))
    minute = abs(int(offset / 60 % 60))
    sign = "-" if offset < 0 else "+"
    return f"{sign}{hour:d}:{minute:02d}"


for paz in (True, False):
    for key in sorted(DST_UNIQUE.keys(), key=get_sort):
        std_abbr, std_offset, dst_abbr, dst_offset = key
        for zone in sorted(DST_UNIQUE[key]):
            if (paz and std_abbr == zone) or (not paz and "/" in zone):
                DST_CHOICES.append((zone, f"{get_label(std_offset)} - {zone} - {std_abbr}/{dst_abbr}"))

