"""
This monkey patch adds VALARM support to django_ical

Until this functionality makes it upstream.
"""

from datetime import timedelta

from icalendar import Event, Alarm
from django_ical import feedgenerator, views

feedgenerator.ITEM_ELEMENT_FIELD_MAP += (
    ('alarm', 'alarm'),
)
views.ICAL_EXTRA_FIELDS.append('alarm')

class PatchedEvent(Event):
    def add(self, vfield, val):
        #print(f"add: {vfield}={val}")
        if vfield == 'alarm':
            self.add_alarm(val)
        else:
            super().add(vfield, val)

    def add_alarm(self, val):
        alarm = Alarm()
        alarm.add('trigger', timedelta(seconds=(val * 3600)))
        alarm.add('action', 'DISPLAY')
        alarm.add('description', 'Reminder')
        self.add_component(alarm)

feedgenerator.Event = PatchedEvent
