#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=too-many-ancestors
"""
Basic budget stats for the project
"""

from django.views.generic import TemplateView, DetailView

from person.mixins import UserRequired
from .stats import get_stats
from .models import Budget

class BudgetView(DetailView):
    model = Budget

    def get_parent(self):
        return self.get_object().team

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['table'] = self.get_table(data['object'])
        data['stats'] = get_stats(self.request, 'budget', budget=data['object'])
        return data

    #def is_allowed(self, user):
    #    return user.is_staff

    @staticmethod
    def get_table(obj):
        """Returns the last ten entries"""
        ret = []
        years = {}
        cats = list(obj.categories.all())
        ignored = set(cats)
        cats += ['Total']
        for entry in obj.entries.order_by("-point")[:9]:
            ret.append({'entry': entry, 'data': ['-'] * len(cats)})

            if entry.point.year not in years:
                years[entry.point.year] = {
                    'value': entry.point.year,
                    'count': 0
                }
            ret[-1]['year'] = years[entry.point.year]
            ret[-1]['year']['count'] += 1

            for datum in entry.data.all():
                val = datum.value
                if val == 0:
                    continue
                if datum.category.is_previous:
                    continue
                if not datum.category.is_income:
                    val = -val

                ret[-1]['data'][cats.index(datum.category)] = val
                ignored.discard(datum.category)
            ret[-1]['data'][-1] = entry.get_total()

        # Remove empty categories
        for i in sorted([cats.index(cat) for cat in ignored], reverse=True):
            cats.pop(i)
            for row in ret:
                row['data'].pop(i)

        return {'rows': ret, 'cols': cats}

