#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for admin of releases app."""
from unittest import mock, skip

from django.contrib.admin.sites import AdminSite
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory, TestCase
from djangocms_text_ckeditor.widgets import TextEditorWidget

from resources.factories import ResourceFactory, CategoryFactory
from resources.models import Category, Resource
from testing.base import TestData
from testing.mixins import GetTestUsersMixin
from .factories import PlatformFactory, ReleaseFactory, ReleasePlatformFactory

from ..admin import PlatformAdmin, ReleaseAdmin, ReleasePlatformAdmin
from ..forms import (PlatformTranslationForm, ReleasePlatformTranslationForm,
                     TranslationForm)
from ..models import Platform, Release, ReleasePlatform

# Since forms and the admin are closely intertwined for releases app,
# the tests here also unit test the forms.


class AdminFormTestMixin:
    """A mixin with useful utilites that can be used for testing
    admin forms."""

    @property
    def model_admin(self):
        raise NotImplementedError("A ModelAdmin instance is required.")

    def generate_request(self, data=None):
        """"Generate a dummy request with just enough data to pass
        form validations."""
        data = data or {}
        request = RequestFactory().post('/', data)
        return request

    def get_admin_form(self, request, instance=None):
        """Get a bound admin form instance."""
        # Using a normal ModelForm won't suffice here as the admin
        # has its own way of creating ModelForms. So, we instead
        # use the form that is internally used by the admin.
        _form = self.model_admin.get_form(request)
        form = _form(request.POST, instance=instance)
        return form


class TestReleaseForm(AdminFormTestMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.model_admin = ReleaseAdmin(
            model=Release, admin_site=AdminSite()
        )

    @classmethod
    def setUpTestData(cls):
        cls.release = ReleaseFactory(version='1.0')
        cls.form_data = {
            'version': '1.0',
            'release_notes': cls.release.release_notes,
        }

    def test_init_sets_release_notes_widget_to_ckeditor(self):
        request = self.generate_request(self.form_data)
        form = self.get_admin_form(request, self.release)
        self.assertIsInstance(
            form.fields['release_notes'].widget, TextEditorWidget)

    @skip("This test exercises a method (ReleaseForm.parent_queryset) that is used only in this test")
    def test_parent_queryset_for_existing_object(self):
        request = self.generate_request(self.form_data)
        form = self.get_admin_form(request, self.release)
        returned = form.parent_queryset(Release.objects.all())
        expected = [
            Release.objects.get(version=version)
            for version in ['1.1', '1.0.x', '0.91', '0.48']
        ]
        self.assertSequenceEqual(returned, expected)

    def test_save_for_existing_object(self):
        request = self.generate_request(self.form_data)
        release = self.release

        # Here we try to check that the local_content method is
        # indeed called and called with correct args when the
        # form is saved. We don't want to actually run local_content
        # so we mock it out.
        mocked_func = mock.Mock(return_value=release.release_notes)
        with mock.patch('releases.forms.local_content', new=mocked_func):
            form = self.get_admin_form(request, release)
            form.is_valid()
            form.save()

        mocked_func.assert_called_once_with(
            release.release_notes, release=release, uploader=None)


class TestReleasePlatformForm(AdminFormTestMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.model_admin = ReleasePlatformAdmin(
            model=ReleasePlatform, admin_site=AdminSite())

    @classmethod
    def setUpTestData(cls):
        cls.relp = ReleasePlatformFactory(platform__codename='gnulinux/appimage',
                                          release__project__slug='inkscape',
                                          release__version='1.0')

    def test_init_sets_info_widget_to_ckeditor(self):
        request = self.generate_request()
        form = self.get_admin_form(request, self.relp)
        self.assertIsInstance(
            form.fields['info'].widget, TextEditorWidget)

    def test_init_filters_resources_with_inkscape_package_category(self):
        # The test release platform does not have a resource in the fixtures
        # So, we create a dummy resource with a dummy file and we set the
        # category to an Inkscape package.
        resource = ResourceFactory(
            name='Resource 1', user=GetTestUsersMixin.get_test_user(),
            download=SimpleUploadedFile(name='1.0 Appimage', content=b'okkkk'),
            category=CategoryFactory(slug='inkscape-package')
        )

        request = self.generate_request()
        form = self.get_admin_form(request, self.relp)

        # There is no other resource with the category of Inkscape package.
        # So, we expect only the resource we just created to show up in the
        # queryset.
        self.assertEqual(list(form.fields['resource'].queryset), [resource])


class TestTranslationForm(TestCase):

    # Since this form is used only in an inlineformset_factory,
    # instantiating it for tests would require using modelform_factory
    # which would be ugly. The test here is the bare minimum which
    # can be tested without instantiating the form.

    def test_modelform_meta_has_correct_fields(self):
        self.assertEqual(
            TranslationForm.Meta.fields,
            ('language', 'release_notes')
        )


class TestPlatformForm(AdminFormTestMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.model_admin = PlatformAdmin(
            model=Platform, admin_site=AdminSite())

    @classmethod
    def setUpTestData(cls):
        cls.platform = PlatformFactory(codename='gnulinux')

    def test_modelform_meta_has_correct_exclude(self):
        request = self.generate_request()
        form = self.get_admin_form(request, self.platform)
        self.assertEqual(form._meta.exclude, ['codename'])

    def test_init_sets_instructions_widget_to_ckeditor(self):
        request = self.generate_request()
        form = self.get_admin_form(request, self.platform)
        self.assertIsInstance(
            form.fields['instruct'].widget, TextEditorWidget)

    parent_test_data = \
        TestData(
            input='gnulinux',
            expected=[
                'windows/32-bit/exe', 'windows/64-bit/exe',
                'windows/32-bit/binary-zip', 'windows/32-bit/msi',
                'windows/64-bit/msi', 'windows', 'windows/32-bit/portable-app',
                'mac-os-x', 'windows/32-bit/compressed-7z', 'source',
                'source/archive/xz', 'windows/64-bit',
                'windows/64-bit/compressed-7z',
                'mac-os-x/1010-1015', 'mac-os-x/104',
                'mac-os-x/104/dmg-ppc', 'mac-os-x/105',
                'mac-os-x/105-106', 'mac-os-x/105/dmg-universal',
                'mac-os-x/106-1010', 'mac-os-x/106-1010/dmg',
                'mac-os-x/107-1010', 'mac-os-x/107-1010/dmg',
                'mac-os-x/version-unknown',
                'mac-os-x/version-unknown/alpha-tarball', 'source/archive',
                'source/archive/bz2', 'source/archive/gz',
                'source/archive/zip', 'source/version-control',
                'source/version-control/git', 'windows/32-bit',
                'windows/32-bit/binary-debug-files-7z',
                'windows/64-bit/binary-debug-files-7z', 'windows/windows-store'
            ],
            case='Parent platform choices for GNU/Linux platform.'
        )

    @skip("This test exercises a method (PlatformForm.parent_queryset) that is used only in this test")
    def test_parent_queryset_for_existing_object(self):
        platform = Platform.objects.get(codename=self.parent_test_data.input)
        request = self.generate_request()
        form = self.get_admin_form(request, platform)
        returned = form.parent_queryset(Platform.objects.all())

        # Check that the choices presented for the parent platform
        # are correct (Choices should not contain the platform itself,
        # neither should they include descendants of the platform).
        #
        # The order does not matter for this particular test since
        # the platforms are presented as choices.
        self.assertQuerysetEqual(
            returned, self.parent_test_data.expected,
            transform=lambda platform: platform.codename,
            ordered=False
        )


class TestPlatformTranslationForm(TestCase):

    # Since this form is used only in an inlineformset_factory,
    # instantiating it for tests would require using modelform_factory
    # which would be ugly. The test here is the bare minimum which
    # can be tested without instantiating the form.

    def test_modelform_meta_has_correct_fields(self):
        self.assertEqual(
            PlatformTranslationForm.Meta.fields,
            ('language', 'name', 'desc', 'instruct')
        )


class TestReleasePlatformTranslationForm(TestCase):

    # Since this form is used only in an inlineformset_factory,
    # instantiating it for tests would require using modelform_factory
    # which would be ugly. The test here is the bare minimum which
    # can be tested without instantiating the form.

    def test_modelform_meta_has_correct_fields(self):
        self.assertEqual(
            ReleasePlatformTranslationForm.Meta.fields,
            ('language', 'howto', 'info')
        )
