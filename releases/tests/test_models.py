#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for models of releases app."""
import factory
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from resources.models import Resource
from testing import base
from testing.base import ModelFieldTestData, TestData
from testing.mixins import GetTestUsersMixin
from .factories import ReleaseFactory, ReleasePlatformFactory, PlatformFactory, DownloadMirrorFactory, \
    build_test_platforms, PlatformTranslationFactory, ReleaseTranslationFactory, ReleasePlatformTranslationFactory

from ..models import (DownloadMirror, Platform, PlatformTranslation, Project,
                      Release, ReleaseFile, ReleasePlatform,
                      ReleasePlatformTranslation, ReleaseStatus,
                      ReleaseTranslation)


class TestReleaseStatus(base.ModelTestCase):

    field_data = [
        ModelFieldTestData(field='name', label='name', help_text=''),
        ModelFieldTestData(field='desc', label='Description', help_text=''),
        ModelFieldTestData(field='style', label='style', help_text=''),
        ModelFieldTestData(field='icon', label='icon', help_text=''),
    ]

    def setUp(self):
        self.instance = self.status = ReleaseStatus.objects.create(name='new')

    def test_model_has_correct_styles(self):
        styles_data = (('blue', 'Blue'),)
        self.assertEqual(ReleaseStatus.STYLES, styles_data)

    def test_model_meta_has_correct_verbose_name(self):
        self.assertEqual(self.status._meta.verbose_name_plural,
                         'Release Statuses')

    def test_object_is_name_field(self):
        self.assertEqual(str(self.status), self.status.name)


class TestProject(base.ModelTestCase):

    field_data = [
        ModelFieldTestData(field='slug', label='slug', help_text=''),
        ModelFieldTestData(field='name', label='name', help_text=''),
        ModelFieldTestData(field='default', label='default', help_text=''),
    ]

    def setUp(self):
        self.instance = self.project = Project.objects.create(slug='inkscape')

    def test_object_is_name_field(self):
        self.assertEqual(str(self.project), self.project.name)

    def test_get_absolute_url(self):
        expected_url = f'{reverse("releases:download")}?project={self.project.slug}'
        self.assertEqual(self.project.get_absolute_url(), expected_url)


class TestRelease(base.ModelTestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ReleaseFactory(version="1.1")

    field_data = [
        ModelFieldTestData(field='project', label='project', help_text=''),
        ModelFieldTestData(field='parent', label='parent', help_text=''),
        ModelFieldTestData(field='version', label='Version', help_text=''),
        ModelFieldTestData(
            field='version_name',
            label='Version Name',
            help_text='If set, uses this string for the version in the display.'),
        ModelFieldTestData(
            field='is_prerelease',
            label='is Pre-Release',
            help_text="If set, will indicate that this is a testing "
                      "pre-release and should not be given to users."),
        ModelFieldTestData(
            field='is_draft',
            label='is draft',
            help_text="Set to true if this release should not be visible at all in the front end."),
        ModelFieldTestData(field='html_desc',
                           label='HTML Description', help_text=''),
        ModelFieldTestData(
            field='keywords', label='HTML Keywords', help_text=''),
        ModelFieldTestData(field='release_notes',
                           label='Release notes', help_text=''),
        ModelFieldTestData(
            field='release_date',
            label='Release date',
            help_text="ONLY set this when THIS release is ready to go. Set "
                      "pre-release dates on pre-releases and remember, as s"
                      "oon as this is released, it will take over the defau"
                      "lt redirection and users will start downloading this"
                      " release."),
        ModelFieldTestData(
            field='status',
            label='status',
            help_text="When release isn't finalised, document if we are in f"
            "reezing, etc, useful for development."),
        ModelFieldTestData(field='edited', label='Last edited', help_text=''),
        ModelFieldTestData(
            field='created', label='Date created', help_text=''),
        ModelFieldTestData(field='background',
                           label='background', help_text=''),
        ModelFieldTestData(
            field='manager',
            label='Manager',
            help_text="Looks after the release schedule and release meetings."),
        ModelFieldTestData(
            field='reviewer',
            label='Reviewer',
            help_text="Reviewers help to make sure the release is working."),
        ModelFieldTestData(
            field='bug_manager',
            label='Bug Manager',
            help_text="Manages critical bugs and decides what needs fixing."),
        ModelFieldTestData(
            field='translation_manager',
            label='Translation Manager',
            help_text="Translation managers look after all translations for the release."),
    ]

    def setUp(self):
        self.instance = self.release = Release.objects.get(version='1.1')

    def test_model_meta_has_correct_ordering(self):
        self.assertEqual(self.release._meta.ordering, ('-release_date', '-version'))

    def test_model_meta_has_correct_get_latest_by(self):
        self.assertEqual(self.release._meta.get_latest_by, 'release_date')

    def test_model_meta_has_correct_unique_together(self):
        self.assertEqual(self.release._meta.unique_together,
                         (('project', 'version'),))

    def test_object_is_of_correct_form(self):
        self.assertEqual(str(self.release), 'Inkscape 1.1')

    def test_get_version_name(self):
        self.assertEqual(self.release.get_version_name(), '1.1')

    def test_get_absolute_url(self):
        self.assertEqual(self.release.get_absolute_url(),
                         '/release/inkscape-1.1/')

    def test_get_absolute_url_for_release_without_project(self):
        # First remove the project from test release
        self.release.project = None
        self.release.save(update_fields=['project'])

        # Check that the url is correct
        self.assertEqual(self.release.get_absolute_url(), '/release/1.1/')

    def test_breadcrumb_parent(self):
        release = ReleaseFactory(version='1.0.1', parent=ReleaseFactory(version='1.0'))
        self.assertEqual(release.breadcrumb_parent(), release.parent)

    def test_breadcrumb_parent_for_release_without_parent(self):
        # This test release does not have a parent
        parent = Release.objects.all()
        self.assertQuerysetEqual(self.release.breadcrumb_parent(), parent)

    def test_revisions(self):
        # We choose 1.0 as it has many revisions
        release = ReleaseFactory(version='1.0')
        versions = ["1.0", "1.0alpha0", "1.0alpha2", "1.0.1", "1.0.2"]
        ReleaseFactory.create_batch(len(versions), version=factory.Iterator(versions), parent=release)

        revisions = release.revisions.values_list('version', flat=True)

        # Check that 1.0 had 5 revisions (4 children and 1 itself)
        self.assertEqual(revisions.count(), 5)


        # Check that all the versions are present
        for version in versions:
            self.assertIn(version, revisions)

    def test_latest(self):
        release = ReleaseFactory(version='1.0')

        # We expect the latest revision to be 1.0.2
        latest_revision = ReleaseFactory(version='1.0.2', parent=release)
        latest_revision.platforms.add(ReleasePlatformFactory())

        # Check that 1.0.2 is indeed the latest
        self.assertEqual(release.latest, latest_revision)

    def test_responsible_people(self):
        expected_values = [
            ('Manager', Release.manager.field.help_text, self.release.manager),
            ('Reviewer', Release.reviewer.field.help_text, self.release.reviewer),
            ('Translation Manager', Release.translation_manager.field.help_text,
             self.release.translation_manager),
            ('Bug Manager', Release.bug_manager.field.help_text,
             self.release.bug_manager),
        ]
        for expected, person in zip(expected_values, self.release.responsible_people()):
            self.assertEqual(expected, person)

    def test_natural_key(self):
        self.assertEqual(self.release.natural_key(), ('inkscape', '1.1'))

    ### Tests for custom queryset methods ###

    def test_for_parent(self):
        # We expect all versions to be present since all the versions either
        # have no parent or they are the children of 1.0 (parent of 1.0.1)
        versions = ['1.1', '1.0.x', '1.0.2', '1.0.1', '1.0',
                    '1.0alpha2', '1.0alpha0', '0.91', '0.48']
        release = ReleaseFactory(version='1.0.1', parent=ReleaseFactory(version='1.0'))
        ReleaseFactory.create_batch(len(versions), version=factory.Iterator(versions), parent=release.parent)

        returned_versions = Release.objects.for_parent(
            release).values_list('version', flat=True)


        # We can't test for sequence equality (yet) because there are releases without
        # release_date that are currently being sorted last, rather than first; this
        # changes in later versions of Django, which allow us to define whether NULLs sort first or last
        self.assertCountEqual(list(returned_versions), versions)

    def test_get_by_natural_key(self):
        # First extract values from the test release
        slug, version = self.release.project.slug, self.release.version

        # Check that on passing that values, test release is returned
        self.assertEqual(Release.objects.get_by_natural_key(
            slug, version), self.release)


class TestReleaseFile(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='disk_file', label='disk file', help_text=''),
        ModelFieldTestData(field='original_url',
                           label='original url', help_text=''),
        ModelFieldTestData(field='uploaded_by',
                           label='uploaded by', help_text=''),
    ]

    @classmethod
    def setUpTestData(cls):
        # Create a new releasefile instance for tests
        # We use SimpleUploadedFile here so that we don't even touch
        # the filesystem.
        cls.filename = 'new-LPE.gif'
        cls.instance = cls.releasefile = ReleaseFile.objects.create(
            disk_file=SimpleUploadedFile(cls.filename, b'okkkkkkk'),
            first_seen=ReleaseFactory(version='1.1'),
            uploaded_by=GetTestUsersMixin.get_test_user()
        )

    def test_disk_file_has_correct_upload_location(self):
        expected_location = 'release/media'
        self.assertEqual(
            ReleaseFile.disk_file.field.upload_to, expected_location)

    def test_disk_file_has_correct_url(self):
        expected_url = f'{settings.MEDIA_URL}release/media/{self.filename}'
        self.assertEqual(self.releasefile.disk_file.url, expected_url)

    def test_object_is_name_of_disk_file(self):
        self.assertEqual(str(self.releasefile),
                         self.releasefile.disk_file.name)

    def test_get_absolute_url(self):
        self.assertEqual(self.releasefile.get_absolute_url(),
                         self.releasefile.disk_file.url)


class TestReleaseTranslation(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='release', label='release', help_text=''),
        ModelFieldTestData(
            field='language',
            label='Language',
            help_text='Which language is this translated into.'),
        ModelFieldTestData(field='html_desc',
                           label='HTML Description', help_text=''),
        ModelFieldTestData(
            field='keywords', label='HTML Keywords', help_text=''),
        ModelFieldTestData(field='release_notes',
                           label='Release notes', help_text=''),
    ]

    def setUp(self):
        self.instance = self.reltrans = ReleaseTranslationFactory(
            release__version='1.0.1', language='de')

    def test_model_meta_has_correct_unique_together(self):
        expected = (('release', 'language'),)
        self.assertEqual(self.reltrans._meta.unique_together, expected)


class TestPlatform(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='name', label='Name', help_text=''),
        ModelFieldTestData(field='desc', label='Description', help_text=''),
        ModelFieldTestData(
            field='keywords', label='HTML Keywords', help_text=''),
        ModelFieldTestData(
            field='parent', label='Parent Platform', help_text=''),
        ModelFieldTestData(
            field='manager', label='Platform Manager', help_text=''),
        ModelFieldTestData(field='codename', label='codename', help_text=''),
        ModelFieldTestData(
            field='codebit',
            label='Code Bit Override',
            help_text="Use this Code Name instead of the Name field "
                      "(when renaming but keeping the urls the same)"),
        ModelFieldTestData(field='order', label='order', help_text=''),
        ModelFieldTestData(
            field='instruct',
            label='Instructions',
            help_text="If supplied, this text will appear after the tabs,"
                      " but before the release notes. Will propergate to"
                      " all child platforms that do not have their own."),
        ModelFieldTestData(
            field='file_template',
            label='file template',
            help_text="Re-write the download filename to use this python "
                      "format template."),
        ModelFieldTestData(
            field='match_family',
            label='match family',
            help_text='User agent os match, whole string.'),
        ModelFieldTestData(
            field='match_version',
            label='match version',
            help_text='User agent os version partial match, e.g. |10|11| '
                      'will match both version 10 and version 11, must ha'
                      've pipes at start and end of string.'),
        ModelFieldTestData(
            field='match_bits', label='match bits', help_text=''),
        ModelFieldTestData(
            field='is_archived',
            label='is archived',
            help_text='If this platform is archived, it will not be made'
                      ' available for new uploads.'),

    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        known_names = {
            'Source': {},
            'GNU/Linux': {
                'AppImage': [],
                'Flatpak': [],
                'Snap': [],
                'Ubuntu': [
                    'DEB Package',
                    'Install Now',
                    'ppa'
                ],
            },
            'Windows': {
                '64-bit': [
                    'exe',
                    'msi',
                    'compressed 7z',
                    'binary (debug files, 7z)',
                ]
            },
            'macOS:mac-os-x': {
                '1010-1015': [],
                '104': ['dmg-ppc'],
                '105': ['dmg-universal'],
                '105-106': [],
                '10.6-10.10': ['dmg'],
                '10.7-10.10': ['dmg'],
                'version-unknown': ['alpha-tarball'],
            },
        }

        cls.gnulinux = PlatformFactory(name='GNU/Linux')
        build_test_platforms(known_names)

    def setUp(self):
        self.instance = self.platform = self.gnulinux

    def test_uuid_attribute(self):
        self.assertEqual(self.platform.uuid(), 'gnulinux')

    def test_tab_name_attribute(self):
        self.assertEqual(self.platform.tab_name(), self.platform.name)

    def test_tab_text_attribute(self):
        self.assertEqual(self.platform.tab_text(), self.platform.desc)

    def test_tab_cat_attribute(self):
        self.assertEqual(self.platform.tab_cat(), {'icon': self.platform.icon})

    def test_model_meta_has_correct_ordering(self):
        self.assertEqual(self.platform._meta.ordering, ('-order', 'codename'))

    def test_get_absolute_url(self):
        expected_url = '/release/all/gnulinux/'
        self.assertEqual(self.platform.get_absolute_url(), expected_url)

    def test_save_changes_children_(self):
        # First we get a child platform
        ubuntu_ppa = Platform.objects.get(codename='gnulinux/ubuntu/ppa')

        # Then we change the codename of the test platform
        self.platform.codebit = 'gnuslashlinux'
        self.platform.save()

        ubuntu_ppa.refresh_from_db()

        # Check that the codename of the child platform got updated
        self.assertEqual(ubuntu_ppa.codename, 'gnuslashlinux/ubuntu/ppa')

        # Finally restore the codebit to the original
        self.platform.codebit = ''
        self.platform.save()


    @staticmethod
    def platforms_from_codenames(codenames):
        # We use a list comprehension instead of a codename__in=codenames lookup
        # because the order in which the ancestors and descendants functions
        # add platforms to their internal list might be different than returned
        # from a queryset.
        return [Platform.objects.filter(codename=codename).last() or f'{codename} missing' for codename in codenames]

    ancestors_test_data = [
        TestData(input='gnulinux',
                 expected=['gnulinux'],
                 case='Base Platform'),
        TestData(input='gnulinux/ubuntu',
                 expected=['gnulinux/ubuntu', 'gnulinux'],
                 case='Child platform'),
        TestData(input='windows/64-bit/exe',
                 expected=['windows/64-bit/exe', 'windows/64-bit', 'windows'],
                 case='A specific Windows platform'),
    ]

    def test_ancestors(self):
        for test_data in self.ancestors_test_data:
            with self.subTest(data=test_data):
                platform = Platform.objects.get(codename=test_data.input)
                ancestors = self.platforms_from_codenames(test_data.expected)
                self.assertListEqual(platform.ancestors(), ancestors)

    descendants_test_data = [
        TestData(input='gnulinux',
                 expected=['gnulinux/appimage',
                           'gnulinux/flatpak',
                           'gnulinux/snap',
                           'gnulinux/ubuntu',
                           'gnulinux/ubuntu/deb-package',
                           'gnulinux/ubuntu/install-now',
                           'gnulinux/ubuntu/ppa'],
                 case='GNU/linux Platform'),
        TestData(input='mac-os-x',
                 expected=['mac-os-x/1010-1015', 'mac-os-x/104',
                           'mac-os-x/104/dmg-ppc', 'mac-os-x/105',
                           'mac-os-x/105/dmg-universal', 'mac-os-x/105-106',
                           'mac-os-x/106-1010', 'mac-os-x/106-1010/dmg',
                           'mac-os-x/107-1010', 'mac-os-x/107-1010/dmg',
                           'mac-os-x/version-unknown',
                           'mac-os-x/version-unknown/alpha-tarball'],
                 case='MacOS Platforms'),
        TestData(input='windows/64-bit',
                 expected=['windows/64-bit/exe',
                           'windows/64-bit/msi',
                           'windows/64-bit/compressed-7z',
                           'windows/64-bit/binary-debug-files-7z'],
                 case='A specific Windows platform'),
    ]

    def test_descendants(self):
        for test_data in self.descendants_test_data:
            with self.subTest(data=test_data):
                platform = Platform.objects.get(codename=test_data.input)
                descendants = self.platforms_from_codenames(test_data.expected)
                self.assertListEqual(platform.descendants(), descendants)

    name_test_data = [
        TestData(input='gnulinux', expected='GNU/Linux', case=''),
        TestData(
            input='windows/64-bit/exe',
            expected='Windows : 64-bit : exe', case=''),
        TestData(
            input='gnulinux/appimage',
            expected='GNU/Linux : AppImage', case=''),
        TestData(
            input='gnulinux/ubuntu/ppa',
            expected='GNU/Linux : Ubuntu : ppa', case=''),
        TestData(input='source', expected='Source', case=''),
        TestData(input='mac-os-x/106-1010',
                 expected='macOS : 10.6-10.10', case=''),
    ]

    def test_full_name(self):
        for test_data in self.name_test_data:
            with self.subTest(data=test_data):
                platform = Platform.objects.get(codename=test_data.input)
                self.assertEqual(platform.full_name, test_data.expected)

    def test_str_of_objects_is_its_name(self):
        self.assertEqual(str(self.platform), self.platform.full_name)

    ### Tests for custom queryset methods ###

    def test_natural_key(self):
        self.assertEqual(self.platform.natural_key(),
                         (self.platform.codename,))


class TestPlatformTranslation(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='platform', label='platform', help_text=''),
        ModelFieldTestData(field='language', label='Language', help_text=''),
        ModelFieldTestData(field='name', label='Name', help_text=''),
        ModelFieldTestData(field='desc', label='Description', help_text=''),
        ModelFieldTestData(
            field='keywords', label='HTML Keywords', help_text=''),
        ModelFieldTestData(
            field='instruct', label='Instructions', help_text=''),

    ]

    def setUp(self):
        self.instance = self.plattrans = PlatformTranslationFactory(
            language='fr', platform__codename='windows/windows-store')

    def test_model_meta_has_correct_unique_together(self):
        self.assertEqual(self.plattrans._meta.unique_together,
                         (('platform', 'language'),))


class TestReleasePlatform(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='release', label='Release', help_text=''),
        ModelFieldTestData(
            field='platform', label='Release Platform', help_text=''),
        ModelFieldTestData(
            field='download', label='Download Link', help_text=''),
        ModelFieldTestData(field='resource', label='resource', help_text=''),
        ModelFieldTestData(
            field='howto', label='Instructions Link', help_text=''),
        ModelFieldTestData(
            field='info', label='Release Platform Information', help_text=''),
        ModelFieldTestData(
            field='created', label='Date created', help_text=''),

    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        PlatformFactory(name='Windows', match_family='Windows')
        PlatformFactory(name='GNU/Linux', match_family='Linux')
        build_test_platforms({
            'GNU/Linux': {'flatpak': []},
            'Windows': {'64-bit': ['exe', 'msi']},
        })
        win64 = Platform.objects.get(codename='windows/64-bit/exe')
        ReleasePlatformFactory(release__version='1.0.1', platform=win64)
        ReleasePlatformFactory(release__version='1.0.1',
                               platform=Platform.objects.get(codename='windows/64-bit/msi'))
        cls.instance = cls.relplat = ReleasePlatformFactory(
            release__version='1.0.2', platform=Platform.objects.get(codename='gnulinux/flatpak'),
            info='ReleasePlatform-specific Info')

    def test_model_meta_has_correct_ordering(self):
        self.assertEqual(self.relplat._meta.ordering, ('platform__parent_id',))

    def test_str_of_object_is_of_correct_form(self):
        self.assertEqual(str(self.relplat),
                         f'{self.relplat.release} - {self.relplat.platform}')

    def test_get_url_kwargs(self):
        expected_kwargs = {'version': self.relplat.release.version,
                           'platform': self.relplat.platform.codename,
                           'project': self.relplat.release.project_id, }
        self.assertDictEqual(self.relplat.get_url_kwargs(), expected_kwargs)

    def test_get_absolute_url(self):
        expected_url = '/release/inkscape-1.0.2/gnulinux/flatpak/'
        self.assertEqual(self.relplat.get_absolute_url(), expected_url)

    def test_get_download_url(self):
        expected_download_url = '/release/inkscape-1.0.2/gnulinux/flatpak/dl/'
        self.assertEqual(self.relplat.get_download_url(),
                         expected_download_url)

    def test_get_resource_url(self):
        # This test release platform does not have a resource
        # So, the resource url is same as the download url
        expected_resource_url = '/release/inkscape-1.0.2/gnulinux/flatpak/dl/'
        self.assertEqual(self.relplat.get_download_url(),
                         expected_resource_url)

    def test_get_resource_url_for_object_with_a_resource(self):
        # Create a dummy resource with a dummy file
        resource = Resource.objects.create(
            name='Resource 1', user=GetTestUsersMixin.get_test_user(),
            download=SimpleUploadedFile(name='1.0.2-flatpak', content=b'okkkk'))

        self.relplat.resource = resource
        self.relplat.save()

        # Check that the correct resource url is returned
        self.assertEqual(self.relplat.get_resource_url(),
                         f'/gallery/item/{resource.pk}/1.0.2-flatpak')

    def test_get_expected_filename(self):
        # We use the Source Archive Tarball because it has a non
        # null file_template
        grandparent_platform = PlatformFactory(name='Source', codename='source')
        parent_platform = PlatformFactory(name='Source', codename='source', parent=grandparent_platform)

        relplat = ReleasePlatformFactory(platform__parent=parent_platform, platform__name='bz2',
                                         platform__codename='bz2',
                                         platform__file_template="{project.slug}-{release.version}.tar.bz2",
                                         release__version='1.0.x')
        resource = Resource.objects.create(
            name='Resource 1', user=GetTestUsersMixin.get_test_user(),
            download=SimpleUploadedFile(name='1.0.x-source', content=b'okkkk'))

        relplat.resource = resource
        relplat.save()

        # Check that we get the correct expected filename
        self.assertEqual(relplat.get_expected_filename(),
                         'inkscape-1.0.x.tar.bz2')

    def test_parent_property(self):
        # A new release platform is created for the parent
        # if the current release platform has a parent
        returned_parent = self.relplat.parent

        # Check that the new release platform created has correct data
        self.assertEqual(returned_parent.release, self.relplat.release)
        self.assertEqual(returned_parent.platform,
                         self.relplat.platform.parent)

    def test_parent_property_when_object_has_no_parent(self):
        # First we remove the parent of the release platform
        self.relplat.platform.parent = None
        self.relplat.platform.save()

        # Check that the property returns the release for the current
        # release platform
        self.assertEqual(self.relplat.parent, self.relplat.release)

    def test_breadcrumb_name(self):
        # We do not test for any other languages other than English
        # for now
        self.assertEqual(self.relplat.breadcrumb_name(),
                         self.relplat.platform.name)

    def test_instructions(self):
        self.assertEqual(self.relplat.instructions, self.relplat.info)

    def test_instructions_when_object_has_no_info(self):
        # First we remove the info for the Windows Store
        # 1.0.2 release platform/
        relplat = ReleasePlatformFactory(
            platform__codename='windows/windows-store',
            release__version='1.0.2',
            info=None
        )

        # Check that the platform instructions are returned
        self.assertEqual(relplat.instructions, relplat.platform.instructions)

    def test_natural_key(self):
        self.assertEqual(self.relplat.natural_key(),
                         (('gnulinux/flatpak',), ('inkscape', '1.0.2')))

    ### Tests for custom queryset methods ###
    # Note that I (pgcd) have trimmed down the data here because, as it was, its only purpose was to test the data
    # included in the fixtures; I think I kept enough to test for the main queryset filtering logic
    os_test_data = [
        TestData(
            input=('Windows', '10', 64),
            expected=[
                ('windows/64-bit/exe', ('inkscape', '1.0.1')),
                ('windows/64-bit/msi', ('inkscape', '1.0.1')),
            ],
            case='Windows 10 - 64 bit releases'
        ),
        TestData(
            input=('Linux', 'ubuntu', 64),
            expected=[
                ('gnulinux/flatpak', ('inkscape', '1.0.2')),
            ],
            case='Ubuntu releases'
        ),
    ]

    def assertReleasePlatformsIn(self, expected, queryset):
        """ Assert that the expected release platforms are present in the 
        queryset."""
        # Check that we have the exact number of release
        # platforms as expected
        self.assertEqual(len(expected), queryset.count())

        # Extract the codenames and versions to generate a queryset
        codenames, releases = zip(*expected)
        _, versions = zip(*releases)

        expected_queryset = ReleasePlatform.objects.filter(
            platform__codename__in=codenames,
            release__version__in=versions)

        # The ordered kwarg should be False since we are just
        # checking that the platforms were in the queryset.
        self.assertQuerysetEqual(expected_queryset, queryset, ordered=False)

    def test_for_os(self):
        for test_data in self.os_test_data:
            with self.subTest(data=test_data):
                queryset = ReleasePlatform.objects.for_os(*test_data.input)
                self.assertReleasePlatformsIn(test_data.expected, queryset)

    level_test_data = [
        TestData(
            input='',
            expected=['gnulinux', 'windows', 'mac-os-x', 'source'],
            case='All the base platforms with no parents (Level 0).'),
        TestData(
            input='windows',
            expected=[
                'windows/64-bit',
                'windows/32-bit',
                'windows/windows-store'
            ],
            case='All the platforms at the same level as Windows (Level 1)'),
        TestData(
            input='gnulinux/ubuntu',
            expected=[
                'gnulinux/ubuntu/deb-package',
                'gnulinux/ubuntu/install-now',
                'gnulinux/ubuntu/ppa'
            ],
            case='All the platforms at the same level as Ubuntu (Level 2)'),
    ]

    @staticmethod
    def platforms_from_codenames(codenames):
        return list(Platform.objects.filter(codename__in=codenames))

    def test_for_level(self):
        for test_data in self.level_test_data:
            with self.subTest(data=test_data):
                platforms = ReleasePlatform.objects.for_level(
                    parent=test_data.input)
                self.assertListEqual(
                    platforms, self.platforms_from_codenames(test_data.expected))


class TestDownloadMirror(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='name', label='Mirror Name', help_text=''),
        ModelFieldTestData(field='release', label='release', help_text=''),
        ModelFieldTestData(
            field='download', label='Download Link', help_text=''),
    ]

    def setUp(self):
        self.instance = self.dwnldmirror = DownloadMirrorFactory()

    def test_str_of_object_is_its_name(self):
        self.assertEqual(str(self.dwnldmirror), self.dwnldmirror.name)


class TestReleasePlatformTranslation(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(
            field='release_platform', label='release platform', help_text=''),
        ModelFieldTestData(
            field='language',
            label='Language',
            help_text='Which language is this translated into.'),
        ModelFieldTestData(
            field='howto', label='Instructions Link', help_text=''),
        ModelFieldTestData(
            field='info', label='Release Platform Information', help_text=''),
    ]

    def setUp(self):
        relplat = ReleasePlatformFactory(
            release__version='1.0.2', platform__codename='gnulinux/flatpak')
        self.instance = self.relplattrans = ReleasePlatformTranslationFactory(release_platform=relplat, language='de')

        def test_model_meta_has_correct_unique_together(self):
            self.assertEqual(self.relplattrans._meta.unique_together,
                             (('release_platform', 'language'),))
