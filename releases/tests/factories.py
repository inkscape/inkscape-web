import factory

import releases.models


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.Project
        django_get_or_create = ('slug', )
    name = 'Inkscape'
    slug = 'inkscape'
    default = True


release_dates = {
    "0.48": "2010-08-23",
    "0.91": "2015-01-28",
    "1.0": "2020-05-01",
    "1.0.2": "2021-01-17"
}

class ReleaseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.Release
        django_get_or_create = ('project', 'version')

    project = factory.SubFactory(ProjectFactory)
    release_notes = factory.Faker('text')
    version = factory.Faker('bothify', text='#.#')
    release_date = factory.LazyAttribute(lambda obj: release_dates.get(obj.version))


class PlatformFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.Platform
        django_get_or_create = ('name', 'parent')

    class Params:
        name_and_bit = factory.Trait(
            name=factory.LazyAttribute(lambda obj: obj.name_and_bit.partition(':')[0]),
            codebit=factory.LazyAttribute(lambda obj: obj.name_and_bit.partition(':')[2]),
        )

    parent = None
    name = 'GNU/Linux'
    instruct = factory.Faker('text')
    match_family = factory.LazyAttribute(lambda obj: obj.parent.match_family if obj.parent else 'Linux')


class ReleasePlatformFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.ReleasePlatform

    release = factory.SubFactory(ReleaseFactory)
    platform = factory.SubFactory(PlatformFactory)


class ReleaseTranslationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.ReleaseTranslation

    release = factory.SubFactory(ReleaseFactory)


class PlatformTranslationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.PlatformTranslation

    platform = factory.SubFactory(PlatformFactory)


class ReleasePlatformTranslationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.ReleasePlatformTranslation

    release_platform = factory.SubFactory(ReleasePlatformFactory)


class DownloadMirrorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = releases.models.DownloadMirror

    name = "Internet Archive"
    release = factory.SubFactory(ReleasePlatformFactory)
    download = "https://archive.org/download/inkscape/"


def build_test_platforms(known_names: dict):
    """Iterates over a dict of platform names to build platfors in the expected hierarchy
    eg, given
    `known_names = {'GNU/Linux': {'Ubuntu': ['deb', 'ppa'], 'Android': {}}}`
    the result would be
    -> Platform GNU/Linux
      -> Ubuntu
          -> deb
          -> ppa
    -> Platform Android
    """
    for i, (name, descendants) in enumerate(known_names.items()):
        first_level = PlatformFactory(name_and_bit=name, order=100-i)
        for n, (second_level_name, third_levels) in enumerate(descendants.items()):
            second_level = PlatformFactory(name_and_bit=second_level_name, parent=first_level, order=100-n)
            for o, third_level in enumerate(third_levels):
                PlatformFactory(name_and_bit=third_level, parent=second_level, order=100-o)
