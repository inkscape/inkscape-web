#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for views of releases app."""
from datetime import datetime
from unittest import mock, skip

import factory
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import Http404
from django.test import TestCase
from django.utils import timezone

from resources.factories import ResourceFactory
from resources.models import Resource
from testing import base
from testing.base import TestData
from testing.mixins import GetTestUsersMixin, GetViewMixin
from .factories import ReleasePlatformFactory, ReleaseFactory, build_test_platforms

from ..models import Platform, Release, ReleasePlatform
from ..views import DownloadRedirect, PlatformList, PlatformView, ReleaseView, ReleasePlatformStage, ReleasePlatformView


class TestDownloadRedirect(GetViewMixin, TestCase):
    view_url = '/release/'
    url_name = 'releases:download'

    user_agent_test_data = [
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:25.0) Gecko/20100101 Firefox/25.0',
                 expected=('Mac OS X', '10.8', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b3) Gecko/20090305 Firefox/3.1b3 GTB5',
                 expected=('Mac OS X', '10.5', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2',
                 expected=('Mac OS X', '10.5', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
                 expected=('Windows', '10.0', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 5.1; rv:25.0) Gecko/20100101 Firefox/25.0',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130328 Firefox/21.0',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b9pre) Gecko/20101228 Firefox/4.0b9pre',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.2a1pre) Gecko/20110324 Firefox/4.2a1pre',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b7) Gecko/20101111 Firefox/4.0b7',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b8pre) Gecko/20101114 Firefox/4.0b8pre ',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0a2) Gecko/20110613 Firefox/6.0a2',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.0; es-ES; rv:1.8.0.3) Gecko/20060426 Firefox/1.5.0.3',
                 expected=('Windows', '5.0', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; cs; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; Windows NT 6.1; rv:2.0b2) Gecko/20100720 Firefox/4.0b2',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (X11; Linux i686 on x86_64; rv:12.0) Gecko/20100101 Firefox/12.0',
                 expected=('Linux', None, 64), case=''),
        TestData(input='Mozilla/5.0 (X11; Linux i686; rv:30.0) Gecko/20100101 Firefox/30.0',
                 expected=('Linux', None, 32), case=''),
        TestData(input='Mozilla/5.0 (X11; Linux x86_64; rv:2.0b4) Gecko/20100818 Firefox/4.0b4',
                 expected=('Linux', None, 64), case=''),
        TestData(input='Mozilla/5.0 (X11; Linux x86_64; rv:2.0b9pre) Gecko/20110111 Firefox/4.0b9pre',
                 expected=('Linux', None, 64), case=''),
        TestData(input='Mozilla/5.0 (X11; Ubuntu; Linux armv7l; rv:17.0) Gecko/20100101 Firefox/17.0',
                 expected=('Linux', 'ubuntu', 32), case=''),
        TestData(input='Mozilla/5.0 (X11; U; Linux amd64; rv:5.0) Gecko/20100101 Firefox/5.0 (Debian)',
                 expected=('Linux', 'debian', 64), case=''),
        TestData(input='Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2) Gecko/20100308 Ubuntu/10.04 (lucid) Firefox/3.6 GTB7.1',
                 expected=('Linux', 'ubuntu', 32), case=''),
        TestData(input='Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.12) Gecko/20080214 Firefox/2.0.0.12',
                 expected=('Linux', None, 64), case=''),

        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; cs-CZ) AppleWebKit/523.15 (KHTML, like Gecko) Version/3.0 Safari/523.15',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/528.16 (KHTML, like Gecko) Version/4.0 Safari/528.16',
                 expected=('Windows', '6.0', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-HK) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; sv-SE) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10',
                 expected=('Mac OS X', '10.7', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17',
                 expected=('Mac OS X', '10.8', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/6.1.3 Safari/537.75.14',
                 expected=('Mac OS X', '10.8', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/600.3.10 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.10',
                 expected=('Mac OS X', '10.10', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.39 (KHTML, like Gecko) Version/9.0 Safari/601.1.39',
                 expected=('Mac OS X', '10.11', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13) AppleWebKit/603.1.13 (KHTML, like Gecko) Version/10.1 Safari/603.1.13',
                 expected=('Mac OS X', '10.13', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36',
                 expected=('Mac OS X', '10.10', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.45 Safari/535.19',
                 expected=('Mac OS X', '10.6', 64), case=''),
        TestData(input='Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4',
                 expected=('Mac OS X', None, 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.792.0 Safari/535.1',
                 expected=('Windows', '6.0', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11',
                 expected=('Windows', '6.0', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1623.0 Safari/537.36',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
                 expected=('Windows', '6.1', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6',
                 expected=('Windows', '6.2', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1467.0 Safari/537.36',
                 expected=('Windows', '6.2', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24',
                 expected=('Windows', '6.2', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.15 (KHTML, like Gecko) Chrome/24.0.1295.0 Safari/537.15',
                 expected=('Windows', '6.2', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36',
                 expected=('Windows', '6.2', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
                 expected=('Windows', '6.3', 64), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.36 Safari/525.19',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML, like Gecko) Chrome/2.0.173.1 Safari/530.5',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, like Gecko) Chrome/4.0.223.3 Safari/532.2',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.600.0 Safari/534.14',
                 expected=('Windows', '5.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0',
                 expected=('Windows', '5.2', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.558.0 Safari/534.10',
                 expected=('Windows', '5.2', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4',
                 expected=('Windows', '5.2', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20',
                 expected=('Windows', '6.0', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.201.1 Safari/532.0',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16',
                 expected=('Windows', '6.1', 32), case=''),
        TestData(input='Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0',
                 expected=('Linux', None, 64), case=''),
        TestData(input='Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.587.0 Safari/534.12',
                 expected=('Windows', '6', 32), case=''),

    ]

    def test_get_os(self):
        for test_data in self.user_agent_test_data:
            with self.subTest(data=test_data):
                user_agent, os_data = test_data.input, test_data.expected
                self.assertEqual(DownloadRedirect._get_os(user_agent), os_data)

    release_url_test_data = [
        TestData(input=('Windows', '10', 64),
                 expected='/release/1.1/windows/32-bit/', case=''),
        TestData(input=('Linux', 'ubuntu', 64),
                 expected='/release/1.1/windows/32-bit/', case=''),
    ]

    @skip(reason="""Until the match bits (and families) are properly set, there is\
    little value this test will add since it will be checking against the wrong\
    behavior. Instead it is left here to test against a correct 'future' version. """)
    def test_get_url(self):
        for test_data in self.release_url_test_data:
            view = self.get_view(DownloadRedirect)
            os_data, expected_url = test_data.input, test_data.expected
            with self.subTest(data=test_data):
                self.assertEqual(view.get_url(None, *os_data), expected_url)

    # TODO: Also check for redirection in the correct future version using the test client


class TestReleaseView(GetTestUsersMixin, base.ViewTestCase):
    url_kwargs = {'version': 'inkscape-1.0.2'}
    url_name = 'releases:release'
    template_name = 'releases/release_detail.html'

    release_test_data = [
        TestData(
            input='1.0.2',
            expected={
                'revisions': ['1.0.2', '1.0.1', '1.0', '1.0alpha2', '1.0alpha0'],
                'hidden_versions': ['1.0alpha2', '1.0alpha0'],
            },
            case='A point release that has been released.'
        ),
        TestData(
            input='1.0',
            expected={
                'revisions': ['1.0.2', '1.0.1', '1.0', '1.0alpha2', '1.0alpha0'],
                'hidden_versions': ['1.0alpha2', '1.0alpha0'],
            },
            case='A top level release that has been released.'
        ),
        TestData(
            input='1.1',
            expected={
                'revisions': ['1.1', '1.1rc1'],
                'hidden_versions': ['1.1', '1.1rc1'],
            },
            case="A top level release that hasn't been released yet."
        ),
        TestData(
            input='1.1rc1',
            expected={
                'revisions': ['1.1', '1.1rc1'],
                'hidden_versions': ['1.1', '1.1rc1'],
            },
            case="A pre-release whose parent release(1.1) hasn't been released."
        ),
        TestData(
            input='0.91',
            expected={
                'revisions': [],
                'hidden_versions': [],
            },
            case="A top level release with no revisions or pre-releases."
        ),
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        releases = {
            '0.48': {'released': True,},
            '0.91': {'released': True,},
            '1.1': {'children': {'1.1rc1': {}}},
            '1.0.x': {},
            '1.0': {
                'released': True,
                'children': {
                    '1.0.2': {'released': True},
                    '1.0.1': {'released': True},
                    '1.0alpha2': {},
                    '1.0alpha0': {}
                }
            }
        }
        for p_version, p_data in releases.items():
            parent = ReleaseFactory(version=p_version, release_date=timezone.now() if p_data.get('released') else None)
            for c_version, c_data in p_data.get('children', {}).items():
                ReleaseFactory(version=c_version, release_date=timezone.now() if c_data.get('released') else None,
                               parent=parent)

    def test_view_has_correct_slug_field_and_slug_url_kwarg(self):
        self.assertEqual(ReleaseView.slug_field, 'version')
        self.assertEqual(ReleaseView.slug_url_kwarg, 'version')

    def test_get_queryset(self):
        view = self.get_view(ReleaseView)
        returned_queryset = view.get_queryset()
        # The order does not matter for this particular test
        # We expect every release to be here since every test release
        # has the project set to Inkscape which is the default project
        self.assertCountEqual(returned_queryset, Release.objects.all())

    def assertCorrectVersions(self, returned, expected, **kwargs):
        """Assert that the versions are as expected."""
        versions = [release.version for release in returned]
        self.assertCountEqual(versions, expected, **kwargs)

    def test_get_context_data(self):
        user = self.get_test_user()
        view = self.get_view(ReleaseView, user=user)

        # Borrow some constants from the actual code
        (REVS, VERS, DEVEL) = range(3)
        LIST = 1

        for test_data in self.release_test_data:
            with self.subTest(data=test_data):
                # Get the context data for a test release
                # We don't need to patch url_kwargs since the code
                # uses self.object and not self.get_object()
                view.object = Release.objects.get(version=test_data.input)
                context_data = view.get_context_data()

                self.assertCorrectVersions(
                    returned=context_data['releases'][REVS][LIST],
                    expected=test_data.expected['revisions'],
                    msg='Revisions were not as expected.'
                )

                # We expect these versions to stay same no matter the release
                # since they are master releases.
                self.assertCorrectVersions(
                    returned=context_data['releases'][VERS][LIST],
                    expected=['1.0', '0.91', '0.48'],
                    msg='Master Versions were not as expected.'
                )

                # We expect these versions to also stay the same no matter
                # the release since they are development releases.
                self.assertCorrectVersions(
                    returned=context_data['releases'][DEVEL][LIST],
                    expected=['1.1', '1.0.x'],
                    msg='In Development Versions were not as expected.'
                )

                hidden_versions = [
                    release.version
                    for release in context_data['releases'][REVS][LIST]
                    # Some releases may not have 'hide' attribute
                    if hasattr(release, 'hide') and release.hide
                ]
                self.assertCountEqual(
                    hidden_versions,
                    test_data.expected['hidden_versions'],
                    msg='Hidden Versions were not as expected.'
                )


class TestPlatformList(base.ViewTestCase):
    url_kwargs = {'version': 'inkscape-1.0.2'}
    url_name = 'releases:platforms'
    template_name = 'releases/platform_list.html'

    def setUp(self):
        self.release = ReleaseFactory(version='1.0.2')
        super().setUp()

    def test_get_title_returns_correct_title(self):
        view = self.get_view(PlatformList)
        view.object = self.release
        self.assertEqual(view.get_title(), 'All Platforms for Inkscape 1.0.2')


class TestPlatformView(base.ViewTestCase):
    url_kwargs = {'platform': 'gnulinux'}
    url_name = 'releases:platform'
    template_name = 'releases/platform_detail.html'

    @classmethod
    def setUpTestData(cls):
        # Most of the release platforms do not have a resource
        # in test fixtures. So, to emulate real releases on the
        # website, we add some dummy resources to some of the
        # release platforms.
        build_test_platforms({
            'GNU/Linux': {'appimage': [], 'Ubuntu': ['Install now', 'ppa']},
            'Windows':  {'32-bit': ['exe'], '64-bit': ['exe']},
            'Mac OS X': {'105-106': []},
        })
        add_resources = {
            '1.0': ['gnulinux/appimage', 'windows/32-bit/exe'],
            '1.0.2': ['windows/64-bit/exe'],
            '0.91': ['mac-os-x/105-106'],
        }
        linked_resources = {  # these are the ones that would've been loaded from fixtures
            '0.91': ['gnulinux/ubuntu/install-now', 'gnulinux/ubuntu/ppa'],
        }
        num = 0
        for version, platforms in add_resources.items():
            for platform in platforms:
                ReleasePlatformFactory(
                    platform=Platform.objects.get(codename=platform),
                    release__version=version,
                    resource=ResourceFactory(name=f'Resource {num}', download___filename=f'File {num}')
                )
                num += 1

        for version, platforms in linked_resources.items():
            for platform in platforms:
                ReleasePlatformFactory(
                    platform=Platform.objects.get(codename=platform),
                    release__version=version,
                    download='https://example.com'
                )

    platform_test_data = [
        TestData(
            input='gnulinux',
            expected=[
                ('0.91', [
                    'gnulinux/ubuntu/install-now',
                    'gnulinux/ubuntu/ppa'
                    # Both have external links
                ]),
                ('1.0', ['gnulinux/appimage']),  # Has a resource
            ],
            case='All GNU/Linux releases.'
        ),
        TestData(
            input='windows',
            expected=[
                ('1.0', ['windows/32-bit/exe']),  # Has a resource
                ('1.0.2', ['windows/64-bit/exe']),  # Has external link
                ('1.0.x', [
                    'windows/64-bit/compressed-7z',
                    'windows/32-bit/compressed-7z'
                ]),  # Both have external links
            ],
            case='All Windows releases.'
        ),
        TestData(
            input='mac-os-x',
            expected=[
                ('0.91', ['mac-os-x/105-106']),  # Has a resource
                ('1.0.x', ['mac-os-x/1010-1015']),  # Has external link
            ],
            case='All Mac OS X releases.'
        )
    ]

    def get_release_and_platforms(self, data):
        """Convert context_data in a format that can be easily tested."""
        version = data['release'].version
        platforms = [rp.platform.codename for rp in data['platforms']]
        return version, platforms

    def test_get_context_data(self):
        for test_data in self.platform_test_data:
            with self.subTest(data=test_data):
                # This patch is needed because the code uses self.get_object()
                with mock.patch.dict(self.url_kwargs, {'platform': test_data.input}):
                    # We don't pass a user to the view. Thus, the user is an
                    # anonymous user. This means they don't have the permission
                    # to change a release.
                    view = self.get_view(PlatformView)
                    view.object = Platform.objects.get(
                        codename=test_data.input)
                    context_data = view.get_context_data(**view.kwargs)

                    # Context Data has mappings from release to its
                    # release platforms like:
                    # Inkscape 1.1 - Windows 32-bit exe, Ubuntu PPA, ...
                    for index, mapping in enumerate(context_data['objects']):
                        self.assertEqual(
                            self.get_release_and_platforms(mapping),
                            test_data.expected[index]
                        )


class TestReleasePlatformStage(base.ViewTestCase):

    view_url = '/release/inkscape-1.0.2/gnulinux/'
    url_kwargs = {'project': 'inkscape', 'version': '1.0.2',
                  'platform': 'gnulinux'}
    url_name = 'releases:platform'
    template_name = 'releases/releaseplatform_stage.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        build_test_platforms({
            'GNU/Linux': {'flatpak': [], 'Ubuntu': ['Install now', 'ppa']},
            'Windows': {'64-bit': ['exe', 'msi', 'compressed (7z)'], '32-bit': ['exe'], },
            'Source': {'Version Control': ['git']},
        })
        win64 = Platform.objects.get(codename='windows/64-bit/exe')
        win32 = Platform.objects.get(codename='windows/32-bit/exe')
        ReleasePlatformFactory.create_batch(2, release__version='1.0.1',
                                            platform=factory.Iterator([win64, win32]))
        ReleasePlatformFactory(release__version='1.0.1',
                               platform=Platform.objects.get(codename='windows/64-bit/msi'))
        ReleasePlatformFactory(release__version='1.0.1',
                               platform=Platform.objects.get(codename='windows/64-bit/compressed-7z'))

        ReleasePlatformFactory(release__version='1.0.2', platform=win32)
        ReleaseFactory(version='1.1')  # No releaseplatforms for this

    def test_view_has_correct_slug_field(self):
        self.assertEqual(ReleasePlatformStage.slug_field, 'codename')

    def test_view_has_correct_slug_url_kwarg(self):
        self.assertEqual(ReleasePlatformStage.slug_url_kwarg, 'platform')

    rp_test_data = [
        TestData(
            input=dict(version='1.0.1', platform='windows'),
            expected=('1.0.1', ['windows/64-bit', 'windows/32-bit']),
            case='Windows downloads for 1.0.1.'
        ),
        TestData(
            input=dict(version='1.0.1', platform='windows/64-bit'),
            expected=(
                '1.0.1',
                [
                    'windows/64-bit/exe',
                    'windows/64-bit/msi',
                    'windows/64-bit/compressed-7z',
                ]
            ),

            case='Windows 64 bit downloads for 1.0.1.'
        ),
        TestData(
            input=dict(version='1.1', platform='gnulinux/ubuntu/ppa'),
            expected=('1.1', []),
            case='Ubuntu PPA downloads for 1.1 (not released yet),'
        ),
    ]

    def get_release_and_platforms(self, data):
        """Convert context_data in a format that can be easily tested."""
        version = data['release'].version
        platforms = [platform.codename for platform in data['platforms']]
        return version, platforms

    def test_get_context_data(self):
        for test_data in self.rp_test_data:
            with self.subTest(data=test_data):
                new_url_kwargs = {
                    'version': test_data.input['version'],
                    'platform': test_data.input['platform']
                }
                with mock.patch.dict(self.url_kwargs, new_url_kwargs):
                    view = self.get_view(ReleasePlatformStage)
                    view.object = Platform.objects.get(
                        codename=test_data.input['platform'])
                    context_data = view.get_context_data()
                    self.assertEqual(
                        self.get_release_and_platforms(context_data),
                        test_data.expected
                    )


class TestReleasePlatformView(base.ViewTestCase):
    view_url = '/release/inkscape-1.0.2/windows/32-bit/exe/dl/'
    url_kwargs = {'project': 'inkscape', 'version': '1.0.2',
                  'platform': 'windows/32-bit/exe'}
    url_name = 'releases:download'
    template_name = 'releases/releaseplatform_detail.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        build_test_platforms({
            'GNU/Linux': {'flatpak': [], 'Ubuntu': ['Install now', 'ppa']},
            'Windows': {'64-bit': ['exe'], '32-bit': ['exe']},
            'Source': {'Version Control': ['git']},
        })
        flatpak = Platform.objects.get(codename='gnulinux/flatpak')
        ReleasePlatformFactory(release__version='1.0.2', platform=flatpak)
        ReleasePlatformFactory(release__version='1.0.1', platform=flatpak)
        ReleasePlatformFactory(release__version='1.0', platform=Platform.objects.get(codename='windows/64-bit/exe'))
        ReleasePlatformFactory(release__version='1.0.2', platform=Platform.objects.get(codename='windows/32-bit/exe'))
        ReleasePlatformFactory(release__version='1.0.x',
                               platform=Platform.objects.get(codename='source/version-control/git'))

    rp_test_data = [
        TestData(
            input=dict(version='1.0.2', platform='gnulinux/flatpak'),
            expected='', case=''
        ),
        TestData(
            input=dict(version='1.0.1', platform='gnulinux/flatpak'),
            expected='', case=''
        ),
        TestData(
            input=dict(version='1.0', platform='windows/64-bit/exe'),
            expected='', case=''
        ),
        TestData(
            input=dict(version='1.0.x', platform='source/version-control/git'),
            expected='', case=''
        ),
    ]

    def test_get_object_returns_correct_object(self):
        for test_data in self.rp_test_data:
            with self.subTest(data=test_data):
                with mock.patch.dict(self.url_kwargs, test_data.input):
                    view = self.get_view(ReleasePlatformView)
                    obj = view.get_object()
                    self.assertEqual(
                        obj,
                        ReleasePlatform.objects.get_by_natural_key(
                            test_data.input['platform'],
                            ('inkscape', test_data.input['version'])
                        )
                    )

    invalid_rp_test_data = [
        TestData(
            input=dict(version='1.0not', platform='windows/64-bit/exe'),
            expected='', case='Invalid version.'
        ),
        TestData(
            input=dict(version='1.0.1', platform='gnulinux/flatpakman'),
            expected='', case='Invalid platform.'
        ),
        TestData(
            input=dict(project='inskcape', version='1.0',
                       platform='gnulinux/flatpak'),
            expected='', case='Invalid Project Name.'
        ),
    ]

    def test_get_object_raises_404_with_incorrect_kwargs(self):
        for test_data in self.invalid_rp_test_data:
            with self.subTest(data=test_data):
                with self.assertRaises(Http404):
                    with mock.patch.dict(self.url_kwargs, test_data.input):
                        view = self.get_view(ReleasePlatformView)
                        view.get_object()
