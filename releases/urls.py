#
# Copyright 2015, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Releases allow inkscape to be downloaded."""

from django.urls import re_path
from inkscape.url_utils import url_tree

from .views import DownloadRedirect, ReleaseView, PlatformList,\
                   ReleasePlatformStage, ReleasePlatformView, ReleasePlatformDownload,\
                   PlatformView,\
                   GitMergeRequestList, GitMergeBrowse

app_name = 'releases'

version_urls = url_tree( # pylint: disable=invalid-name
    r'^(?P<version>[\w\+\.-]+)/',
    re_path('^$', ReleaseView.as_view(), name="release"),
    re_path(r'^platforms/$', PlatformList.as_view(), name="platforms"),

    # We don't use url_tree here because .+ competes with /dl/
    re_path('^(?P<platform>.+)/dl/$', ReleasePlatformView.as_view(), name="download"),
    re_path('^(?P<platform>.+)/dl/(?P<filename>.+)$', ReleasePlatformDownload.as_view(), name="download.direct"),
    re_path('^(?P<platform>.+)/$', ReleasePlatformStage.as_view(), name="platform"),
)

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^$', DownloadRedirect.as_view(), name="download"),
    re_path('^all/(?P<platform>.+)/$', PlatformView.as_view(), name="platform"),
    url_tree(
        r'^(?P<project>[\w\-\.]+)-',
        re_path('^inprogress/$', GitMergeRequestList.as_view(), name="inprogress"),
        re_path('^inprogress/(?P<sha>[0-9a-f]+)/(?P<build>.+)/', GitMergeBrowse.as_view(), name="inprogress-download"),
        version_urls,
    ),
    version_urls,
]
