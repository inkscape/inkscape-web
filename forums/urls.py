#
# Copyright 2016, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Forum views accessable from the website.
"""

from django.urls import re_path
from inkscape.url_utils import url_tree
from person import user_urls

from .views import (
    ModerationList, ForumList,
    TopicList, TopicDetail, TopicCreate, TopicMove, TopicEdit, TopicDelete,
    TopicMerge, TopicSplit, UserTopicList, UnreadTopicList,
    CommentList, CommentCreate, CommentEdit, CommentEmote, Subscriptions,
    CommentModList, CommentModPublic, CommentModRemove, CommentRaw,
    UserFlagList, UserModList, UserBanList, WordBanList,
    UserFlagToggle, UserModToggle, UserBanToggle, WordBanCreate, WordBanDelete,
    ForumStats, BulkWordFilter
)
from .rss import ForumTopicFeed

from .search_views import CommentSearch, TopicSearch, TopicSubjectSearch

app_name = 'forums'

user_urls.urlpatterns += [
    re_path(r'^comments/$', CommentList.as_view(), name="comment_list"),
    re_path(r'^topics/$', UserTopicList.as_view(), name="topic_list"),
    re_path(r'^topics/rss/$', ForumTopicFeed(), name='topic_feed'),
]

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^$', ForumList.as_view(), name="list"),
    re_path(r'^log/$', ModerationList.as_view(), name="log"),
    re_path(r'^stats/$', ForumStats.as_view(), name="stats"),
    url_tree(
        r'^users/',
        re_path(r'^banned/$', UserBanList.as_view(), name='ban_list'),
        re_path(r'^words/$', WordBanList.as_view(), name='word_list'),
        re_path(r'^mods/$', UserModList.as_view(), name='mod_list'),
        re_path(r'^flags/$', UserFlagList.as_view(), name='flag_list'),
        re_path(r'^banned/flag/$', UserBanToggle.as_view(), name='ban_user'),
        re_path(r'^mods/flag/$', UserModToggle.as_view(), name='mod_user'),
        re_path(r'^flags/flag/$', UserFlagToggle.as_view(), name='flag_user'),
        re_path(r'^words/create/$', WordBanCreate.as_view(), name='create_words'),
        re_path(r'^words/delete/(?P<pk>\d+)/$', WordBanDelete.as_view(), name='delete_words'),
    ),
    re_path(r'^unread/$', UnreadTopicList.as_view(), name="unread"),
    re_path(r'^check/$', CommentModList.as_view(), name="check"),
    re_path(r'^check/filter/$', BulkWordFilter.as_view(), name="word_filter"),
    re_path(r'^search/$', TopicSubjectSearch(), name='search'),
    re_path(r'^search/topics/$', TopicSearch(), name='search.topics'),
    re_path(r'^search/posts/$', CommentSearch(), name='search.posts'),
    re_path(r'^topics/$', TopicList.as_view(), name="topic_list"),
    re_path(r'^subscriptions/$', Subscriptions.as_view(), name="topic_subs"),
    re_path(r'^~(?P<username>[^\/]+)/rss/$', ForumTopicFeed(), name='topic_feed'),
    url_tree(
        r'^c(?P<pk>\d+)/',
        re_path(r'^$', CommentRaw.as_view(), name='comment_raw'),
        re_path(r'^emote/$', CommentEmote.as_view(), name='emote'),
        re_path(r'^edit/$', CommentEdit.as_view(), name='comment_edit'),
        re_path(r'^rem/$', CommentModRemove.as_view(), name='comment_remove'),
        re_path(r'^pub/$', CommentModPublic.as_view(), name='comment_public'),
    ),
    url_tree(
        r'^(?P<slug>[\w-]+)/',
        re_path(r'^$', TopicList.as_view(), name='topic_list'),
        re_path(r'^rss/$', ForumTopicFeed(), name='topic_feed'),
        re_path(r'^new/$', TopicCreate.as_view(), name='create'),
        re_path(r'^move/$', TopicMove.as_view(), name='topic_move'),
        re_path(r'^edit/$', TopicEdit.as_view(), name='topic_edit'),
        re_path(r'^del/$', TopicDelete.as_view(), name='topic_delete'),
        re_path(r'^merge/$', TopicMerge.as_view(), name='topic_merge'),
        re_path(r'^split/$', TopicSplit.as_view(), name='topic_split'),
    ),
    url_tree(
        r'^(?P<forum>[\w-]+)/(?P<slug>[\w-]+)/',
        re_path(r'^$', TopicDetail.as_view(), name='topic'),
        re_path(r'^comment/$', CommentCreate.as_view(), name='comment_create'),
    ),
]
