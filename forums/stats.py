#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Methods for getting forum statistics
"""

from django.db.models import F, Count
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from stats.utils import get_stats
from stats.base import StatisticBase, DateCountBase

from person.models import User
from .models import ModerationLog, ForumTopic, Comment

class TopHelpers(StatisticBase):
    """Count the top helper users"""
    title = _("Top Forum Helpers")
    template_name = 'stats/top_helpers.html'

    def get_data(self):
        for dat in ForumTopic.objects\
            .exclude(first_username=F('last_username'))\
            .values('last_username')\
            .annotate(count=Count('last_username'))\
            .order_by('-count')[:20]:
            try:
                user = User.objects.get(username=dat['last_username'])
            except user.DoesNotExist:
                user = User(username=dat['last_username'], pk=0)
            yield {
                'user': user,
                'count': dat['count'],
            }

    def filter_data(self, gen):
        return {'helpers': list(gen)}

class CountNewPosts(DateCountBase):
    """Count new users posting to the forum"""
    date_field = 'performed'
    title = _("New User Posts")

    def get_data(self):
        return ModerationLog.objects.filter(action='CommentModPublic')

class CountSpamPosts(DateCountBase):
    """Count new users posting to the forum"""
    date_field = 'performed'
    title = _("Spam Posts Removed (manually)")

    def get_data(self):
        return ModerationLog.objects.filter(action='CommentModRemove')

class CountInstantBans(DateCountBase):
    date_field = 'performed'
    title = _("Word Based Instant Bans")

    def get_data(self):
        return ModerationLog.objects.filter(action='UserInstantBan')

class CountNewTopics(DateCountBase):
    date_field = 'first_posted'
    title = _("New Topics")

    def get_data(self):
        return ForumTopic.objects.filter(removed=False)

class CountAllPosts(DateCountBase):
    date_field = 'submit_date'
    title = _("All Posts")

    def get_data(self):
        ctype = ContentType.objects.get_for_model(ForumTopic)
        return Comment.objects.filter(content_type=ctype)

