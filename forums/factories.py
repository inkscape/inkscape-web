import django_comments.models
import factory
from django.utils.text import slugify
from factory.django import DjangoModelFactory

import forums.models


class BannedWordsFactory(DjangoModelFactory):
    class Meta:
        model = forums.models.BannedWords
    in_title = True
    in_body = True
    new_user = False


class ModerationLogFactory(DjangoModelFactory):
    class Meta:
        model = forums.models.ModerationLog


class ForumGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = forums.models.ForumGroup
        django_get_or_create = ('name', )
    name = factory.Faker('word')


class ForumFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = forums.models.Forum
        django_get_or_create = ('slug', )

    name = factory.Faker('word')
    group = factory.SubFactory(ForumGroupFactory)
    slug = factory.LazyAttribute(lambda o: slugify(o.name))


class ForumTopicFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = forums.models.ForumTopic
        django_get_or_create = ('slug', )

    subject = factory.Faker('text', max_nb_chars=70)
    slug = factory.LazyAttribute(lambda o: slugify(o.subject))
    forum = factory.SubFactory(ForumFactory)


class CommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = django_comments.models.Comment
        exclude = ['topic']

    class Params:
        topic = factory.Trait(content_object=factory.LazyAttribute(lambda o: o.topic))

    user = factory.SubFactory('person.factories.UserFactory')
    user_name = factory.SelfAttribute('.user.username')
    content_object = factory.SubFactory(ForumTopicFactory)
    site_id = 1


class CommentFlagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = django_comments.models.CommentFlag

    comment = factory.SubFactory(CommentFactory)
    user = factory.SubFactory('person.factories.UserFactory')


class UserFlagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = forums.models.UserFlag
        django_get_or_create = ('user', 'flag')

    class Params:
        moderator = factory.Trait(flag=forums.models.UserFlag.FLAG_MODERATOR)
        banned = factory.Trait(flag=forums.models.UserFlag.FLAG_BANNED)

    user = factory.SubFactory('person.factories.UserFactory')


class ForumTopicAlertFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = forums.models
