#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for forms of forums app."""

from datetime import datetime, timedelta
from unittest import mock

from alerts.models import AlertSubscription
from django.contrib.auth.models import Group
from django.forms import ValidationError
from django.test import TestCase
from django.utils import timezone
from datetime import UTC
from django_comments.forms import CommentForm
from django_comments.models import Comment
from person.models import Team, User
from resources.factories import CategoryFactory
from resources.models import Category, Resource
from testing import base
from testing.base import TestData
from testing.factories import AlertTypeFactory
from testing.mixins import GetTestUsersMixin
from ..factories import BannedWordsFactory, ForumFactory, ForumTopicFactory, CommentFactory

from ..forms import (COMMENT_EDITED, MENTION, MENTION_FIX, AddCommentForm,
                     AttachmentMixin, CommentFlagForm, EditCommentForm,
                     MergeTopics, NewTopicForm, SplitTopic, fix_mention,
                     replace_mention)
from ..models import CommentAttachment, ForumTopic, UserFlag
from ..utils import censor_text

# The timezone.now that should be patched for tests
forms_now_target = 'forums.forms.now'


class TestReplaceMention(TestCase):

    test_data = [
        TestData(input=r"Hi @thedoctor, I can't seem to add rectangles.",
                 expected=r"""Hi <a href="/~thedoctor/">@thedoctor</a>, I can't seem to add rectangles.""",
                 case="Existing user can be mentioned"),
        TestData(input=r"@earthling could you send a screenshot? I am available at mars@example.com",
                 expected=r"""<a href="/~earthling/">@earthling</a> could you send a screenshot? I am available at mars@example.com""",
                 case="Mention can be at the start of the line. Emails should not be converted"),
        TestData(input=r"<p>@earthling</p>",
                 expected=r"""<p><a href="/~earthling/">@earthling</a></p>""",
                 case="Just mention as the comment should work"),
        TestData(input=r"Please flag this person @mods.",
                 expected=r"""Please flag this person <a href="/*mods/">@mods</a>.""",
                 case="A team can be mentioned"),
        TestData(input=r"You are not a real user @notarealuser",
                 expected=r"You are not a real user @notarealuser",
                 case="Non-existing user cannot be mentioned. Leave the mention untouched")
    ]

    @classmethod
    def setUpTestData(cls):
        for username in ('thedoctor', 'gallifreyer', 'earthling'):
            User.objects.create(username=username)

        # Create a new Mods team
        mod = Group.objects.create(name="Mods")
        Team.objects.create(name="Mods", group=mod)

    def test_replace_mention(self):
        for test_input, expected, case in self.test_data:
            with self.subTest(test_input=test_input, expected=expected, case=case):
                output = MENTION.sub(replace_mention, test_input)
                self.assertEqual(output, expected)


class TestFixMention(TestCase):

    test_data = [
        TestData(input=r'<p><a href="~@earthling">@earthling</a></p>',
                 expected=r'<p><a href="/~earthling/">@earthling</a></p>',
                 case="Base case for the regex"),
        TestData(input=r'~@notarealuser', expected='',
                 case="Remove mention of non-existing user")
    ]

    @classmethod
    def setUpTestData(cls):
        User.objects.create(username='earthling')

    def test_fix_mention(self):
        for test_input, expected, case in self.test_data:
            with self.subTest(test_input=test_input, expected=expected, case=case):
                output = MENTION_FIX.sub(fix_mention, test_input)
                self.assertEqual(output, expected)


class TestAttachmentMixin(GetTestUsersMixin, TestCase):
    FORUM_NOT_READY = True

    @classmethod
    def setUpTestData(cls):
        # Set attributes that the mixin expects to be there
        cls.user = cls.get_test_user()
        cls.cleaned_data = {
            'comment': '<p> Hello Inkscape users...<p>'
        }

    def get_mixin_instance(self):
        mixin = AttachmentMixin()
        mixin.user = self.user
        mixin.cleaned_data = self.cleaned_data
        return mixin

    @mock.patch('forums.forms.FORUM_NOT_READY', True)
    def test_clean_comment_throws_error_when_forum_not_ready(self):
        with self.assertRaises(ValidationError) as err:
            mixin = self.get_mixin_instance()
            mixin.clean_comment()

        # Check that the error was the correct one
        self.assertEqual(err.exception.message,
                         "The Forum is not open yet. Please post your"
                         " question in other parts of the website.")

    def test_user_cannot_multipost_when_has_unmoderated_comments(self):
        # Make all comments of the test user unmoderated
        CommentFactory.create_batch(2, user=self.get_test_user(), is_public=False)

        with self.assertRaises(ValidationError) as err:
            mixin = self.get_mixin_instance()
            mixin.clean_comment()

        # Check that the error was the correct one
        self.assertEqual(err.exception.message,
                         "You can not post more"
                         " than 2 comments awaiting moderation")

    def test_banned_user_cannot_comment(self):
        # Ban the user
        flag = UserFlag.objects.create(
            user=self.user,
            flag=UserFlag.FLAG_BANNED,
            title='Banned'
        )
        self.user.forum_flags.add(flag)

        with self.assertRaises(ValidationError) as err:
            mixin = self.get_mixin_instance()
            mixin.clean_comment()

        # Check that the error was the correct one
        self.assertEqual(err.exception.message,
                         "You have been banned from posting to this forum!")

    emoji_test_data = [
        TestData(input='🤖 Test Robot',
                 expected='🤖 Test Robot',
                 case="robot_face emoji not in expected range. Shouldn't modify the string"),
        TestData(input='Hehe, I did it wrong 😀🙃',
                 expected=r'Hehe, I did it wrong <span class="emoji">😀</span><span class="emoji">🙃</span>',
                 case="Common emojis. Regex should change the string"),
        TestData(input='🐧🥶❄',
                 expected=r'<span class="emoji">🐧</span>🥶<span class="emoji">❄</span>',
                 case="cold_face not in expected range. Rest all emojis should be changed")
    ]

    def test_emoji_fix(self):
        mixin = AttachmentMixin()
        mixin.user = self.user
        for test_input, expected, case in self.emoji_test_data:
            mixin.cleaned_data = {'comment': test_input}
            with self.subTest(test_input=test_input, expected=expected, case=case):
                output = mixin.clean_comment()
                self.assertEqual(output, expected)

    censor_test_data = [
        TestData(input=dict(title='Tacos are the best',
                            body='Do folks like tacos here?'),
                 expected='',
                 case="Banned word in title"),
        TestData(input=dict(title='I like pizzas though...',
                            body='Do you like cheesetacos?'),
                 expected='',
                 case="Banned word in body and instant ban"),
    ]

    def test_censor_text(self):
        BannedWordsFactory(phrase="tacos", ban_user=False)
        BannedWordsFactory(phrase="cheesetacos", ban_user=True)
        mixin = AttachmentMixin()
        mixin.user = self.get_test_user()

        # Banned word in title
        title, body = self.censor_test_data[0].input.values()
        with self.assertRaises(ValidationError) as err:
            censor_text(mixin.user, title, body)

        # Check that the error message is correct
        self.assertEqual(err.exception.message, "Post has been blocked.")

        # Instant ban
        title, body = self.censor_test_data[1].input.values()
        with self.assertRaises(ValidationError) as err:
            censor_text(mixin.user, title, body)

        # Check that the error message is correct
        self.assertEqual(err.exception.message,
                         "Instant ban! Please contact the moderators for help.")

    # No need to test attachments here, they're separately tested below


class AlertCleanupMixin:
    def _fixture_teardown(self):
        AlertSubscription.objects.all().delete()
        super()._fixture_teardown()


class TestAddCommentForm(AlertCleanupMixin, GetTestUsersMixin, base.FormTestCase,):
    form = AddCommentForm
    field_names = ['comment', 'attachments', 'galleries', 'embedded']
    topic = {'slug': 'try-vectors'}

    @classmethod
    def setUpTestData(cls):
        # Create three resources for the tests
        cls.resources = [Resource.objects.create(
            name=f'Resource {num}', user=cls.get_test_user())
            for num in range(3)]
        cls.attachments_category = CategoryFactory(slug='fat')
        topic = ForumTopicFactory(slug=cls.topic['slug'])

        cls.form_args = {
            'user': cls.get_test_user(),
            'ip_address': '127.0.0.1',
            'target_object': topic,
        }

        # django_comments specific data
        extra_data = CommentForm(topic).generate_security_data()

        # Create valid data for the form and add different types of attachments
        cls.form_data = {
            **extra_data,
            'attachments': f'{cls.resources[0].pk}',
            'galleries': f'{cls.resources[1].pk}',
            'embedded': f'{cls.resources[2].pk}',
            'comment': "Heyaa! This art is awesome."
        }

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        form = self.form(**self.form_args, data=self.form_data)
        self.assertTrue(form.is_valid())

    def test_can_submit_form_without_attachments_galleries_and_embedded(self):
        # Clear all types of attachments
        with mock.patch.dict(self.form_data, attachments=None, galleries=None, embedded=None):
            form = self.form(**self.form_args, data=self.form_data)
            # No errors should be thrown
            self.assertFalse(form.errors)

    def test_form_throws_error_when_forum_attachment_category_not_present(self):
        # Delete the Forum Attachment Category
        Category.objects.get(slug='fat').delete()
        form = self.form(**self.form_args, data=self.form_data)

        self.assertOnlyThisFieldError(
            form, field_name='attachments',
            error_msg="Category for forum attachments doesn't exist,"
            " refusing to allow forum attachments! (please ask the"
            " website admin to enable attachments)")

    def test_cannot_add_comment_to_locked_topic(self):
        # Lock the forum topic
        topic = ForumTopic.objects.get(slug=self.topic['slug'])
        topic.locked = True
        topic.save(update_fields=['locked'])

        form = self.form(**self.form_args, data=self.form_data)

        self.assertOnlyThisFieldError(form, field_name='object_pk',
                                      error_msg="This comment thread is locked.")

    def test_save(self):
        user = self.get_test_user()
        form = self.form(**self.form_args, data=self.form_data)

        form.is_valid()
        # Check that the comment actually got saved
        comment = form.save()
        self.assertEqual(comment, Comment.objects.last())

        expected_values = {
            'user_name': user.username,
            'email': user.email,
            'url': 'R',
            'comment': '<html><head></head><body>' +
            self.form_data['comment'] + '</body></html>',
            'user': user,
            'ip_address': self.form_args['ip_address'],
            'is_public': False  # The test user does not have required permission
        }

        # Check that each of the fields were correctly set (independently from other fields)
        for field, value in expected_values.items():
            with self.subTest():
                self.assertEqual(actual := getattr(comment, field), value,
                                 f"Invalid value for {field}: expected '{value}', actual '{actual}'")


class TestEditCommentForm(GetTestUsersMixin, base.FormTestCase):
    form = EditCommentForm
    field_names = ['comment', 'attachments', 'galleries', 'embedded']

    @classmethod
    def setUpTestData(cls):
        cls.attachments_category = CategoryFactory(slug='fat')
        # The comment to use in tests
        cls.comment = CommentFactory()

        # Create some new resources so that we can attach them to the comment
        resources = [Resource.objects.create(
            name=f'Resource {num}', user=cls.get_test_user())
            for num in range(4)]

        # Mark two new resources as galleries one as embedded and one as attachment
        galleries = resources[:2]
        embedded = resources[2]
        attachments = resources[3]

        CommentAttachment.objects.create(
            comment=cls.comment, resource=galleries[0], inline=1)
        CommentAttachment.objects.create(
            comment=cls.comment, resource=galleries[1], inline=1)
        CommentAttachment.objects.create(
            comment=cls.comment, resource=embedded, inline=2)
        CommentAttachment.objects.create(
            comment=cls.comment, resource=attachments, inline=0)

        cls.form_args = {
            'user': cls.get_test_user(),
            'grace': timedelta(days=1),
            'instance': cls.comment,
        }

        # Create valid data for the form
        cls.form_data = {
            'attachments': f'{attachments.pk}',
            'galleries': f'{galleries[0].pk},{galleries[1].pk}',
            'embedded': f'{embedded.pk}',
            'comment': "Heyaa! This art is awesome."
        }

    def setUp(self):
        super().setUp()

        # Freeze the datetime for tests so that the comment edit date
        # is within grace period
        within_grace_period = datetime(2021, 5, 24, 10, 10, 10, tzinfo=UTC)
        patcher = mock.patch(forms_now_target, return_value=within_grace_period)
        patcher.start()
        self.addCleanup(patcher.stop)

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        # Also checks that user can edit comment within grace period
        form = self.form(**self.form_args, data=self.form_data)
        self.assertTrue(form.is_valid())

    def test_user_cannot_edit_comment_after_grace_period(self):
        after_grace_period = timezone.now() + timedelta(days=10)

        # Freeze datetime to a date after grace period
        with mock.patch(forms_now_target, return_value=after_grace_period):
            form = self.form(**self.form_args, data=self.form_data)
            self.assertOnlyThisFieldError(
                form, field_name='__all__', error_msg="You can not edit this post"
                " after the grace period. Please contact an administrator for help.")

    def test_moderator_can_edit_comment_after_grace_period(self):
        # Make sure moderator is accessing the form
        with mock.patch.dict(self.form_args, {'user': self.get_test_moderator()}):
            after_grace_period = datetime(2021, 6, 4, 10, 10, 10, tzinfo=UTC)

            # Freeze datetime to a date after grace period
            with mock.patch(forms_now_target, return_value=after_grace_period):
                form = self.form(**self.form_args, data=self.form_data)
                self.assertTrue(form.is_valid())

    def test_can_edit_comment_attachments(self):

        # Pretend that the user deleted the attachment and embedded attachment
        new_attachment_pks = {
            'attachments': None,
            'embedded': None
        }

        with mock.patch.dict(self.form_data, new_attachment_pks):
            form = self.form(**self.form_args, data=self.form_data)
            self.assertFalse(form.errors)  # this way you can see what errors are raised when invalid
            form.save()

        # Check that the attachment and embedded attachment were removed
        for inline in (0, 2):
            with self.subTest(inline=inline):
                result = self.comment.attachments.filter(inline=inline)
                self.assertQuerysetEqual(result, [])

    def test_save(self):
        form = self.form(**self.form_args, data=self.form_data)
        form.is_valid()
        comment = form.save()

        expected_values = {
            'comment': '<html><head></head><body>' +
            self.form_data['comment'] + '</body></html>'
        }

        self.assertEqual(comment.comment, expected_values['comment'])

        flags = [user_flag.flag for user_flag in comment.flags.filter(
            user=self.form_args['user'])]

        # Check that the edited flag was set for this comment for the test user
        self.assertIn(COMMENT_EDITED, flags)


class TestNewTopicForm(AlertCleanupMixin, GetTestUsersMixin, base.FormTestCase):
    form = NewTopicForm
    field_names = ['subject', 'comment',
                   'attachments', 'galleries', 'embedded']
    forum = {'slug': 'cafe'}
    new_topic = {'name': 'Get Inkscape 1.1', 'slug': 'get-inkscape-11'}

    @classmethod
    def setUpTestData(cls):
        # Use this forum for creating a new topic
        forum = ForumFactory(slug=cls.forum['slug'])
        cls.attachments_category = CategoryFactory(slug='fat')

        # Create a single resource to attach to the comment
        resource = Resource.objects.create(
            name='Resource 1', user=cls.get_test_user())

        cls.form_args = {
            'user': cls.get_test_user(),
            'ip_address': '127.0.0.1',
            'target_object': forum,
        }

        # django_comments specific data
        extra_data = CommentForm(forum).generate_security_data()

        # Create valid data for the form
        cls.form_data = {
            **extra_data,
            'attachments': f'{resource.pk}',
            'galleries': None,
            'embedded': None,
            'subject': cls.new_topic['name'],
            'comment': 'Heyaa! This art is awesome.'
        }

    def test_form_does_not_have_fields(self):
        form = self.form(**self.form_args, data=self.form_data)

        for field in ('url', 'name', 'email'):
            with self.subTest(field=field):
                with self.assertRaises(KeyError):
                    form.fields[field]

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        form = self.form(**self.form_args, data=self.form_data)
        self.assertFalse(form.errors)

    def test_form_has_extra_fields_when_used_by_moderator(self):
        fields = ['locked', 'sticky', 'injected']
        mod = self.get_test_moderator()
        # Make sure moderator is accessing the form
        with mock.patch.dict(self.form_args, {'user': mod}):
            form = self.form(**self.form_args, data=self.form_data)
            for field in fields:
                # Check that the field was added to form
                # (independently of other fields)
                with self.subTest(field=field):
                    self.assertIn(field, form.fields.keys())

    # TODO: Check for injections

    def test_save(self):
        form = self.form(**self.form_args, data=self.form_data)
        form.is_valid()

        # Check that the topic was created
        topic = form.save()
        self.assertEqual(topic, ForumTopic.objects.get(
            slug=self.new_topic['slug']))

        expected_values = {
            'subject': self.new_topic['name'],
            'locked': True,  # The test user does not have required permission
            'has_attachments': True
        }

        # Check that each of the fields were correctly set (independently from other fields)
        for field, value in expected_values.items():
            with self.subTest(field=field):
                self.assertEqual(getattr(topic, field), value)

        comment = Comment.objects.last()
        topic_comment = topic.comments.first()

        # Check that the comment was added to the topic
        self.assertEqual(comment, topic_comment)


class TestSplitTopic(base.FormTestCase):
    form = SplitTopic
    field_names = ['new_name', 'comments']
    source_topic = {'slug': 'try-vectors'}
    new_topic = {'name': 'Hello humans!', 'slug': 'hello-humans'}

    @classmethod
    def setUpTestData(cls):
        cls.topic = ForumTopicFactory(slug=cls.source_topic['slug'])

        cls.form_args = {
            'from_topic': cls.topic
        }

        # Get 2 comments for form data
        comments = CommentFactory.create_batch(3, content_object=cls.topic)

        # Create valid data for the form
        cls.form_data = {
            'new_name': cls.new_topic['name'],
            'comments': [x.pk for x in comments[:2]]
        }

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        form = self.form(**self.form_args, data=self.form_data)
        self.assertFalse(form.errors)

    def test_form_throws_error_when_all_comments_selected(self):
        # Get all the comments
        pk_list = [comment.pk for comment in self.topic.comments.all()]

        with mock.patch.dict(self.form_data, {'comments': pk_list}):
            form = self.form(**self.form_args, data=self.form_data)
            self.assertOnlyThisFieldError(
                form, field_name='comments',
                error_msg="You can not select every comment to split!")

    def test_save(self):
        form = self.form(**self.form_args, data=self.form_data)
        form.is_valid()

        topic = form.save()
        new_topic = ForumTopic.objects.get(slug=self.new_topic['slug'])

        # Check that the topic got saved
        self.assertEqual(topic, new_topic)

        # Check that the comments are in new topic
        pk_list = [comment.pk for comment in topic.comments.all()]
        for comment_pk in self.form_data['comments']:
            self.assertIn(comment_pk, pk_list)


class TestMergeTopics(base.FormTestCase):
    form = MergeTopics
    field_names = ['with_topic']
    from_topic = {'slug': 'try-vectors'}
    with_topic = {'slug': 'new-sunday-challenge'}

    @classmethod
    def setUpTestData(cls):
        cls.topic = ForumTopicFactory(slug=cls.from_topic['slug'])

        cls.form_args = {
            'from_topic': cls.topic
        }

        # Create valid data for the form
        cls.form_data = {
            'with_topic': ForumTopicFactory(slug=cls.with_topic['slug']).pk
        }

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        form = self.form(**self.form_args, data=self.form_data)
        self.assertTrue(form.is_valid())

    def test_form_throws_error_when_merged_with_same_topic(self):
        # Make the with_topic same as from_topic
        with mock.patch.dict(self.form_data, {'with_topic': self.form_args['from_topic'].pk}):
            form = self.form(**self.form_args, data=self.form_data)
            self.assertOnlyThisFieldError(
                form, field_name='with_topic',
                error_msg='Merged topics must be different.')


class TestCommentFlagForm(base.FormTestCase):
    form = CommentFlagForm
    field_names = ['flag']

    form_args = {}
    form_data = {'flag': '🤖'}

    def test_form_is_valid_when_instantiated_with_valid_data(self):
        form = self.form(**self.form_args, data=self.form_data)
        self.assertTrue(form.is_valid())

    def test_form_throws_error_when_reserved_flag_used(self):
        with mock.patch.dict(self.form_data, {'flag': '*'}):
            form = self.form(data=self.form_data)
            self.assertOnlyThisFieldError(
                form, field_name='flag', error_msg="Reserved Flag can not be set!")
