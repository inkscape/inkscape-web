#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
"""Tests for middleware of forums app."""

from collections import OrderedDict
from datetime import datetime, timedelta
from unittest import mock

from django.core.cache import caches
from django.test import Client, TestCase, override_settings
from django.urls import reverse
from datetime import UTC
from testing.base import TestData
from testing.mixins import GetTestUsersMixin
from testing.utils import clear_cache

from ..middleware import RecentUsersMiddleware

# The timezone.now that should be patched for tests
middleware_now_target = 'forums.middleware.now'


class MiddlewareMixin:
    # Bare minimum middlewares required
    middlewares = [
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
        'forums.middleware.RecentUsersMiddleware',
    ]

    def send_request(self, *args, **kwargs):
        """Allow client to send requests from custom ip addresses."""
        login_user = kwargs.get('login_user', None)
        ip_address = kwargs.get('ip_address', '127.0.0.1')
        url = kwargs.get('url', reverse('forums:list'))

        client = Client(REMOTE_ADDR=ip_address)
        if login_user:
            client.force_login(login_user)

        response = client.get(url)

        return response

    def generate_fake_request(self, ip_address='127.0.0.1'):
        """Generate fake requests for unit testing the middleware methods."""
        request = mock.Mock(spec_set=['META'])
        request.META = {
            'REMOTE_ADDR': ip_address,
            'HTTP_X_FORWARDED_FOR': None,
        }
        return request


class TestRecentUsersMiddleware(GetTestUsersMixin, MiddlewareMixin, TestCase):

    middlewares = MiddlewareMixin.middlewares

    # This middleware uses the cache, so we replace the cache
    # with a dummy one. This way, each test can be isolated
    # since the dummy cache does not store anything. There is also
    # no need to clear this cache.
    cache_name = 'default'
    cache = {
        cache_name: {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache'
        },
    }

    # Settings that the tests should use
    new_settings = {
        'MIDDLEWARE_CLASSES': middlewares,
        'CACHE_MIDDLEWARE_ALIAS': cache_name,
        'CACHES': cache,
    }

    @override_settings(**new_settings)
    def test_middleware_adds_lurker_to_context(self):
        response = self.send_request()
        # Check that the middleware adds correct context data
        self.assertIn('visitors', response.context.keys())

        request = self.generate_fake_request()
        lurker_ip = RecentUsersMiddleware().get_ip_pk(request)

        online_users = response.context['visitors']

        # We expect only one user to be online
        self.assertEqual(len(online_users), 1)

        # Confirm that the user is a lurker and not a logged in user
        self.assertNotIn('username', online_users[lurker_ip].keys())

    @override_settings(**new_settings)
    def test_middleware_adds_logged_user_to_context(self):
        response = self.send_request(
            login_user=self.get_test_user(),
        )

        # Check that the middleware adds correct context data
        self.assertIn('visitors', response.context.keys())

        online_users = response.context['visitors']

        # We expect only one user to be online
        self.assertEqual(len(online_users), 1)

        # Confirm that the user is a logged in user
        self.assertIn('username', online_users[self.get_test_user().pk].keys())

    @override_settings(**new_settings)
    def test_middleware_behavior_on_other_urls_than_forums_for_lurkers(self):
        # Send a request to any other url other than forums
        response = self.send_request(url='/')

        online_visitors = response.context['visitors']

        # Check that the middleware did not add anything to context
        self.assertEqual(len(online_visitors), 0)

    @override_settings(**new_settings)
    def test_middleware_behavior_on_other_urls_than_forums_for_logged_in_users(self):
        user = self.get_test_user()

        # Send a request to any other url other than forums
        response = self.send_request(login_user=user, url='/')

        online_visitors = response.context['visitors']

        # Check that the middleware adds user to context
        self.assertEqual(len(online_visitors), 1)
        self.assertIn(user.pk, online_visitors)

    ip_test_data = [
        TestData(input='127.0.0.1', expected=2130706433, case=''),
        TestData(input='192.168.1.1', expected=3232235777, case=''),
        TestData(input='172.189.2.1', expected=2898067969, case=''),
        TestData(input='255.255.255.255', expected=4294967295, case=''),
        TestData(input='0.0.0.0', expected=0, case=''),
    ]

    def test_get_ip_pk(self):
        for test_input, expected, _ in self.ip_test_data:
            with self.subTest(test_input=test_input, expected=expected):
                request = self.generate_fake_request(ip_address=test_input)
                ip_address = RecentUsersMiddleware().get_ip_pk(request)
                self.assertEqual(ip_address, expected)

    at_time = datetime(2021, 7, 2, 10, 10, 10, tzinfo=UTC)

    @override_settings(**new_settings)
    def test_set_visitor(self):
        request = self.generate_fake_request(ip_address='172.189.2.1')
        ip_pk = RecentUsersMiddleware().get_ip_pk(request)
        user = self.get_test_user()

        returned_visitors = RecentUsersMiddleware()\
            .set_visitor(user, self.at_time, ip_pk)

        expected_visitors = OrderedDict(
            {
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'photo_url': user.photo_url(),
                'last_seen': self.at_time,
                'ip_pk': ip_pk,
            }
        )

        # Check that set_visitor inserted correct values in dict
        self.assertIn(user.pk, returned_visitors)
        for visitor in returned_visitors.values():
            for key, value in visitor.items():
                with self.subTest(key=key, value=value):
                    self.assertEqual(expected_visitors[key], value)

    @override_settings(**new_settings)
    def test_set_lurker(self):
        request = self.generate_fake_request(ip_address='172.189.2.1')
        ip_pk = RecentUsersMiddleware().get_ip_pk(request)

        returned_visitors = RecentUsersMiddleware()\
            .set_lurker(ip_pk, self.at_time)

        expected_visitors = OrderedDict(
            {
                'ip_pk': ip_pk,
                'last_seen': self.at_time,
            }
        )

        # Check that set_lurker inserted correct values in dict
        self.assertIn(ip_pk, returned_visitors)
        for visitor in returned_visitors.values():
            for key, value in visitor.items():
                with self.subTest(key=key, value=value):
                    self.assertEqual(expected_visitors[key], value)


class TestRecentUsersMiddlewareWithCaching(
        GetTestUsersMixin, MiddlewareMixin, TestCase
):
    middlewares = MiddlewareMixin.middlewares

    # We use the default local cache for tests because we want to check for
    # multiple users whose data is stored in the cache.
    cache_name = 'default'
    cache = {
        cache_name: {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        },
    }

    # Settings that the tests should use
    new_settings = {
        'MIDDLEWARE_CLASSES': middlewares,
        'CACHE_MIDDLEWARE_ALIAS': cache_name,
        'CACHES': cache,
    }

    @clear_cache(caches[cache_name])
    @override_settings(**new_settings)
    def test_middleware_can_add_multiple_users_to_context(self):
        # First a logged in user visits the forums from a system
        user = self.get_test_user()
        self.send_request(
            login_user=user,
            ip_address='200.200.200.100',
        )

        # Then a lurker visits the forums
        self.send_request()

        # At the same time, a moderator visits the forums
        mod = self.get_test_moderator()
        response = self.send_request(
            login_user=mod,
            ip_address='165.255.200.100',
        )

        online_users = response.context['visitors']

        # There should be three online users now
        self.assertEqual(len(online_users), 3)

        # Check that the test user and mod are in online users
        for user in (user, mod):
            with self.subTest(user=user):
                self.assertIn(user.pk, online_users)

        # Also check there is one lurker online
        self.assertEqual(response.context['lurkers'], 1)

    @clear_cache(caches[cache_name])
    @override_settings(**new_settings)
    def test_middleware_ignores_echoes_of_logged_off_users(self):
        # First a logged in user visits the forums from an ip address
        user = self.get_test_user()
        self.send_request(login_user=user)

        # Then the user logs out and visits the forums from the same
        # ip address
        response = self.send_request()

        online_users = response.context['visitors']

        # Check that only one user is online
        self.assertEqual(len(online_users), 1)

        # Check that the online user is the test user

    @clear_cache(caches[cache_name])
    @override_settings(**new_settings)
    def test_middleware_does_not_add_very_old_visitors_to_context(self):
        login_date = datetime(2021, 7, 2, 10, 10, 10, tzinfo=UTC)

        # A user logs in at login_time
        with mock.patch(middleware_now_target, return_value=login_date):
            self.send_request(login_user=self.get_test_user())

        # After 2 hours a lurker visits the forum from their system
        lurker_login_date = login_date + timedelta(hours=2)
        response = None
        with mock.patch(middleware_now_target, return_value=lurker_login_date):
            response = self.send_request(ip_address='172.189.2.1')

        request = self.generate_fake_request(ip_address='172.189.2.1')
        ip_pk = RecentUsersMiddleware().get_ip_pk(request)

        online_visitors = response.context['visitors']

        # Check that only the lurker is online
        self.assertEqual(len(online_visitors), 1)
        self.assertIn(ip_pk, online_visitors)
