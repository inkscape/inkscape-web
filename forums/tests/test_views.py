#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for views of forums app."""

from http import HTTPStatus
from unittest import mock, skip

from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django_comments.models import Comment, CommentFlag

from person.factories import UserFactory
from person.models import User
from testing import base
from testing.factories import AlertSubscriptionFactory
from testing.mixins import (GetTestUsersMixin, GetViewMixin,
                            ModeratorLoginMixin, TestUserLoginMixin)
from .test_forms import AlertCleanupMixin
from .utils import ForumDataSetupMixin
from ..factories import ForumFactory, ForumTopicFactory, CommentFactory, CommentFlagFactory, UserFlagFactory, \
    BannedWordsFactory

from ..models import BannedWords, Forum, ForumTopic, UserFlag
from ..views import (CommentCreate, CommentEdit, CommentEmote, CommentList,
                     CommentModList, CommentModPublic, CommentModRemove,
                     ForumList, ForumStats, Subscriptions, TopicCreate,
                     TopicDetail, TopicList, TopicMerge,
                     UnreadTopicList, UserBanList, UserBanToggle, UserFlagList, UserFlagToggle, UserModList,
                     UserModToggle, UserTopicList, WordBanCreate,
                     WordBanDelete, WordBanList)


class TestForumList(GetTestUsersMixin, base.ViewTestCase):
    url_name = 'forums:list'
    template_name = 'forums/forum_list.html'
    url_kwargs = {}

    def create_new_forum_topics(self, number_of_topics):
        """Create some number of topics and return a list of them."""
        topics = []
        forum = Forum.objects.get(slug="cafe")
        for num in range(number_of_topics):
            topic = ForumTopic(subject=f'New Topic {num}', forum=forum)
            topic.save()
            topics.append(topic)
        # Reversed because it is easier to compare
        return list(reversed(topics))

    def test_get_context_data(self):
        topics = self.create_new_forum_topics(15)
        # Send in a dummy request and get the result
        user = self.get_test_user()
        view = self.get_view(ForumList, user=user)
        result = view.get_context_data()
        self.assertIsNotNone(result)
        # Compare the result with 10 most recent topics
        self.assertQuerysetEqual(result['recent_topics'], topics[:10])


class TestModerationList(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:log'
    template_name = 'forums/moderationlog_list.html'
    url_kwargs = {}


class TestTopicList(base.ViewTestCase, ForumDataSetupMixin):
    view_url = '/forums/cafe/'
    url_name = 'forums:topic_list'
    url_kwargs = {'slug': 'cafe'}
    template_name = 'forums/forumtopic_list.html'

    def test_get_queryset_with_count_url_parameter_and_slug(self):
        view = self.get_view(TopicList, get_params={'count': 3})
        result = view.get_queryset()
        topic = ForumTopic.objects.get(slug='try-vectors')
        self.assertQuerysetEqual(result, [topic])

    @mock.patch.dict(url_kwargs, {}, clear=True)  # Empty url_kwargs
    def test_get_queryset_with_only_count_url_parameter(self):
        view = self.get_view(TopicList, get_params={'count': 3})
        topics = ForumTopic.objects.filter(
            post_count=3).order_by('-last_posted')
        result = view.get_queryset()
        self.assertQuerysetEqual(result, topics)

    # TODO:
    # def test_get_moved_topic(self):


class TestUserTopicList(base.ViewTestCase):
    view_url = '/~tester/topics/'
    url_name = 'topic_list'
    url_kwargs = {'username': 'tester'}
    template_name = 'forums/forumtopic_list_user.html'

    def setUp(self):
        self.user = UserFactory(username=self.url_kwargs['username'])
        self.topic = ForumTopicFactory(first_username=self.user.username)
        super().setUp()

    def test_get_queryset_with_username(self):
        view = self.get_view(UserTopicList)
        result = view.get_queryset()
        self.assertQuerysetEqual(result, [self.topic])


class TestSubscriptions(TestUserLoginMixin, base.ViewTestCase):
    url_name = 'forums:topic_subs'
    template_name = 'forums/forumtopic_list_user.html'
    url_kwargs = {}

    def test_get_queryset(self):
        topic = ForumTopicFactory(slug='new-sunday-challenge')
        AlertSubscriptionFactory(target=topic.pk, user=self.user, alert__slug='forums.forum_topic_alert')
        view = self.get_view(Subscriptions, self.user)
        result = view.get_queryset()
        self.assertQuerysetEqual(result, [topic])


class TestUnreadTopicList(base.ViewTestCase):

    view_url = '/forums/unread/'
    url_name = 'forums:unread'
    template_name = 'forums/forumtopic_list_unread.html'

    def test_get_queryset(self):
        view = self.get_view(UnreadTopicList)
        topics = ForumTopic.objects.order_by('-last_posted')
        result = view.get_queryset()
        self.assertQuerysetEqual(result, topics)


class TestTopicDetail(GetTestUsersMixin, base.ViewTestCase):

    view_url = '/forums/beyond/how-to-draw-a-rectangle/'
    url_name = 'forums:topic'
    url_kwargs = {'forum': 'beyond', 'slug': 'how-to-draw-a-rectangle'}
    template_name = 'forums/forumtopic_detail.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        topic = ForumTopicFactory(forum__slug='beyond', slug='how-to-draw-a-rectangle')
        CommentFactory( content_object=topic, submit_date='2021-05-09')
        CommentFactory( content_object=topic)

    def test_dispatch(self):
        view = self.get_view(TopicDetail, get_params={
                             'jumpto': '2021-05-10T00:00:00.902Z'})
        response = view.dispatch(view.request, view.args, view.kwargs)
        # We know that comment with pk = 2 is the first comment
        # after the jumpto date in this forum topic
        self.assertEqual(response.url, f'{self.view_url}#c2')

    # Use the wrong forum to emulate a moved topic
    @mock.patch.dict(url_kwargs, {'forum': 'cafe'})
    def test_get_template_names_for_moved_topic(self):
        view = self.get_view(TopicDetail)
        template = view.get_template_names()
        self.assertEqual(template, 'forums/forumtopic_moved.html')

    def test_removed_topics_should_return_404_for_unauthorized_users(self):
        topic = ForumTopicFactory(forum=Forum.objects.last(), slug='topic-to-be-removed')
        view = self.get_view(TopicDetail)
        view.kwargs['slug'] = topic.slug
        response = view.dispatch(view.request, view.args, view.kwargs)
        self.assertEqual(response.status_code, 200)
        # When the topic is flagged as removed, only moderators should be able to access it
        topic.removed = True
        topic.save()
        self.assertRaises(Http404, view.dispatch, view.request, view.args, view.kwargs)
        view.request.user = self.get_test_moderator()
        response = view.dispatch(view.request, view.args, view.kwargs)
        self.assertEqual(response.status_code, 200)


class TestCommentList(GetTestUsersMixin, base.ViewTestCase):
    url_name = 'comment_list'
    url_kwargs = {'username': 'tester'}
    template_name = 'forums/comment_list.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        CommentFactory(user__username=cls.url_kwargs['username'])

    def test_template_name_for_authenticated_user(self):
        user = self.get_test_user()
        view = self.get_view(CommentList, user)
        self.assertEqual(view.template_name, 'forums/comment_list_user.html')

    def test_template_name_for_other_users(self):
        view = self.get_view(CommentList)
        self.assertEqual(view.template_name, self.template_name)

    def test_get_queryset_for_a_user(self):
        view = self.get_view(CommentList)
        comment = Comment.objects.filter(user_name='tester')
        result = view.get_queryset()
        self.assertQuerysetEqual(result, comment, ordered=False)

    @mock.patch.dict(url_kwargs, {'username': 'testerwho'})
    def test_get_queryset_for_non_existing_user(self):
        # We expect the method to throw a 404 error
        with self.assertRaises(Http404):
            view = self.get_view(CommentList)
            view.get_queryset()


class TestCommentCreate(AlertCleanupMixin, ForumDataSetupMixin, TestUserLoginMixin, base.ViewTestCase):
    url_name = 'forums:comment_create'
    template_name = 'forums/comment_form.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.url_kwargs = {'forum': 'cafe', 'slug': 'try-vectors'}


    def test_get_parent(self):
        view = self.get_view(CommentCreate)
        topic = ForumTopic.objects.get(slug='try-vectors')
        result = view.get_parent()
        self.assertEqual(result, topic)

    def test_get_form_kwargs(self):
        view = self.get_view(CommentCreate, user=self.user)
        topic = ForumTopic.objects.get(slug='try-vectors')
        kwargs = view.get_form_kwargs()
        for key in ('user', 'ip_address', 'target_object'):
            self.assertIn(key, kwargs.keys())
        self.assertEqual(kwargs['user'], self.user)
        self.assertEqual(kwargs['target_object'], topic)

    def test_form_valid(self):
        view = self.get_view(CommentCreate, user=self.user)
        # django_comments requires some special kwargs
        kwargs = view.get_form().generate_security_data()
        response = self.client.post(
            self.view_url, {**kwargs, 'comment': "Oi! Where's my sonic screwdriver?"})
        last_comment_pk = Comment.objects.last().pk
        url = f'/forums/cafe/try-vectors/?c={last_comment_pk}#c{last_comment_pk}'
        # After creating a new comment, user should be redirected to the newly created comment
        self.assertRedirects(response, url)


class TestCommentEdit(TestUserLoginMixin, base.ViewTestCase):
    url_name = 'forums:comment_edit'
    template_name = 'forums/comment_form.html'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.comment = CommentFactory()
        cls.url_kwargs = {'pk': cls.comment.pk}


    def test_get_form_kwargs(self):
        view = self.get_view(CommentEdit, user=self.user)
        kwargs = view.get_form_kwargs()
        for key in ('user', 'grace'):
            self.assertIn(key, kwargs.keys())

    @skip("""There is some redirection going on involving fragments ('#' part of the URL).
    Needs some more investigation.""")
    def test_form_valid(self):
        comment = self.comment
        comment.submit_date = timezone.now()
        comment.save()
        view = self.get_view(CommentEdit, user=self.user)
        view.object = comment
        # django_comments redirects /comments/cr/121/8/#c10 type urls to
        # the appropriate url
        response = self.client.post(
            f'{self.view_url}?next=/forums/competitions/new-sunday-challenge/#c10', {'comment': 'Wellll'})
        self.assertEqual(response.url, '/comments/cr/121/8/#c10')


class TestCommentRaw(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:comment_raw'
    template_name = 'forums/comment_raw.html'

    @classmethod
    def setUpTestData(cls):
        # Mark the comment as removed for all tests
        cls.comment = CommentFactory(is_removed=True)
        cls.url_kwargs = {'pk': cls.comment.pk}

    def test_normal_user_cannot_access_removed_comment(self):
        # First moderator logs out
        self.client.logout()
        user = self.get_test_user()
        self.client.force_login(user)
        response = self.client.get(self.view_url)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)


class TestCommentModPublic(ModeratorLoginMixin, base.BaseViewTestCase):
    url_name = 'forums:comment_public'
    template_name = None

    def setUp(self):
        self.comment = CommentFactory()
        self.url_kwargs = {'pk': self.comment.pk}
        super().setUp()

    # Unit test the field_changed function
    def test_field_changed(self):
        comment = CommentFactory()
        view = self.get_view(CommentModPublic, self.moderator)
        response_data = view.field_changed(comment, comment.is_public)
        self.assertEqual(response_data, {'is_public': comment.is_public})

    def test_accessing_view_flips_is_public_field(self):
        comment = CommentFactory()
        old_value = comment.is_public
        self.client.get(reverse('forums:comment_public', args=[comment.pk]))
        comment.refresh_from_db()
        new_value = comment.is_public
        self.assertNotEqual(old_value, new_value)

    def test_add_permissions(self):
        user = self.get_test_user()
        view = self.get_view(CommentModPublic, user=user)
        view.add_permissions(user)
        self.assertTrue(user.has_perms(
            ['forums.can_post_comment', 'forums.can_post_topic']))


class TestCommentModRemove(ModeratorLoginMixin, base.BaseViewTestCase):
    url_name = 'forums:comment_remove'

    template_name = None

    def setUp(self):
        self.comment = CommentFactory()
        self.url_kwargs = {'pk': self.comment.pk}
        super().setUp()

    # Unit test the field_changed function
    def test_field_changed(self):
        view = self.get_view(CommentModRemove, self.moderator)
        response_data = view.field_changed(self.comment, self.comment.is_removed)
        self.assertEqual(
            response_data, {'c': self.comment.pk, 'removed': self.comment.is_removed})

    def test_accessing_view_flips_is_removed_field(self):
        old_value = self.comment.is_removed
        self.client.get(self.view_url)
        self.comment.refresh_from_db()
        new_value = self.comment.is_removed
        self.assertNotEqual(old_value, new_value)


class TestCommentModList(ModeratorLoginMixin, base.ViewTestCase):
    url_kwargs = {}
    url_name = 'forums:check'
    template_name = 'forums/moderator_list.html'

    @classmethod
    def setUpTestData(cls):
        # Set the comment as hidden for all tests
        cls.comment = CommentFactory(is_public=False)

    def test_get_queryset(self):
        view = self.get_view(CommentModList, self.moderator)
        result = view.get_queryset()
        self.assertQuerysetEqual(result, [self.comment])


class TestTopicMove(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:topic_move'
    url_kwargs = {'slug': 'try-vectors'}
    template_name = 'forums/moderator_form.html'
    new_forum = 'beyond'

    def setUp(self):
        ForumTopicFactory(slug=self.url_kwargs['slug'])
        super().setUp()

    def test_submitting_to_form_moves_topic(self):
        forum = ForumFactory(slug=self.new_forum)
        response = self.client.post(self.view_url, {'forum': forum.pk})
        # After moving the topic, form should redirect to topic's new location
        self.assertRedirects(response, reverse('forums:topic', kwargs={
                             'forum': self.new_forum, **self.url_kwargs}))
        # Also check that the topic is indeed moved
        topic = ForumTopic.objects.select_related(
            'forum').get(slug='try-vectors')
        self.assertEqual(topic.forum.slug, self.new_forum)


class TestTopicEdit(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:topic_edit'
    url_kwargs = {'slug': 'try-vectors'}
    template_name = 'forums/moderator_form.html'
    new_topic_name = 'Take a trip to the vector land!'

    def setUp(self):
        ForumTopicFactory(slug=self.url_kwargs['slug'], forum__slug='cafe')
        super().setUp()

    def test_submitting_to_form_edits_topic(self):
        response = self.client.post(
            self.view_url, {'subject': self.new_topic_name, 'injected': 0, 'sticky': 0})
        # Even after changing the topic, its slug remains the same
        self.assertRedirects(response, reverse('forums:topic', kwargs={
                             'forum': 'cafe', **self.url_kwargs}))
        # Also check for the changed name
        topic = ForumTopic.objects.get(slug=self.url_kwargs['slug'])
        self.assertEqual(topic.subject, self.new_topic_name)


class TestTopicDelete(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/try-vectors/del/'
    url_name = 'forums:topic_delete'
    url_kwargs = {'slug': 'try-vectors'}
    template_name = 'forums/moderator_form.html'

    @classmethod
    def setUpTestData(cls):
        cls.topic = ForumTopicFactory(slug=cls.url_kwargs['slug'])

    def test_view_deletes_topic(self):
        response = self.client.post(self.view_url)

        # After deleting the topic, moderator should be redirected to parent forum
        self.assertRedirects(response, reverse(
            'forums:topic_list', kwargs={'slug': self.topic.forum.slug}))

        # Also check that the topic has been removed
        self.topic.refresh_from_db()
        self.assertTrue(self.topic.removed)


class TestTopicCreate(AlertCleanupMixin, TestUserLoginMixin, base.ViewTestCase):
    view_url = '/forums/cafe/new/'
    url_name = 'forums:create'
    url_kwargs = {'slug': 'cafe'}
    template_name = 'forums/forumtopic_form.html'
    new_topic_name = 'Artist Update'

    def setUp(self):
        super().setUp()
        view = self.get_view(TopicCreate, user=self.user)
        # django_comments requires some special kwargs
        kwargs = view.get_form().generate_security_data()
        self.post_params = {
            **kwargs,
            'subject': self.new_topic_name,
            'comment': '<p>Heya!!</p>'
        }
        self.new_topic_kwargs = {'forum': 'cafe', 'slug': 'artist-update'}

    def test_get_parent_returns_correct_topic(self):
        view = self.get_view(TopicCreate, user=self.user,
                             post_params=self.post_params)
        topic = Forum.objects.get(slug=self.url_kwargs['slug'])
        returned_topic = view.get_parent()
        self.assertEqual(returned_topic, topic)

    def test_get_form_kwargs_returns_correct_kwargs(self):
        view = self.get_view(TopicCreate, user=self.user,
                             post_params=self.post_params)
        returned_kwargs = view.get_form_kwargs()
        expected_kwargs = {'user': self.user,
                           'ip_address': '127.0.0.1',
                           'target_object': Forum.objects.get(slug=self.url_kwargs['slug'])}
        self.assertDictContainsSubset(expected_kwargs, returned_kwargs)

    def test_view_creates_new_topic(self):
        response = self.client.post(self.view_url, self.post_params)

        # After creating a new topic, user should be redirected to that new topic
        # This also checks that the new topic now exists
        self.assertRedirects(response, reverse(
            'forums:topic', kwargs=self.new_topic_kwargs))


class TestTopicMerge(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:topic_merge'
    url_kwargs = {'slug': 'try-vectors'}
    template_name = 'forums/moderator_form.html'
    target_topic_data = {'forum': 'beyond', 'slug': 'how-to-draw-a-rectangle'}

    @classmethod
    def setUpTestData(cls):
        cls.topic = ForumTopicFactory(slug=cls.url_kwargs['slug'])
        cls.target_topic = ForumTopicFactory(slug=cls.target_topic_data['slug'], forum__slug='beyond')
        CommentFactory.create_batch(2, content_object=cls.topic)
        CommentFactory.create_batch(2, content_object=cls.target_topic)
        cls.view_url = reverse('forums:topic_merge', kwargs=cls.url_kwargs)

    def test_get_form_kwargs(self):
        view = self.get_view(TopicMerge)
        kwargs = view.get_form_kwargs()
        self.assertEqual(kwargs['from_topic'], self.topic)

    def test_submitting_to_form_merges_topic(self):
        # Get all comments of the topic before it is merged
        comment_pks = [comment.pk for comment in list(
            self.topic.comments.all())]

        response = self.client.post(
            self.view_url, {'with_topic': self.target_topic.pk})

        # After merging, the form should redirect to the topic it was merged with
        self.assertRedirects(response, reverse(
            'forums:topic', kwargs=self.target_topic_data))

        # Check that the target topic has comments of the topic that was merged
        target_topic_comment_pks = [
            comment.pk for comment in list(self.target_topic.comments.all())]
        for pk in comment_pks:
            self.assertIn(pk, target_topic_comment_pks)

        # Also check that the source topic does not exist anymore
        response = self.client.get(reverse('forums:topic', kwargs={
            'forum': 'cafe', **self.url_kwargs}))
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)


class TestTopicSplit(ModeratorLoginMixin, base.ViewTestCase):

    # view_url = '/forums/new-sunday-challenge/split/'
    url_name = 'forums:topic_split'
    url_kwargs = {'slug': 'new-sunday-challenge'}
    template_name = 'forums/moderator_form.html'
    target_topic = {'forum': 'competitions', 'slug': 'another-challenge'}
    target_topic_name = 'Another Challenge'

    def setUp(self):
        self.topic = ForumTopicFactory(slug=self.url_kwargs['slug'], forum__slug='competitions')
        CommentFactory.create_batch(3, content_object=self.topic)
        super().setUp()

    def test_submitting_to_form_splits_topic(self):
        comments = list(self.topic.comments.all())
        # Split out two comments
        response = self.client.post(self.view_url, {
            'new_name': self.target_topic_name,
            'comments': [comment.pk for comment in comments[:2]]})

        # After splitting, the form should redirect to the newly created topic
        self.assertRedirects(response, reverse(
            'forums:topic', kwargs=self.target_topic))

        # Check that the two comments are indeed removed from the source topic
        comments_after_splitting = self.topic.comments.all()
        self.assertQuerysetEqual(comments[2:], comments_after_splitting)

        # Also check that the comments splitted end up in the new target topic
        comments_on_new_topic = ForumTopic.objects.get(
            slug=self.target_topic['slug']).comments.all()
        self.assertQuerysetEqual(comments[:2], comments_on_new_topic)


class TestCommentEmote(TestUserLoginMixin, TestCase, base.GetViewMixin):
    url_name = 'forums:emote'
    old_emote = '🐧'
    new_emote = '🔥'
    get_params = {'flag': new_emote}

    @classmethod
    def setUpTestData(cls):
        # This comment already has the emote
        cls.comment = CommentFactory()
        cls.url_kwargs['pk'] = cls.comment.pk
        cls.comment_flag = CommentFlagFactory(comment=cls.comment, flag=u"\U0001F427")
        cls.view_url = reverse(cls.url_name, kwargs=cls.url_kwargs)

    def get_emotes(self):
        comment_flags = CommentFlag.objects.filter(
            comment_id=self.url_kwargs['pk'], user=self.user)
        return [emote_flag.flag for emote_flag in comment_flags]

    def test_get_object_returns_correct_object(self):
        view = self.get_view(CommentEmote, self.user,
                             get_params=self.get_params)
        obj = view.get_object()
        self.assertEqual(self.comment_flag, obj)

    def test_view_replaces_old_comment_emote_with_new(self):
        response = self.client.post(self.view_url, self.get_params)

        # Check that the new_emote replaces the old emote
        self.comment_flag.refresh_from_db()
        self.assertNotIn(self.old_emote, self.get_emotes())
        self.assertEqual(self.comment_flag.flag, self.new_emote)

        # Check that the view returns correct JSON Response
        self.assertJSONEqual(response.content, {
            'id': self.comment_flag.pk,
            'flag': self.comment_flag.flag,
            'comment': self.comment_flag.comment_id,
            'user': self.comment_flag.user_id,
        })


class TestUserFlagList(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/users/flags/'
    url_name = 'forums:flag_list'
    template_name = 'forums/userflag_list.html'

    def test_get_queryset_returns_only_custom_flags(self):
        view = self.get_view(UserFlagList, self.moderator)
        user_flag = UserFlag.objects.filter(user=self.get_test_user())
        result = view.get_queryset()
        self.assertQuerysetEqual(result, user_flag)


class TestUserModList(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/users/mods/'
    url_name = 'forums:mod_list'
    template_name = 'forums/userflag_mod_list.html'

    def test_get_queryset_returns_only_custom_flags(self):
        view = self.get_view(UserModList, self.moderator)
        mod_flag = UserFlag.objects.filter(user=self.get_test_moderator())
        result = view.get_queryset()
        self.assertQuerysetEqual(result, mod_flag)


class TestUserBanList(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/users/banned/'
    url_name = 'forums:ban_list'
    template_name = 'forums/userflag_ban_list.html'

    @classmethod
    def setUpTestData(cls):
        # Mark test user as banned for all tests
        UserFlagFactory(flag=UserFlag.FLAG_BANNED, user=cls.get_test_user())

    def test_get_queryset_returns_only_banned_users(self):
        view = self.get_view(UserBanList, self.moderator)
        user_flag = UserFlag.objects.filter(user=self.get_test_user())
        result = view.get_queryset()
        self.assertQuerysetEqual(result, user_flag)


class TestWordBanList(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/users/words/'
    url_name = 'forums:word_list'
    template_name = 'forums/bannedwords_list.html'

    def test_get_queryset_returns_banned_words(self):
        BannedWordsFactory(phrase="tacos")
        BannedWordsFactory(phrase="cheesetacos")

        view = self.get_view(WordBanList, self.moderator)
        phrases = [banned.phrase for banned in view.get_queryset()]
        for phrase in ('tacos', 'cheesetacos'):
            self.assertIn(phrase, phrases)


class TestWordBanCreate(ModeratorLoginMixin, base.ViewTestCase):
    view_url = '/forums/users/words/create/'
    url_name = 'forums:create_words'
    template_name = 'modal.html'
    new_word = 'pineapplepizza'
    post_params = {'phrase': new_word, 'next': reverse('forums:word_list')}

    def test_get_success_url(self):
        view = self.get_view(WordBanCreate, self.moderator,
                             post_params=self.post_params)
        view.object = BannedWords.objects.create(phrase=self.new_word)
        return_url = view.get_success_url()

        # Check that the url returned matches with what was sent as 'next' url parameter
        self.assertEqual(return_url, self.post_params['next'])

        # Also check that the moderator was correctly set
        self.assertEqual(view.object.moderator, self.moderator)

    def test_view_creates_banned_word(self):
        response = self.client.post(self.view_url, self.post_params)

        # After creating the banned word, moderator should be redirected to Banned Words list
        self.assertRedirects(response, self.post_params['next'])

        # Check that the new word is now banned
        banned_words = [word.phrase for word in BannedWords.objects.all()]
        self.assertIn(self.new_word, banned_words)


class TestWordBanDelete(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:delete_words'
    template_name = 'forums/bannedwords_confirm_delete.html'

    def setUp(self):
        self.word = BannedWordsFactory(phrase="tacos")
        self.url_kwargs = {'pk': self.word.pk}
        super().setUp()

    # get_success_url already tested

    def test_get_context_data_has_delete_kwarg(self):
        view = self.get_view(WordBanDelete, self.moderator)
        # We need to manually set the object since we're using RequestFactory in get_view
        view.object = self.word
        kwargs = view.get_context_data(**view.kwargs)
        self.assertTrue(kwargs['delete'])

    def test_view_deletes_banned_word(self):
        # The word exists here
        word = self.word
        self.url_kwargs = {'pk': word.pk}
        self.client.post(reverse(self.url_name, kwargs=self.url_kwargs))

        # Check that it does not exist after the request
        with self.assertRaises(ObjectDoesNotExist):
            BannedWords.objects.get(pk=word.pk)


class TestUserFlagToggle(ModeratorLoginMixin, GetViewMixin, TestCase):
    view_url = '/forums/users/flags/flag/'
    url_name = 'forums:flag_user'
    flag = '🤖'

    @classmethod
    def setUpTestData(cls):
        cls.user = cls.get_test_user()

    def get_flags(self):
        user_flags = UserFlag.objects.filter(user=self.user)
        flags = [user_flag.flag for user_flag in user_flags]
        return flags

    def test_get_object_with_slug(self):
        # TestUser already has the flag set
        view = self.get_view(UserFlagToggle, self.moderator, get_params={
            'user': self.user.username
        })
        view.get_object()
        self.assertEqual(view.kwargs['slug'], self.user.username)

    def test_get_data_returns_correct_data(self):
        view = self.get_view(UserFlagToggle, self.moderator, get_params={
            'title': 'Flag',
            'mod': 'modonly'
        })
        data = view.get_data()
        self.assertEqual(data['title'], 'Flag')
        self.assertEqual(data['modflag'], True)

    def test_view_removes_flag(self):
        UserFlagFactory(user=self.user, flag=self.flag)  # ensure the flag is present to begin with
        # 'next' is arbitrary here
        response = self.client.get(self.view_url, {
            'flag': self.flag,
            'user': self.user.username,
            'next': reverse('forums:flag_list')
        })
        self.assertRedirects(response, reverse('forums:flag_list'))

        # Check that the flag is now removed from TestUser's flags
        self.assertNotIn(self.flag, self.get_flags())

    def test_view_adds_flag(self):
        new_flag = '🔥'
        # 'next' is arbitrary here
        response = self.client.get(self.view_url, {
            'flag': new_flag,
            'user': self.get_test_user().username,
            'next': reverse('forums:flag_list')
        })
        self.assertRedirects(response, reverse('forums:flag_list'))

        # Check that the new flag is now in User's flag list
        self.assertIn(new_flag, self.get_flags())

    def test_get_flag_without_flag_gives_404(self):
        # Do not send the flag in GET request
        response = self.client.get(self.view_url)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)


class TestUserModToggle(ModeratorLoginMixin, GetViewMixin, TestCase):
    view_url = '/forums/users/mods/flag/'
    url_name = 'forums:mod_user'

    def test_get_flag_returns_moderator_flag(self):
        view = self.get_view(UserModToggle, self.moderator)
        flag = view.get_flag()
        self.assertEqual(flag, UserFlag.FLAG_MODERATOR)

    def test_view_removes_moderator(self):
        mod = self.get_test_moderator()
        self.client.get(self.view_url, {
            'next': reverse('forums:mod_list'),
            'user': mod.username
        })
        # Need a refresh from the database for new permissions
        mod = User.objects.get(pk=mod.pk)
        self.assertFalse(mod.is_moderator())

    def test_view_adds_new_moderator(self):
        user = self.get_test_user()
        self.client.get(self.view_url, {
            'next': reverse('forums:mod_list'),
            'user': user.username,
            'title': 'New Moderator',
            'update': 1
        })
        # Need a refresh from the database for new permissions
        user = self.get_test_user()
        self.assertTrue(user.is_moderator())


class TestUserBanToggle(ModeratorLoginMixin, GetViewMixin, TestCase):
    view_url = '/forums/users/banned/flag/'
    url_name = 'forums:ban_user'

    @classmethod
    def setUpTestData(cls):
        cls.user = cls.get_test_user()
        cls.get_params = {
            'user': cls.user.username,
            'next': reverse('forums:ban_list')
        }

    def get_flags(self):
        user_flags = UserFlag.objects.filter(user=self.user)
        flags = [user_flag.flag for user_flag in user_flags]
        return flags

    def test_get_flag_returns_banned_flag(self):
        view = self.get_view(UserBanToggle, self.moderator)
        flag = view.get_flag()
        self.assertEqual(flag, UserFlag.FLAG_BANNED)

    def test_view_bans_normal_user(self):
        response = self.client.get(self.view_url, {
            **self.get_params,
            'title': 'Banned!',
            'update': 1
        })

        # After toggling the flag, moderator should be redirected to the Banned Users List
        # Banned user should be present in the list now
        self.assertRedirects(response, reverse('forums:ban_list'))
        self.assertIn(UserFlag.FLAG_BANNED, self.get_flags())

    def test_view_unbans_banned_user(self):
        # First ban the test user
        UserFlag.objects.create(
            user=self.user,
            flag=UserFlag.FLAG_BANNED,
            title='Banned'
        )
        # Now unban the user
        response = self.client.get(self.view_url, self.get_params)

        # After toggling the flag, moderator should be redirected to the Banned Users List
        # Unbanned user should not be present in the Banned Users list now
        self.assertRedirects(response, reverse('forums:ban_list'))
        self.assertNotIn(UserFlag.FLAG_BANNED, self.get_flags())


class TestForumStats(ModeratorLoginMixin, base.ViewTestCase):
    url_name = 'forums:stats'
    template_name = 'forums/forum_stats.html'
    url_kwargs = {}

    def test_get_context_data_has_stats(self):
        view = self.get_view(ForumStats, self.moderator)
        data = view.get_context_data(**view.kwargs)
        self.assertIn('stats', data.keys())
