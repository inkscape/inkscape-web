from forums.factories import ForumTopicFactory, CommentFactory
from django.test import TestCase


class ForumDataSetupMixin(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        topic = ForumTopicFactory(slug='try-vectors', forum__slug='cafe')
        CommentFactory.create_batch(3, topic=topic)
        topic.refresh_meta_data()
