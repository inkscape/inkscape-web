#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for models of forums app."""

from collections import namedtuple
from datetime import datetime, timedelta
from unittest import mock

from datetime import UTC
from django_comments.models import Comment

from person.factories import UserFactory
from resources.models import Resource
from testing import base
from testing.base import ModelFieldTestData
from testing.mixins import GetTestUsersMixin
from forums.factories import (BannedWordsFactory, ModerationLogFactory, ForumFactory, ForumTopicFactory,
                              CommentFactory, UserFlagFactory)

from ..models import (BannedWords, CommentAttachment, Forum, ForumGroup,
                      ForumTopic, ModerationLog, UserFlag)

PermissionsTestData = namedtuple(
    "PermissionsTestData", ["perm", "verbose_name"])


class TestForumGroup(base.ModelTestCase):

    field_labels = [
        ModelFieldTestData(field='name', label='name', help_text=''),
        ModelFieldTestData(field='sort', label='sort', help_text=''),
    ]

    @classmethod
    def setUpTestData(cls):
        cls.group = ForumGroup.objects.create(
            name='Forum Group'
        )
        cls.instance = cls.group

    def test_object_is_name_of_forum_group(self):
        self.assertEqual(str(self.group), self.group.name)


class TestForum(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='group', label='group', help_text=''),
        ModelFieldTestData(field='sort', label='sort', help_text=''),
        ModelFieldTestData(field='name', label='name', help_text=''),
        ModelFieldTestData(field='slug', label='slug', help_text=''),
        ModelFieldTestData(field='desc', label='desc', help_text=''),
        ModelFieldTestData(field='icon', label='icon', help_text=''),
        ModelFieldTestData(
            field='emotes',
            label='emotes',
            help_text="Custom reactions for posts on this forum. Replace the default ones."),
        ModelFieldTestData(
            field='lang',
            label='lang',
            help_text="Set this ONLY if you want this forum restricted to this language"),
        ModelFieldTestData(
            field='team',
            label='team',
            help_text="Set this ONLY if you want this forum restricted to this team."),
        ModelFieldTestData(
            field='content_type',
            label='Fixed Content From',
            help_text="When fixed content is set, new topics can not be created. Instead, "
            "commented items are automatically posted as topics."),
        ModelFieldTestData(
            field='post_count',
            label='Number of Posts',
            help_text="Number of Posts"),
        ModelFieldTestData(
            field='last_posted', label='Last Posted', help_text="Last Posted"),
    ]

    forum_slug = 'cafe'

    @classmethod
    def setUpTestData(cls):
        cls.forum = ForumFactory(slug=cls.forum_slug)
        cls.forum_comments = CommentFactory.create_batch(2, content_object__forum=cls.forum )

    def setUp(self):
        self.instance = self.forum = Forum.objects.get(slug=self.forum_slug)

    def test_object_is_name_of_forum(self):
        self.assertEqual(str(self.forum), self.forum.name)

    def test_get_absolute_url(self):
        expected_url = f'/forums/{self.forum_slug}/'
        self.assertEqual(self.forum.get_absolute_url(), expected_url)

    def test_comment_returns_all_comments_in_forum(self):
        # These are the comments that are in the forum
        comments = self.forum_comments
        self.assertQuerysetEqual(comments, self.forum.comments)

    def test_public_comment_returns_all_public_comments_in_forum(self):
        # Mark a comment on this forum as removed
        CommentFactory(is_removed=True, content_object__forum=self.forum)

        # Get those comments which are not removed
        comments = self.forum_comments

        self.assertQuerysetEqual(self.forum.public_comments, comments)

    def test_save_adds_slug_if_not_already_provided(self):
        group = ForumGroup.objects.get(pk=1)
        forum = Forum.objects.create(
            name='New Forum',
            group=group,
        )
        forum.save()

        # Check that the slug is there even when we didn't pass it
        self.assertEqual(forum.slug, 'new-forum')

    def test_refresh_metadata(self):
        forum = self.forum
        forum.refresh_meta_data()

        # Save previous values
        post_count = forum.post_count
        last_posted = forum.last_posted

        # Set post_count to 0 and last_posted to some other date
        forum.post_count = 0
        forum.last_posted = datetime(2021, 2, 1, 1, 1, tzinfo=UTC)
        forum.save(update_fields=['post_count', 'last_posted'])

        forum.refresh_meta_data()
        forum.refresh_from_db()

        # Check that the previous values which are the correct ones
        # are restored
        self.assertEqual(forum.post_count, post_count)
        self.assertEqual(forum.last_posted, last_posted)

    # TODO Check for help_text


class TestForumTopic(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='forum', label='forum', help_text=''),
        ModelFieldTestData(field='object_pk', label='object pk', help_text=''),
        ModelFieldTestData(field='subject', label='subject', help_text=''),
        ModelFieldTestData(field='slug', label='slug', help_text=''),
        ModelFieldTestData(
            field='message_id', label='message id', help_text=''),
        ModelFieldTestData(
            field='post_count', label='Number of Posts', help_text=''),
        ModelFieldTestData(field='first_posted',
                           label='First Posted', help_text=''),
        ModelFieldTestData(field='last_posted',
                           label='Last Posted', help_text=''),
        ModelFieldTestData(field='first_username',
                           label='first username', help_text=''),
        ModelFieldTestData(field='last_username',
                           label='last username', help_text=''),
        ModelFieldTestData(field='has_attachments',
                           label='has attachments', help_text=''),
        ModelFieldTestData(
            field='sticky',
            label='Sticky in Topic List',
            help_text="Pins the thread to the top of the thread list, "
            "the higher the number the nearer the top (moderators only)."),
        ModelFieldTestData(
            field='injected',
            label='Sticky in New Topic Form',
            help_text="When a user has less than this number of posts, this topic will be injected"
            " into the create post form as a header the user will have to read. "
            "Negatives numbers show topics to veteran users instead."),
        ModelFieldTestData(
            field='locked',
            label='Topic Locked',
            help_text='Start this topic locked. Useful for announcements (moderators only)'
        ),
        ModelFieldTestData(
            field='removed',
            label='removed',
            help_text='Topic deleted by moderator.',
        ),
    ]

    def setUp(self):
        self.instance = self.topic = ForumTopicFactory(slug='try-vectors', sticky=True)

    permissions_data = [
        PermissionsTestData(
            perm='can_post_comment',
            verbose_name="User can post comments to the forums."),
        PermissionsTestData(
            perm='can_edit_comment',
            verbose_name="User can edit their own comments without restriction."),
        PermissionsTestData(
            perm='can_post_topic',
            verbose_name="User can make new forum topics."),
    ]

    def test_model_meta_has_correct_permissions(self):
        for permission in self.permissions_data:
            perm = permission.perm
            verbose_name = permission.verbose_name

            # Check that the model has correct permissions
            # with the correct corresponding verbose name
            with self.subTest(perm=permission):
                self.assertIn((perm, verbose_name),
                              ForumTopic._meta.permissions)

    def test_model_meta_has_correct_ordering(self):
        ordering = ('-sticky', '-last_posted',)
        self.assertEqual(ordering, ForumTopic._meta.ordering)

    def test_model_meta_has_correct_get_latest_by(self):
        get_latest_by = 'last_posted'
        self.assertEqual(get_latest_by, ForumTopic._meta.get_latest_by)

    def test_object_is_name_of_subject_field(self):
        self.assertEqual(str(self.topic), self.topic.subject)

    def test_content_type_property(self):
        content_type = ForumTopic.content_type()
        self.assertEqual(content_type.model, 'forumtopic')

    def test_comment_subject_property(self):
        subject = self.topic.comment_subject
        self.assertEqual(subject, self.topic)

    def test_comments_property(self):
        comments = CommentFactory.create_batch(2, content_object=self.topic)
        self.assertQuerysetEqual(comments, self.topic.comments)

    def test_public_comments_property(self):
        # Mark a comment on this forum as removed
        CommentFactory(content_object=self.topic, is_removed=True)

        # Get those comments which are not removed
        comments = CommentFactory.create_batch(2, content_object=self.topic, )

        self.assertQuerysetEqual(self.topic.public_comments, comments)

    def test_object_template(self):
        template = self.topic.object_template
        # We do not bother to test for the other code path
        # which checks for object_pk
        self.assertEqual(template, 'forums/forumtopic_header.html')

    def test_is_sticky_property(self):
        # We know the test topic is sticky
        self.assertTrue(self.topic.is_sticky)

    def test_is_moderated_property_for_non_moderated_topic(self):
        # We know the test topic isn't moderated
        self.assertFalse(self.topic.is_moderated)

    def test_is_moderated_property_for_moderated_topic(self):
        CommentFactory(is_public=False, content_object=self.topic)

        # The post_count has changed
        self.topic.refresh_meta_data()

        # Check that the topic is moderated
        self.assertTrue(self.topic.is_moderated)

    def test_get_absolute_url(self):
        url = self.topic.get_absolute_url()
        expected_url = f'/forums/{self.topic.forum.slug}/{self.topic.slug}/'
        self.assertEqual(url, expected_url)

    def test_refresh_metadata(self):
        first_comment, last_comment = CommentFactory.create_batch(2, content_object=self.topic)

        # Then we refresh the topic metadata
        self.topic.refresh_meta_data()

        # Check that the appropriate fields got updated
        # with values from the comments
        fields = [
            (self.topic.post_count, 2),
            (self.topic.first_posted, first_comment.submit_date),
            (self.topic.last_posted, last_comment.submit_date),
            (self.topic.first_username, first_comment.user.username),
            (self.topic.last_username, last_comment.user.username),
        ]

        for field, expected_value in fields:
            with self.subTest(field=field, expected_value=expected_value):
                self.assertEqual(field, expected_value)

    def test_refresh_metadata_with_no_public_comments_marks_topic_as_removed(self):
        """This case arises when the topic has only removed comments."""
        # Then we mark the first comment as non-public
        CommentFactory(content_object=self.topic, is_removed=True)
        # Then we refresh the topic metadata
        self.topic.refresh_meta_data()

        # Check that the topic is removed now
        self.assertTrue(self.topic.removed)

    def test_save_adds_slug_if_not_already_provided(self):
        forum = ForumFactory(slug='cafe')
        topic = ForumTopic.objects.create(forum=forum, subject='blimey!')

        # Check that the correct slug was added
        self.assertEqual(topic.slug, 'blimey')

    def test_save_uses_different_slug_for_topics_with_same_subject(self):
        forum = ForumFactory(slug='cafe')
        # The test topic already has this slug
        topic = ForumTopic.objects.create(forum=forum, subject='try-vectors')

        # Check that the newly created has a different slug
        self.assertNotEqual(topic.slug, self.topic.slug)

        # Also check that the different slug is of the form
        # original_slug_<random_str>
        original_slug = topic.slug.split('_')[0]
        self.assertEqual(original_slug, self.topic.slug)


class TestCommentAttachment(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='resource', label='resource', help_text=''),
        ModelFieldTestData(field='comment', label='comment', help_text=''),
        ModelFieldTestData(field='inline', label='inline', help_text=''),
        ModelFieldTestData(field='desc', label='desc', help_text=''),

    ]

    def setUp(self):
        user = GetTestUsersMixin.get_test_user()
        self.resource = Resource.objects.create(name="Resource", user=user)
        self.comment = CommentFactory(user=user)
        self.instance = self.attachment = CommentAttachment.objects.create(
            resource=self.resource,
            comment=self.comment
        )

    def test_object_is_correct_string(self):
        self.assertEqual(str(self.attachment),
                         f'{self.resource} attached to comment in the forum.')

    def test_three_choices_for_attachments_are_available(self):
        expected_choices = [
            (0, 'Attachment'),
            (1, 'Gallery'),
            (2, 'Embeded'),
        ]
        choices = self.attachment._meta.get_field('inline').choices
        self.assertEqual(choices, expected_choices)

    def test_attachment_manager(self):
        # First we create an embedded attachment
        attachment = CommentAttachment.objects.create(
            resource=self.resource,
            comment=self.comment,
            inline=2,
        )

        galleries = CommentAttachment.objects.embedded_pks()

        # Check that only one gallery was returned
        self.assertEqual(galleries.count(), 1)
        self.assertEqual(galleries[0], attachment.pk)


class TestModerationLog(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='action', label='action', help_text=''),
        ModelFieldTestData(field='moderator', label='moderator', help_text=''),
        ModelFieldTestData(field='performed', label='performed', help_text=''),
        ModelFieldTestData(field='user', label='user', help_text=''),
        ModelFieldTestData(field='comment', label='comment', help_text=''),
        ModelFieldTestData(field='topic', label='topic', help_text=''),
        ModelFieldTestData(field='forum', label='forum', help_text=''),
        ModelFieldTestData(field='detail', label='detail', help_text=''),
    ]

    freeze_date = datetime(2021, 6, 25, 0, 0, 0, tzinfo=UTC)

    def setUp(self):
        comment = CommentFactory(user=UserFactory(username='tester'))

        # see comment below; note that this testing problem can be solved by
        # removing auto_now_add from ModerationLog.performed and setting its value in the save() method, while adding
        # editable=False to the field
        with mock.patch('django.utils.timezone.now', return_value=self.freeze_date - timedelta(days=15)):
            self.instance = self.log = ModerationLogFactory(
                action="TopicMove",
                moderator=UserFactory(username='moderator'),
                user=UserFactory(username='moderator'),
                comment=comment,
                topic=comment.content_object,
                forum=comment.content_object.forum,
                # Note that these details are denormalized and they don't reflect the actual data from the factory
                detail=f'{{"user": {{"username": "{comment.user.username}", "id": {comment.user_id}}}, '
                       f'"comment": {{"id": {comment.pk}}}, '
                       f'"topic": {{"id": {comment.content_object.pk}}}, '
                       '"to_forum": "cafe", '
                       '"from_forum": "competitions"}'
            )

    def test_model_meta_has_correct_ordering(self):
        self.assertEqual(self.log._meta.ordering, ('performed', ))

    def test_object_is_name_of_action(self):
        self.assertEqual(str(self.log), self.log.action)

    def test_get_view_returns_correct_view(self):
        from forums.views import TopicMove
        view = self.log.get_view()
        self.assertEqual(TopicMove, view)

    def test_get_methods_that_use_get_info(self):
        # The view for test log (TopicMove) has these attributes
        attributes = [
            ('get_log_icon', 'export'),
            ('get_log_color', 'info'),
            ('get_log_name', 'Topic Moved'),
        ]

        for attribute, expected_value in attributes:
            with self.subTest(attr=attribute, value=expected_value):
                self.assertEqual(
                    getattr(self.log, attribute)(),
                    expected_value)

    def test_details_returns_correct_dictionary(self):
        comment = self.instance.comment
        expected_dict = {
            'user': {'username': comment.user.username, 'id': comment.user_id},
            'comment': {'id': comment.pk},
            'topic': {'id': comment.content_object.pk},
            'to_forum': 'cafe', 'from_forum': 'competitions',
        }

        self.assertDictEqual(self.log.details(), expected_dict)

    @mock.patch('forums.models.now', mock.Mock(return_value=freeze_date))
    def test_get_last_manager_after(self):
        # First create some more dummy logs
        comment = CommentFactory(content_object=self.instance.comment.content_object)
        logs = []
        for i in range(10):
            # A new log was created every 3 days up until the freeze_date
            new_freeze_date = self.freeze_date - timedelta(days=3*i)

            # This patch is required because the performed field has auto_add
            # which makes testing of anything using this field hard.
            # Currently, there is no other option than to patch timezone.now
            with mock.patch('django.utils.timezone.now', return_value=new_freeze_date):
                logs.append(
                    ModerationLog.objects.create(
                        action='Test',
                        moderator=GetTestUsersMixin.get_test_moderator(),
                        comment=comment,
                        topic=comment.content_object,
                        forum=comment.content_object.forum
                    )
                )

        last_seven_days_log = ModerationLog.objects.get_last()

        # We expect only the first created three logs to be there in the seven days log
        self.assertQuerysetEqual(last_seven_days_log, reversed(logs[:3]))


class TestUserFlags(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='user', label='user', help_text=''),
        ModelFieldTestData(field='flag', label='flag', help_text=''),
        ModelFieldTestData(field='title', label='title', help_text=''),
        ModelFieldTestData(field='modflag',
                           label='modflag',
                           help_text='If true, this flag is only visible to moderators.'),
        ModelFieldTestData(field='created', label='created', help_text=''),
    ]

    def setUp(self):
        self.instance = self.flag = UserFlagFactory(flag='')

    def test_flag_banned_symbol_is_correct(self):
        self.assertEqual(UserFlag.FLAG_BANNED, '🚫')

    def test_flag_moderator_symbol_is_correct(self):
        self.assertEqual(UserFlag.FLAG_MODERATOR, '⚖')

    def test_model_meta_has_correct_unique_together(self):
        unique_together = (('user', 'flag'), )
        self.assertEqual(self.flag._meta.unique_together, unique_together)

    def test_model_meta_has_correct_verbose_name(self):
        verbose_name = ('user forum flag')
        verbose_name_plural = ('user forum flags')
        self.assertEqual(self.flag._meta.verbose_name, verbose_name)
        self.assertEqual(self.flag._meta.verbose_name_plural,
                         verbose_name_plural)


class TestBannedWords(base.ModelTestCase):
    field_data = [
        ModelFieldTestData(field='moderator', label='moderator', help_text=''),
        ModelFieldTestData(field='created', label='created', help_text=''),
        ModelFieldTestData(field='phrase', label='phrase', help_text=''),
        ModelFieldTestData(field='in_title',
                           label='in title',
                           help_text='Apply filter to text in the subject/title of a new topic'),
        ModelFieldTestData(field='in_body',
                           label='in body',
                           help_text='Apply filter to text in the body/comment of any comment or new topic'),
        ModelFieldTestData(field='new_user', label='new user',
                           help_text='Apply only to new users (moderation locked)'),
        ModelFieldTestData(field='ban_user',
                           label='ban user',
                           help_text='Ban user if they use this phrase'),
        ModelFieldTestData(field='found_count',
                           label='found count', help_text='')
    ]

    def setUp(self):
        self.instance = self.word = BannedWordsFactory()

    def test_object_is_name_of_phrase(self):
        self.assertEqual(str(self.word), self.word.phrase)

    def test_save_saves_phrase_in_lower(self):
        word = BannedWords.objects.create(phrase='Sans')
        self.assertEqual(word.phrase, 'sans')
