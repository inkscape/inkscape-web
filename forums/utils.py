#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=too-many-ancestors
#
"""
Forum utils.
"""

import re

from datetime import datetime
from django.utils import timezone
from django.forms import ValidationError

import collections
if not hasattr('collections', 'Callable'):
    collections.Callable = collections.abc.Callable

from bs4 import BeautifulSoup

from django.templatetags.static import static
from django.utils.translation import gettext_lazy as _

from resources.models import Resource

def clean_comment(comment):
    """Clean the comment of unwanted elements"""
    # embedded_pks = comment.attachments.embedded_pks()  # Unused
    soup = BeautifulSoup(comment.comment, 'html5lib')
    # Remove some bad tags we don't want
    for tag in soup(['iframe', 'script', 'object', 'embed']):
        tag.extract()
    # Check html for image tags.
    external_image = static('forums/images/external_image.png')
    broken_image = static('forums/images/broken_image.png')
    for tag in soup(['img']):
        tag_id = tag.get('id', '')
        if tag_id.startswith('inline_'):
            try:
                pkey = int(tag_id.split('_', 1)[1])
                resource = Resource.objects.get(pk=pkey)
                if resource.mime().is_image():
                    tag['src'] = resource.download.url
                else:
                    tag['src'] = resource.thumbnail_url()
            except (ValueError, Resource.DoesNotExist):
                tag['src'] = broken_image
        else:
            tag['src'] = external_image
    # Remove script and other tags
    return str(soup)

def doy(date):
    """Return day of year from datetime"""
    return (date - datetime(date.year, 1, 1, tzinfo=date.tzinfo)).days + 1

def censor_text(user, title, body, err=ValidationError):
    """Check if the comment has been banned"""
    from .models import BannedWords

    censor = False
    for bwd in BannedWords.objects.all().match_words(user, title, body):
        bwd.found_count += 1
        bwd.save()
        if bwd.ban_user:
            user.forum_flags.instant_ban(user, words=bwd.phrase)
            if err:
                raise err(_("Instant ban! Please contact the moderators for help."))
        censor = True

    if censor and err:
        raise err(_("Post has been blocked."))
    return censor
