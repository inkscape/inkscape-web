# -*- coding: utf-8 -*-
#
# Copyright 2013, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Resources provide galleries, downloads and other important parts"""

from django.urls import re_path
from inkscape.url_utils import url_tree
from person import user_urls, team_urls

from .views import (
    ResourceList, ResourcePick, ResourceFeed, ResourceJson, ViewResource, ResourcesJson,
    GalleryList, GalleryView, GalleryFeed, CreateGallery, BasicResourcesJson,
    PasteInResource, AddCodeResource, UploadResource, DropResource, LinkToResource,
    DeleteGallery, EditGallery, DeleteResource, EditResource, PublishResource,
    MoveResource, DownloadReadme, VoteResource, DownloadResource, ReleaseResource,
    UploadJson, TagsJson, QuotaJson, GalleryJson, UnpublishedGallery,
    CheckResource, ResourceParade, GalleryParade, RenameResource, QuotaChecker,
    FavoriteResources, GalleryTally, GalleryTallyJson,
)

def resource_search(*args, lst=ResourceList, feed=ResourceFeed,
                    pick=ResourcePick, json=ResourcesJson, parade=ResourceParade):
    """Generate standard url patterns for resource listing"""
    return [
        re_path(r'^$', lst.as_view(), name='resources'),
        re_path(r'^pick/$', pick.as_view(), name='resources_pick'),
        re_path(r'^rss/$', feed(), name='resources_rss'),
        re_path(r'^json/$', json.as_view(), name='resources_json'),
        re_path(r'^parade/$', parade.as_view(), name='resources_parade'),
        url_tree(
            r'^=(?P<category>[^\/]+)/',
            re_path(r'^$', lst.as_view(), name='resources'),
            re_path(r'^rss/$', feed(), name='resources_rss'),
            re_path(r'^json/$', json.as_view(), name='resources_json'),
            re_path(r'^parade/$', parade.as_view(), name='resources_parade'),
            *args)
    ]

owner_patterns = [ # pylint: disable=invalid-name
    url_tree(
        r'^galleries/',
        re_path(r'^$', GalleryList.as_view(), name='galleries'),
        re_path(r'^quota/$', QuotaChecker.as_view(), name='resource.check_quota'),
        url_tree(r'^(?P<galleries>[^\/]+)/', *resource_search(
            lst=GalleryView, feed=GalleryFeed, json=GalleryJson, parade=GalleryParade)),
    ),
    url_tree(r'^resources/', *resource_search()),
]
user_patterns = [ # pylint: disable=invalid-name
    # Try a utf-8 url, see if it breaks web browsers.
    re_path(r'^★(?P<slug>[^\/]+)$', ViewResource.as_view(), name='resource'),
    re_path(r'^favorites/', FavoriteResources.as_view(), name='favorites'),
]
# Add to the username user profile and teamname
user_urls.urlpatterns += list(owner_patterns + user_patterns)
team_urls.urlpatterns += list(owner_patterns)

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^paste/(?P<pk>\d+)/$', ViewResource.as_view(), name='pasted_item'),
    re_path(r'^json/tags.json$', TagsJson.as_view(), name='tags.json'),
    re_path(r'^json/quota.json$', QuotaJson.as_view(), name='quota.json'),
    re_path(r'^json/resources.json$', BasicResourcesJson.as_view(), name='resources.json'),
    re_path(r'^json/upload.json$', UploadJson.as_view(), name='upload.json'),

    url_tree(
        r'^gallery/',
        re_path(r'^new/$', CreateGallery.as_view(), name='new_gallery'),
        re_path(r'^link/$', LinkToResource.as_view(), name='resource.link'),
        re_path(r'^paste/$', PasteInResource.as_view(), name='pastebin'),
        re_path(r'^upload/$', UploadResource.as_view(), name='resource.upload'),
        re_path(r'^upload/go/$', DropResource.as_view(), name='resource.drop'),
        re_path(r'^upload/code/$', AddCodeResource.as_view(), name='resource.addcode'),
        re_path(r'^upload/release/$', ReleaseResource.as_view(), name='resource.release'),
        re_path(r'^unpublished/$', UnpublishedGallery.as_view(), name='resource.unpublished'),

        url_tree(
            r'^(?P<gallery_id>\d+)/',
            # We should move these to galleries/
            re_path(r'^$', GalleryView.as_view(), name='resources'),
            re_path(r'^del/$', DeleteGallery.as_view(), name='gallery.delete'),
            re_path(r'^link/$', LinkToResource.as_view(), name='resource.link'),
            re_path(r'^edit/$', EditGallery.as_view(), name='gallery.edit'),
            re_path(r'^tally/$', GalleryTally.as_view(), name='resource.tally'),
            re_path(r'^tally/json/$', GalleryTallyJson.as_view(), name='resource.tally.json'),
            re_path(r'^upload/$', UploadResource.as_view(), name='resource.upload'),
            re_path(r'^upload/go/$', DropResource.as_view(), name='resource.drop'),
        ),

        url_tree(
            r'^item/(?P<pk>\d+)/',
            re_path(r'^$', ViewResource.as_view(), name='resource'),
            re_path(r'^del/$', DeleteResource.as_view(), name='delete_resource'),
            re_path(r'^pub/$', PublishResource.as_view(), name='publish_resource'),
            re_path(r'^edit/$', EditResource.as_view(), name='edit_resource'),
            re_path(r'^json/$', ResourceJson.as_view(), name='resource.json'),
            re_path(r'^rename/$', RenameResource.as_view(), name='rename_resource'),
            re_path(r'^view/$', DownloadResource.as_view(), name='view_resource'),
            re_path(r'^move/(?P<source>\d+)/$', MoveResource.as_view(), name='resource.move'),
            re_path(r'^copy/$', MoveResource.as_view(), name='resource.copy'),
            re_path(r'^check/$', CheckResource.as_view(), name='resource.check'),
            re_path(r'^readme.txt$', DownloadReadme.as_view(), name='resource.readme'),
            re_path(r'^(?P<like>[\+\-])$', VoteResource.as_view(), name='resource.like'),
            re_path(r'^(?P<fn>[^\/]+)/?$', DownloadResource.as_view(), name='download_resource'),
        ),
        *resource_search(
            re_path(r'^(?P<galleries>[^\/=]+)/$', GalleryView.as_view(), name='resources'),
            re_path(r'^(?P<galleries>[^\/=]+)/rss/$', GalleryFeed(), name='resources_rss'),
            re_path(r'^(?P<galleries>[^\/=]+)/json/$', GalleryJson.as_view(), name='resources_json'),
            re_path(r'^(?P<galleries>[^\/=]+)/parade/$', GalleryParade.as_view(), name='resources_parade'),
        )
    ),
]
