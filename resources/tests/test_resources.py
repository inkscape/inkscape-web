#
# Copyright 2015, Maren Hachmann <marenhachmann@yahoo.com>
#                 Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test Resource Items and Lists
"""

__all__ = ('ResourceTests', 'ResourceAnonTests')

import os
from unittest import mock

import factory.django

from person.factories import UserFactory, TeamFactory, GroupFactory
from resources.models import Resource, Quota, Gallery, License
from resources.forms import ResourceForm, ResourceEditPasteForm, ResourcePasteForm
from resources.video_url import video_detect

from person.models import User

from .base import BaseCase
from resources.factories import ResourceFactory, GalleryFactory
from resources.storage import resource_storage


class ResourceTests(BaseCase):
    """Test non-request functions and methods"""
    def test_slug(self):
        """Unique slug creation"""
        data = {
          'name': 'Test Resource Title',
          'user': UserFactory(),
        }
        one = Resource.objects.create(**data)
        self.assertEqual(one.slug, 'test-resource-title')
        two = Resource.objects.create(**data)
        self.assertEqual(two.slug, 'test-resource-title+1')
        now = Resource.objects.create(**data)
        self.assertEqual(now.slug, 'test-resource-title+2')
        two.delete()
        now = Resource.objects.create(**data)
        self.assertEqual(now.slug, 'test-resource-title+1')

    def test_file_deletion(self):
        """Check that removal of a Resource removes the corresponding Resource and vice versa"""
        resources = ResourceFactory.create_batch(2)
        resource = resources[0]
        Resource.objects.get(pk=resource.pk).delete()
        with self.assertRaises(Resource.DoesNotExist):
            Resource.objects.get(pk=resource.pk)
            
        resource = resources[0]
        resource.delete()
        with self.assertRaises(Resource.DoesNotExist):
            Resource.objects.get(pk=resource.pk)

    def test_media_size(self):
        """Make sure file sizes are reported"""
        fixture_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures/media/test/file5.svg')
        svg = ResourceFactory(download=factory.django.FileField(from_path=fixture_path))
        self.assertTupleEqual(svg.file.media_coords, (24, 24))

        fixture_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures/media/test/file4.svg')
        svg = ResourceFactory(download=factory.django.FileField(from_path=fixture_path))
        self.assertTupleEqual(svg.file.media_coords, (-1, -1))

    def test_mime_type(self):
        """Make sure file types are right"""
        pdf = ResourceFactory(pdf=True).mime()
        self.assertEquals(pdf.subtype(), 'pdf')
        self.assertEquals(pdf.type(), 'document')
        self.assertEquals(pdf.icon(), '/static/mime/pdf.svg')

        unknown = ResourceFactory(download__filename='plasticman.unk').mime()
        self.assertEquals(unknown.icon(), '/static/mime/unknown.svg')


class BaseResourceViewTestCase(BaseCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.license_pd = License.objects.get(code='PD')
        group = GroupFactory(name='Everyone')
        team = TeamFactory(name='Base Team', group=group)
        cls.user = UserFactory(username='tester', groups=[group])
        cls.different_user = UserFactory(username='another_user')
        cls.own_resource = ResourceFactory(user=cls.user, license=cls.license_pd,
                                           category__acceptable_licenses=[cls.license_pd])
        cls.other_resource = ResourceFactory(user=cls.different_user, license=cls.license_pd,
                                           category__acceptable_licenses=[cls.license_pd])


class ResourceViewTests(BaseResourceViewTestCase):
    credentials = dict(username='tester', password='123456')

    def test_view_my_public_item_detail(self):
        """Testing item detail view and template for public own items,
        and make sure the view counter is correctly incremented"""
        #make sure we own the file and it's public
        resources = Resource.objects.filter(published=True, user=self.user)
        self.assertGreater(resources.count(), 0,
            "Create a published resource for user %s" % self.user)
        resource = resources[0]
        num_views = resource.viewed
        
        response = self.assertGet('resource', pk=resource.pk)
        self.assertEqual(response.context['object'], resource)
        self.assertContains(response, resource.filename())
        self.assertContains(response, resource.name)
        self.assertContains(response, resource.description())
        self.assertContains(response, str(self.user))
        # can't increment view number on my own items
        self.assertEqual(response.context['object'].viewed, num_views)
        self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, num_views)
        
    def test_view_my_unpublished_item_detail(self):
        """Testing item detail view and template for non-published own items"""
        # make sure we own the file and it is unpublished
        resource = ResourceFactory(published=False, user=self.user, viewed=0)

        response = self.assertGet('resource', pk=resource.pk)
        self.assertEqual(response.context['object'], resource)
        self.assertNotContains(response, resource.filename())
        self.assertContains(response, resource.name)
        self.assertContains(response, resource.description())
        self.assertContains(response, str(self.user))
        # can't increment view number on my own items
        self.assertEqual(response.context['object'].viewed, resource.viewed)
        self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, 0)
    
    def test_view_someone_elses_public_item_detail(self):
        """Testing item detail view and template for someone elses public resource:
        license, picture, description, username should be contained, and views should
        be counted correctly"""
        # make sure we don't own the file and it is public

        # FIXME: Resource.viewed does NOT seem to be written to anywhere, at least not that I could find; this test
        # passes in the previous revision because the fixture already has viewed=1 - in other words, there's no
        # change. To be sure, I also tried removing the viewed attribute from the model, the admin and the fixtures,
        # and all tests in 5226da706b473080afd6f2267e57a16a3f2a4b42 that don't reference Resource.viewed directly
        # pass rather than raising exceptions due to a missing field. I suspect we could do away with the whole test
        # (and related, probably)
        resource = ResourceFactory(published=True, user=self.different_user, viewed=1)

        response = self.assertGet('resource', pk=resource.pk)
        self.assertEqual(response.context['object'], resource)
        self.assertContains(response, resource.filename())
        self.assertContains(response, resource.name)
        self.assertContains(response, resource.description())
        self.assertContains(response, str(resource.user) )
        self.assertContains(response, resource.license.value)

        # Number of views is incremented after #253
        self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, 1)
        
        # number of views should only be incremented once per user session
        response = self.assertGet('resource', pk=resource.pk)
        self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, 1)

    def test_view_someone_elses_unpublished_item_detail(self):
        """Testing item detail view for someone elses non-public resource: 
        Page not found and no incrementing of view number"""
        # Make sure we don't own the resource
        resource = ResourceFactory(published=False, user=self.different_user)
        num = resource.viewed
        
        response = self.assertGet('resource', pk=resource.pk, status=404)
        self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, num)
    
    def test_view_text_file_detail(self):
        """Check if the text of a textfile is displayed on item view page and its readmore is not"""
        # specific text file resource, description contains a 'Readmore' marker: [[...]]
        resource = ResourceFactory(desc="Big description for Resource Two[[...]]And Some More",
                                   media_type="text/plain",
                                   download=factory.django.FileField(data='Text File Content', filename='readme.txt'))
        self.client.force_login(resource.user)
        response = self.assertGet('resource', pk=resource.pk)
        self.assertContains(response, 'Text File Content')
        self.assertContains(response, 'Big description for Resource Two')
        self.assertContains(response, 'readme.txt')
        self.assertNotContains(response, 'And Some More')
    
    def test_view_public_resource_full_screen(self):
        """Check that a resource can be viewed in full screen, 
        and that fullview number is incremented when a user visits
        a published item in 'full screen glory'"""
        resource = ResourceFactory(published=True, fullview=0, svg=True)
        
        response = self.assertGet('view_resource', pk=resource.pk, follow=False, status=302)
        self.assertEqual(response.url, resource.download.url)

        # Number of fullviews is incremented after #253
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 1)
        
        # The full view counter is untracked so every request will increment the counter
        response = self.assertGet('view_resource', pk=resource.pk)

        # Number of fullviews is incremented after #253
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 2)
        
    def test_view_own_unpublished_resource_full_screen(self):
        """Check that we can look at our unpublished resource in full screen mode,
        and that fullviews aren't counted as long as the resource isn't public"""
        resource = ResourceFactory(published=False, user=self.user, fullview=0)
        
        response = self.assertGet('view_resource', pk=resource.pk, status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 0)

    def test_view_someone_elses_unpublished_resource_full_screen(self):
        """Make sure that fullscreen view for unpublished items 
        doesn't work if they are not ours. Also make sure this 
        doesn't increment the fullview counter"""
        resource = ResourceFactory(published=False, user=self.different_user, fullview=0)
        
        response = self.assertGet('view_resource', pk=resource.pk, status=404)
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 0)
    
    def test_view_readme(self):
        """Download the description as a readme file"""
        resource = ResourceFactory(user=self.user, desc="Description with marker[[...]] and additional content")
        response = self.assertGet('resource.readme', pk=resource.pk)
        self.assertContains(response, resource.desc)

    def test_download(self):
        """Download the actual file"""
        resource = ResourceFactory(published=True, downed=0, download__filename="test8.svg")
        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename(), follow=False, status=302)

        # We expect a 'dl' link instead of a 'media' link because
        # we hand off the download even in development versions.
        self.assertEqual(response.url, resource.download.url.replace('media', 'dl'))

        resource = Resource.objects.get(pk=resource.pk)
        # Downloads  cause increases in downloaded counter after #253
        self.assertEqual(resource.downed, 1)

        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename(), follow=False)

        resource = Resource.objects.get(pk=resource.pk)
        # Counter should remain zero until logs are processed
        self.assertEqual(resource.downed, 2)
        
    def test_download_non_existent_file(self):
        resources = Resource.objects.filter(published=True, downed=0)
        self.assertGreater(resources.count(), 0,
                           'Create a public resource with 0 downloads')
        resource = resources[0]
        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename() + 'I_don_t_exist', follow=True, status=200)

        # For some reason, escaped characters became hex rather than decimal
        self.assertContains(response, f"Can not find file &#x27;{resource.filename()}I_don_t_exist&#x27;")
        self.assertContains(response, resource.description())

        resource = Resource.objects.get(pk=resource.pk)
        self.assertEqual(resource.downed, 1)
        
    def test_download_non_public_file_not_owner(self):
        resource = ResourceFactory(published=False, user=self.different_user, downed=0)
        num_dl = resource.downed
        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename(), follow=False, status=404)

        resource = Resource.objects.get(pk=resource.pk)
        self.assertEqual(resource.downed, num_dl)
        

class UploadViewTests(BaseResourceViewTestCase):
    credentials = dict(username='tester', password='123456')

    def test_submit_item(self):
        """Tests views and templates for uploading a new resource file"""
        # This part could be repeated for different inputs/files to check for errors and correct saving, subtests? 
        # Could become a mess if all are in one test.
        num = Resource.objects.count() 
        
        # check GET
        response = self.assertGet('resource.upload')
        self.assertIsInstance(response.context['form'], ResourceForm)
        
        # check POST
        response = self.assertPost('resource.upload', data=self.data, status=200)
        self.assertEqual(Resource.objects.count(), num + 1)

    def test_submit_long_filename(self):
        """Submit an item with an extra large filename"""
        for x in range(92, 97):
            data = self.data
            name = 'x' * x
            data['download'] = self.open('file5.svg', name=name + '.svg')
            data['rendering'] = self.open('preview5.png', name=name + '.png')
            res = self.assertPost('resource.upload', data=data, status=200)

            out = res.context_data['object'].download.name
            self.assertLess(len(out), 101)
            self.assertEqual(out[:-12], 'resources/file/' + ('x' * 73))

            out = res.context_data['object'].rendering.name
            self.assertLess(len(out), 101)
            self.assertEqual(out[:-12], 'resources/render/' + ('x' * 71))

        name = 'x' * 97
        data['download'] = self.open('file5.svg', name=name + '.svg')
        data['rendering'] = self.open('preview5.png', name=name + '.png')
        self.assertPost('resource.upload', data=data, form_errors={
            'download': "Ensure this filename has at most 100 characters (it has 101).",
            'rendering': "Ensure this filename has at most 100 characters (it has 101)."
        })

    def test_submit_gallery_item(self):
        """Test if I can upload a file into my own gallery when a gallery is selected"""
        gallery = GalleryFactory()
        num = Resource.objects.count()
        
        (get, post) = self.assertBoth('resource.upload', gallery_id=gallery.pk, data=self.data, status=200)
        self.assertEqual(get.context['gallery'], gallery)
        self.assertIsInstance(get.context['form'], ResourceForm)
        self.assertEqual(Resource.objects.count(), num + 1)
        self.assertEqual(post.context['object'].gallery, gallery)

    def test_paste_text_POST(self):
        """Test pasting a text, a long one (success) 
        and a short one (fail)"""
        num = Resource.objects.count()
        data = {'download': "foo" * 100, "name": 'normal', "media_type": 'text/markdown', "license": 1}
        shortdata = {'download': "blah" * 5, "name": 'short'}
        
        (get, post) = self.assertBoth('pastebin', data=data, status=200)
        self.assertIsInstance(get.context['form'], ResourcePasteForm)
        self.assertEqual(Resource.objects.count(), num + 1)
        self.assertContains(post, "foofoo")
        
        response = self.assertPost('pastebin', data=shortdata, form_errors={
              '_default': None,
              'download': 'Text is too small for the pastebin.',
            })
        self.assertEqual(Resource.objects.count(), num + 1)
        self.assertContains(response, "blahblah")

    def test_submit_gallery_failure(self):
        """Test when no permission to submit exists"""
        gallery = GalleryFactory(user__username='another-user')
        num = Resource.objects.count()
        
        # check GET
        response = self.assertGet('resource.upload', gallery_id=gallery.pk, status=403)
        
        # check POST
        response = self.assertPost('resource.upload', gallery_id=gallery.pk, data=self.data, status=403)
        self.assertEqual(Resource.objects.count(), num)

    def test_submit_group_gallery(self):
        """Test to upload a resource when a gallery is group based, 
        and the user is member of the group, but not owner of the gallery"""
        group = self.user.groups.first()
        self.assertIsNotNone(group, f"User {self.user.username} must belong to at least one group")
        gallery = GalleryFactory(user__username='another-user', group=group)
        num = Resource.objects.count()

        # check GET
        response = self.assertGet('resource.upload', gallery_id=gallery.pk, status=200)
        self.assertEqual(response.context['gallery'], gallery)
        self.assertIsInstance(response.context['form'], ResourceForm)
        
        # check POST
        response = self.assertPost('resource.upload', gallery_id=gallery.pk,
            data=self.data, status=200)
        self.assertEqual(Resource.objects.count(), num + 1)
        self.assertEqual(response.context['object'].gallery, gallery)

    def test_drop_item(self):
        """Drag and drop file (ajax request)"""
        num = Resource.objects.count()
        
        (_, post) = self.assertBoth('resource.drop', status=200, data={
          'name': "New Name", 'download': self.download})
        self.assertEqual(Resource.objects.count(), num + 1)
        self.assertContains(post, 'OK|')

    def test_submit_item_quota_exceeded(self):
        """Check that submitting an item which would make the user exceed the quota will fail"""
        default_quota = Quota.objects.filter(group__isnull=True)[0]
        default_quota.size = 1 # 1024 byte
        default_quota.save()
        name = self.download.name
        quot = self.user.quota()

        self.assertGreater(os.path.getsize(name), quot,
            "Make sure that the file %s is bigger than %d byte" % (name, quot))

        response = self.assertPost('resource.upload', data=self.data,
            form_errors={'download': 'Not enough space to upload this file.'})

    def test_publish_item(self):
        """Check that we can publish our own items"""
        resource = ResourceFactory(published=False, user=self.user)

        # check POST
        response = self.assertGet('publish_resource', pk=resource.pk, status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).published, True)
        
        # Make sure nothing weird will happen when published twice.
        response = self.assertGet('publish_resource', pk=resource.pk, status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).published, True)

    def test_publish_another_persons_item(self):
        """Make sure we can't publish resources which are not ours"""
        resource = resource = ResourceFactory(published=False, user=self.different_user)
        self.assertBoth('publish_resource', pk=resource.pk, status=403)
        self.assertEqual(Resource.objects.get(pk=resource.pk).published, False)

    # Resource Edit tests:
    def test_edit_paste_being_the_owner(self):
        """Test if we can access the paste edit page for our own item"""
        # Make sure we own the file and that it IS a pasted text item
        resource = ResourceFactory(user=self.user, category__slug='pastebin')
        
        # check GET
        response = self.assertGet('edit_resource', pk=resource.pk)
        self.assertIsInstance(response.context['form'], ResourceEditPasteForm)
        self.assertContains(response, resource.name)
        
        # check POST
        data = {
            'name': 'New Name',
            'license': 1,
            'media_type': 'text/css',
            'download': 'A' * 300,
        }
        self.assertPost('edit_resource', pk=resource.pk, data=data, status=200)

    def test_edit_item_being_the_owner(self):
        """Test if we can edit an item which belongs to us"""
        # Make sure we own the file and that it's NOT a pasted text item
        resource = self.getObj(Resource, user=self.user, not_category=1)

        (get, _) = self.assertBoth('edit_resource', pk=resource.pk, data={'owner': True})
        self.assertIsInstance(get.context['form'], ResourceForm)
        self.assertContains(get, resource.name)
      
    def test_edit_item_by_other_user(self):
        """Check that we can't access the edit form for other people's items"""
        #make sure we don't own the file
        resources = Resource.objects.exclude(user=self.user)
        self.assertGreater(resources.count(), 0,
            "Create a resource which doesn't belong to user %s" % self.user)
        resource = resources[0]
        
        # check GET
        response = self.assertGet('edit_resource', pk=resource.pk, status=403)
        
        # check POST
        response = self.assertPost('edit_resource', pk=resource.pk, data=self.data, status=403)
        self.assertNotEqual(resource.description, self.data['desc'])
        
    # Resource Deletion tests:
    def test_delete_own_item(self):
        """Check that we can delete our own items"""
        resources = Resource.objects.filter(user=self.user)
        self.assertGreater(resources.count(), 0,
            "Create a resource which belongs to user %s" % self.user)
        resource = resources[0]
        
        # check GET
        response = self.assertGet('delete_resource', pk=resource.pk, status=200)
        
        # check POST
        # we need to patch the delete() method to avoid removing files needed in other tests
        # Please note that this may become unnecessary if/when we move to factories instead of fixtures
        with mock.patch.object(resource_storage, 'delete'):
            response = self.assertPost('delete_resource', pk=resource.pk, follow=False, status=302)
            with self.assertRaises(Resource.DoesNotExist):
                deleted = Resource.objects.get(pk=resource.pk)
        
        deleted = self.assertGet('resource', pk=resource.pk, status=404)
        
    def test_delete_another_persons_item(self):
        """Make sure that we can't delete other people's items"""
        resources = Resource.objects.exclude(user=self.user)
        self.assertGreater(resources.count(), 0,
            "Create a resource which does not belong to user %s" % self.user)
        resource = resources[0]
        
        self.assertGet('delete_resource', pk=resource.pk, status=403)
        self.assertPost('delete_resource', pk=resource.pk, status=403)
        
        self.assertEqual(resource, Resource.objects.get(pk=resource.pk))

    def test_no_revision_on_create(self):
        """We don't get a revision with a new resource"""
        self.assertPost('resource.drop', data={
          'name': "my_resource",
          'download': self.download,
        }, status=200)
        resource = Resource.objects.get(name='my_resource')
        self.assertEqual(resource.revisions.count(), 0)

    def test_no_revision_on_edit(self):
        """We don't get a revision with a no-file edit"""
        resource = ResourceFactory(user=self.user, category__slug='screenshot')
        data = self.data.copy()
        data.pop('download')
        data['name'] = 'updated'
        response = self.assertPost('edit_resource', pk=resource.pk, data=data, status=200)
        resource = Resource.objects.get(pk=resource.pk)
        self.assertEqual(resource.name, 'updated')
        self.assertEqual(resource.revisions.count(), 0)

    def test_new_revision_on_save(self):
        """We do get a revision with a file edit"""
        resource = ResourceFactory(user=self.user, category__slug='screenshot')
        self.assertPost('edit_resource', pk=resource.pk, data=self.data, status=200)
        resource = Resource.objects.get(pk=resource.pk)
        self.assertEqual(resource.name, 'Test Resource Title')
        self.assertEqual(resource.revisions.count(), 1)


class ResourceVoteTests(BaseResourceViewTestCase):
    credentials = dict(username='tester', password='123456')

    def test_like_item_not_being_owner(self):
        """Like a gallery item which belongs to someone else"""
        # Note that the initial value of Resource.liked is irrelevant here, because we have no Vote fixtures,
        # so any refresh will start from zero
        resource = ResourceFactory(user=self.different_user)

        response = self.assertGet('resource.like', pk=resource.pk, like='+', status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).liked, 1)
        
        # try a second time, should not increment
        response = self.assertGet('resource.like', pk=resource.pk, like='+')
        self.assertEqual(Resource.objects.get(pk=resource.pk).liked, 1)

        response = self.assertGet('resource.like', pk=resource.pk, like='-', status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).liked, 0)
        
        # and a second time, for good measure, shouldn't change anything
        response = self.assertGet('resource.like', pk=resource.pk, like='-', status=200)
        self.assertEqual(Resource.objects.get(pk=resource.pk).liked, 0)
        
    def test_like_unpublished_item_not_being_owner(self):
        """Like a gallery item which belongs to someone else, and is not public
        - should fail and not change anything in db"""
        resource = ResourceFactory(published=False, user=self.different_user, liked=0)
        previous_value = resource.liked
        # Make a request to this resource like link
        response = self.assertGet('resource.like', pk=resource.pk, like='+', follow=False, status=404)
        # This should not increment the counter
        self.assertEqual(resource.liked, previous_value)
        
    def test_like_item_being_owner(self):
        """Like a gallery item which belongs to me should fail"""
        # use the fact that counter would start with 1, if liking would work
        resource = self.own_resource
        num_likes = resource.liked

        # We only return a feedback message to the user, rather than returning 403
        self.assertGet('resource.like', pk=resource.pk, like='+')
        self.assertEqual(Resource.objects.get(pk=resource.pk).liked, num_likes)


class ResourceAnonTests(BaseResourceViewTestCase):
    """Tests for AnonymousUser"""
    
    def test_view_public_item_detail_anon(self):
        """Testing item detail view and template for public items,
        and make sure the view counter is correctly incremented (once per session)"""
        #make sure the file is public
        resources = Resource.objects.filter(published=True)
        self.assertGreater(resources.count(), 0,
            "Create a published resource")
        resource = resources[0]
        num_views = resource.viewed
        
        # the response already contains the updated number
        response = self.assertGet('resource', pk=resource.pk)
        self.assertEqual(response.context['object'], resource)
        self.assertContains(response, resource.filename())
        self.assertContains(response, resource.name)
        self.assertContains(response, resource.description())
        # we don't have any real views saved in the db, so we start with zero

        #self.assertEqual(response.context['object'].viewed, num_views + 1)
        
        # number of views should only be incremented once per user session
        #response = self.assertGet('resource', pk=resource.pk)
        #self.assertEqual(Resource.objects.get(pk=resource.pk).viewed, num_views + 1)
    
    def test_view_public_resource_full_screen_anon(self):
        """Check that an anonymous user can look at a
        resource in full screen, and that fullview number is incremented
        every time"""
        resource = ResourceFactory(published=True, fullview=0, svg=True)
        
        response = self.assertGet('view_resource', pk=resource.pk, follow=False, status=302)
        self.assertEqual(response.url, resource.download.url)

        # Full screen views are incremented after #253
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 1)
        
        # all full views are counted (like downloads)
        response = self.assertGet('view_resource', pk=resource.pk)

        # Full screen views are incremented after #253
        self.assertEqual(Resource.objects.get(pk=resource.pk).fullview, 2)

    def test_submit_item_anon(self):
        """Test if we can upload a file when we're not logged in,
        shouldn't be allowed and shouldn't work"""
        num = Resource.objects.count()
        
        self.assertGet('resource.upload', follow=False, status=302)
        self.assertPost('resource.upload', data=self.data, status=403)
        self.assertEqual(Resource.objects.count(), num)

    def test_drop_item_anon(self):
        """Drag and drop file (ajax request) when not logged in - shouldn't work"""
        num = Resource.objects.count()
        
        self.assertGet('resource.drop', status=302, follow=False)
        self.assertPost('resource.drop', data={
          'name': "New Name",
          'download': self.download,
        }, status=403)
        self.assertEqual(Resource.objects.count(), num)
    
    def test_paste_text_anon(self):
        """Test pasting a valid text when logged out (fail)"""
        num = Resource.objects.count()
        data = {'download': "foo" * 100,}
        
        self.assertGet('pastebin', status=302, follow=False)
        self.assertPost('pastebin', data=data, status=403)
        self.assertEqual(Resource.objects.count(), num)
    
        shortdata = {'download': "blah" * 5}
        response = self.assertPost('pastebin', data=shortdata, status=403)
    
    def test_like_item_anon(self):
        """Like a gallery item when logged out should fail"""
        resources = Resource.objects.all()
        self.assertGreater(resources.count(), 0,
            "Create a resource!")
        num_likes = resources[0].liked
        
        response = self.assertGet('resource.like', pk=resources[0].pk, like='+')
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertEqual(resources[0].liked, num_likes)
        
        response = self.assertGet('resource.like', pk=resources[0].pk, like='-')
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertEqual(resources[0].liked, num_likes)
        
    def test_like_unpublished_item_anon(self):
        """Like an unpublished gallery item when logged out should fail"""
        resource = ResourceFactory(published=False, liked=0)

        response = self.assertGet('resource.like', pk=resource.pk, like='+')
        self.assertTemplateUsed(response, 'registration/login.html')
        resource.refresh_from_db()
        self.assertEqual(resource.liked, 0)
    
    def test_publish_item_anon(self):
        """Make sure we can't publish resources when logged out"""
        resource = ResourceFactory(published=False)

        # check GET
        response = self.assertGet('publish_resource', pk=resource.pk, follow=False, status=302)
        
        # check POST
        response = self.assertPost('publish_resource', pk=resource.pk, status=403)
        self.assertEqual(Resource.objects.get(pk=resource.pk).published, False)
    
    def test_download_anon(self):
        """Download the actual file"""
        resource = ResourceFactory(published=True, downed=0, svg=True)
        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename(), follow=False, status=302)

        self.assertEqual(response.url, resource.download.url.replace('media', 'dl'))

        resource = Resource.objects.get(pk=resource.pk)

        # Downloads are counted on request after #253
        self.assertEqual(resource.downed, 1)
        
        # every download should increment the counter
        response = self.assertGet('download_resource', pk=resource.pk,
                       fn=resource.filename(), follow=False)
        resource = Resource.objects.get(pk=resource.pk)

        # Downloads aren't counted on request, but in logs XXX
        self.assertEqual(resource.downed, 2)

    def test_edit_item_anon(self):
        """Test that we can't edit any items when we are logged out"""
        resources = Resource.objects.all()
        self.assertGreater(resources.count(), 0,
            "Create a resource!")
        resource = resources[0]
        desc = resource.description
        
        # check GET
        response = self.assertGet('edit_resource', pk=resource.pk, follow=False, status=302)
        
        # check POST
        response = self.assertPost('edit_resource', pk=resource.pk, data=self.data, status=403)
        self.assertEqual(resource.description, desc)
    
    def test_delete_item_anon(self):
        """Make sure that we can't delete resources when we are logged out"""
        resources = Resource.objects.all()
        self.assertGreater(resources.count(), 0,
            "Create a resource")
        resource = resources[0]
        
        # check GET
        response = self.assertGet('delete_resource', pk=resource.pk, follow=False, status=302)
        
        # check POST
        response = self.assertPost('delete_resource', pk=resource.pk, status=403)
        self.assertEqual(resource, Resource.objects.get(pk=resource.pk))

    def test_video_view(self):
        """Make sure video links embed a vieo feature"""
        self.assertTrue(video_detect('http://youtube.com/watch?v=01234567911'))
        for resource in Resource.objects.filter(link__contains='VideoTag', published=True):
            response = self.assertGet('resource', pk=resource.pk)
            self.assertContains(response, '<iframe')
            self.assertContains(response, 'VideoTag')

    def test_endorsement(self):
        """Make sure GPG and Hashes work for downloads"""
        gpg_key = (
            "-----BEGIN PGP PUBLIC KEY BLOCK-----\nVersion: GnuPG "
            "v1\n\nmI0EVZ/z+wEEALgNIYf+UhxJDetqBTaAWPVwjmMVLM3aH6qrht+8B1Qc80YqL/ub"
            "\nd3W2vmsmEjvR7pYuHnpUBnmEnkkdVltEDwf1IO47MSieIlymIzN+DkREmCriNQML\nsZWkJ4/XCKgGKmWZoO2Ujsd/9I"
            "+Q4RXskhkTV4lClcBom9HIGue7i2dfABEBAAG0\nXElua3NjYXBlIFRlc3QgS2V5IChQYXNzd29yZCBpcyAndGVzdCcgKG9ubHkgZm9y"
            "\nIHJ1bm5pbmcgdGVzdHMpIElOU0VDVVJFKSA8dGVzdEBpbmtzY2FwZS5vcmc+iLgE\nEwECACIFAlWf8"
            "/sCGwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEPcRjblD\nZ00N75UD"
            "+wRzQ1ebkFZZCw1RqSvwEaf2GsHx9W09Ozg9TsiQ1zb4fC/KHWc/VJlC"
            "\nHRVbiQkEqMYTnLbxVFmuNHPv7Imt6gJsvoP0TBSQ5wQeryzvgcSLXmT6qXbBgf2M"
            "\ntnuLcbwz9uvckrYQfPLoAdLq1LSuk0oR1V4xY0Ja+oVD5446CuaNuI0EVZ/z+wEE"
            "\nAK0gfuDYC6plAV6fQ9x1H0gHcNNVNo2aOSy4E5VdoeWDJdHSd3loMlNjpm1i0MNJ\nkFC9mHFJxb/dYCl6XsSM9xaeZ0M81"
            "/sSLZeI9AZ2RgYCpNIV9b2hXuyljK1/9EJ3\nQM9Z45N26ZhyoVuXN2OeiyYP9ATSe9MFXfiF+fMVGnirABEBAAGInwQYAQIACQUC"
            "\nVZ/z+wIbDAAKCRD3EY25Q2dNDdvdA/9r5Or/nzegUp1X0VDovORhBAq3J+7C66Y4\n"
            "/WJ1Ax7CGHilGnME8YGdQxiHzJX9pTR9rmE12DABLATl2gElKC16ooC3dO+XRxT/\n4m7gSA6P/Oe4p8SNzeQuC0nlKeDS6qT3Py"
            "/HL0MyFJXeRFcfM13qkSFlmrHXLsDB\nbGg+4pPhxA==\n=fOA9\n-----END PGP PUBLIC KEY BLOCK-----")

        fixture_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures/media/test/')
        ResourceFactory(signature=factory.django.FileField(from_path=os.path.join(fixture_path, 'good.md5')),
                        download=factory.django.FileField(from_path=os.path.join(fixture_path, 'preview5.png')))
        ResourceFactory(signature=factory.django.FileField(from_path=os.path.join(fixture_path, 'good.sig')),
                        download=factory.django.FileField(from_path=os.path.join(fixture_path, 'file1.svg')),
                        user__username='somethingelse', user__gpg_key=gpg_key)
        ResourceFactory(signature=factory.django.FileField(from_path=os.path.join(fixture_path, 'bad.sig')),
                        download=factory.django.FileField(from_path=os.path.join(fixture_path, 'file2.txt')))
        self.assertEndorsement(signature='')
        self.assertEndorsement(Resource.ENDORSE_HASH, signature__contains='good.md5')
        self.assertEndorsement(Resource.ENDORSE_SIGN, signature__contains='good.sig')
        self.assertEndorsement(signature__contains='bad')


