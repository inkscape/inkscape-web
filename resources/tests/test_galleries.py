#
# Copyright 2015, Maren Hachmann <marenhachmann@yahoo.com>
#                 Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test Resource Items and Lists
"""
import factory
from django.urls import reverse

from person.factories import UserFactory, GroupFactory, TeamFactory
from .base import BaseCase

from django.utils.timezone import now

from django.core.management import call_command

from resources.models import Resource, Gallery, Category
from resources.views import ResourceList
from resources.forms import GalleryForm

from person.models import User
from ..factories import GalleryFactory, ResourceFactory


class GalleryUserTests(BaseCase):
    """Gallery viewing and sorting tests"""
    credentials = dict(username='tester', password='123456')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        teams = [TeamFactory(group__name='Everyone'), TeamFactory(group__name='More Exclusive')]
        cls.groups = [x.group for x in teams]
        cls.user = UserFactory(username='tester', groups=cls.groups)
        cls.another_user = UserFactory(username='another_user')
        galleries = GalleryFactory.create_batch(3, user=cls.user)
        ResourceFactory(galleries=galleries[:1], user=cls.user)
        ResourceFactory(user=cls.user)

    def test_view_global_gallery(self):
        """Look at the gallery containing every public resource from everyone"""
        ResourceFactory.create_batch(3, user__username=factory.Faker('user_name'))
        resources = Resource.objects.filter(published=True).order_by('-liked')
        self.assertGreater(resources.count(), 3,
                           "Create a few public resources for the global gallery")

        response = self.assertGet('resources', status=200)
        self.assertEqual(response.context['object_list'].count(), resources.count())
        # make sure we see uploads from different people
        self.assertGreater(len(set([item.user for item in response.context['object_list']])), 1)
        # make sure every resource is displayed with either the correct licence
        # or an edit link, when it's ours
        pos = 0

        with open('/tmp/result.txt', 'w') as fhl:
            fhl.write(response.content.decode())

        for resource in resources:
            self.assertTrue(resource.is_available(), "File not available: %s" % resource.download.name)
            self.assertTrue(resource.is_visible(), "File not visible: %s" % resource.download.name)
            if resource.user != self.user:
                search_term = resource.license.value
            else:
                search_term = reverse('edit_resource', kwargs={'pk': resource.pk})
            new_pos = response.content.decode().find(str(search_term), pos)
            self.assertGreater(new_pos, -1, "%s:'%s' not found, does the file exist?" % (str(resource), search_term))
            pos = new_pos

        #and we can't upload here directly
        self.assertNotContains(response, '<form method="POST" action="' + reverse('new_gallery'))

    def test_narrow_global_gallery(self):
        """make sure we can choose to see only the resources
        we want to see in the global gallery"""
        ResourceFactory.create_batch(3, published=True)
        resources = Resource.objects.filter(published=True)
        self.assertGreater(resources.count(), 3,
                           "Create a few public resources for the global gallery")

        categories = Category.objects.filter(id__in=resources.values('category_id'), filterable=True)
        self.assertGreater(categories.count(), 2,
                           "Create a few categories for the global gallery, and assign public resources to them")

        for category in categories:
            items = resources.filter(category=category.pk)

            response = self.assertGet('resources', category=category.value, status=200)
            self.assertEqual(response.context['object_list'].count(),
                             items.count(), 'The number of items in category %s is not correct' % category.value)
            for item in items:
                self.assertIn(item, response.context['object_list'])
                self.assertContains(response, item.name)

    def test_sort_global_gallery(self):
        "test if ordering for global galleries works as expected"
        ResourceFactory.create_batch(2, user__username=factory.Faker('user_name'))
        resources = Resource.objects.filter(published=True)
        self.assertGreater(resources.count(), 3,
                           "Create a few public resources for the global gallery")

        baseresponse = self.assertGet('resources')
        orderlist = [ordering for ordering, label in ResourceList.orders if label is not None]
        self.assertGreater(len(orderlist), 3,
                           "Create some possible orderings for your gallery")
        rev_orderlist = [o[1:] if o[0]=='-' else '-' + o for o in orderlist]

        #the generator nature of 'orders' in template context doesn't allow us
        #to use that for testing because it's already 'exhausted'

        #make sure the links to the reverse standard order are in the html
        for rev_order in rev_orderlist:
            self.assertContains(baseresponse, rev_order)

        #test normal and reverse order
        for order in orderlist + rev_orderlist:
            ordered = resources.order_by(order)
            response = self.client.get(reverse('resources') + '?order=' + order, status=200)
            #conveniently respects ordering when checking for equality
            self.assertEqual(list(response.context['object_list']), list(ordered))

            #objects in html in correct order of appearance?
            for i in range(1, len(ordered)):
                first_name = ordered[i-1].name
                second_name = ordered[i].name
                self.assertGreater(response.content.decode().find(str(second_name)),
                                  response.content.decode().find(str(first_name)))

    def test_view_user_gallery_owner(self):
        """Look at all my own uploads"""
        resources = Resource.objects.filter(user=self.user)
        self.assertGreater(resources.count(), 1,
                           "Create another resource for user %s" % self.user)

        response = self.assertGet('resources', username=self.user.username, status=200)
        for resource in resources:
            self.assertContains(response, resource.name)
        self.assertContains(response, self.user.username)
        self.assertEqual(response.context['object_list'].count(), resources.count())
        self.assertContains(response, '<form method="POST" action="' + reverse('new_gallery'))

    def test_view_user_gallery_not_owner(self):
        """Look at all uploads by another user"""
        # We need a user that's not self.user, with published resources
        owner = UserFactory(username='owner_of_a_lonely_heart')
        ResourceFactory.create_batch(2, user=owner, published=True)
        resources = Resource.objects.filter(user=owner, published=True)
        self.assertGreater(resources.count(), 1,
                           "Create another public resource for user %s" % owner)

        response = self.assertGet('resources', username=owner.username, status=200)
        for resource in resources:
            self.assertContains(response, resource.name)
        self.assertContains(response, owner.username)
        self.assertEqual(response.context['object_list'].count(), resources.count())
        self.assertNotContains(response, '<form method="POST" action="' + reverse('new_gallery'))

    def test_view_group_gallery(self):
        """Look at a global gallery belonging to a group of users (team in UI),
        containing all items that have been uploaded into its subgalleries
        not being a member of that group. After this, look at a subgallery, to
        see if it contains the right items, too."""

        galleries = GalleryFactory.create_batch(2, user__username='owner', group=TeamFactory().group)
        subgallery = galleries[0]
        other_subgallery = galleries[1]

        this_group = subgallery.group

        #add resources to both subgalleries
        resource_owner = subgallery.group.user_set.all()[0]
        resources = ResourceFactory.create_batch(3, user=resource_owner, published=True)
        subgallery.items.add(resources[0], resources[1])
        other_subgallery.items.add(resources[2])

        all_this_groups_items = [item for gal in this_group.galleries.all() for item in gal.items.all()]

        # First part: fetch global team gallery page, containing all subgalleries and all their resources
        response = self.assertGet('resources', team=subgallery.group.team.slug, status=200)

        # make sure all resources from all subgalleries are on that page
        self.assertEqual(len(all_this_groups_items), response.context['object_list'].count())
        for item in subgallery.items.all():
            self.assertContains(response, item.name)

        # Second part: fetch a team's subgallery and check if it contains the right resources
        response = self.assertGet('resources', galleries=subgallery.slug, team=subgallery.group.team.slug, status=200)

        self.assertEqual(subgallery.items.count(), response.context['object_list'].count())
        for item in subgallery.items.all():
            self.assertContains(response, item.name)

    def test_narrow_user_gallery_owner(self):
        """make sure we can choose to see only the resources
        we want to see in our own gallery"""
        ResourceFactory.create_batch(2, user=self.user)
        resources = Resource.objects.filter(user=self.user, category__filterable=True)
        self.assertGreater(resources.count(), 2,
                           "Create a few resources for user %s" % self.user)

        cat_ids = resources.values_list('category_id', flat=True)
        categories = Category.objects.filter(id__in=cat_ids)
        self.assertGreater(categories.count(), 2,
                "Create a few categories for the global gallery, and assign public resources to them: %s" % str(cat_ids))

        for category in categories:
            items = resources.filter(category=category.pk)

            response = self.assertGet('resources', username=self.user.username, category=category.value, status=200)
            self.assertEqual(response.context['object_list'].count(),
                             items.count(), 'The number of items in category %s is not correct' % category.value)
            for item in items:
                self.assertIn(item, response.context['object_list'])
                self.assertContains(response, item.name)

    def test_narrow_user_gallery_not_owner(self):
        """make sure we choose a category in a stranger's gallery"""
        owner = self.another_user
        ResourceFactory.create_batch(3, user=owner, published=True)
        resources = Resource.objects.filter(user=owner, published=True)
        self.assertGreater(resources.count(), 2,
                           "Create a few resources for user %s" % owner)

        categories = Category.objects.filter(id__in=resources.values('category_id'))
        self.assertGreater(categories.count(), 2,
                           "Create more different categories for public resources by user %s " % owner )

        for category in categories:
            items = resources.filter(category=category.pk)

            response = self.assertGet('resources', username=owner.username, category=category.value, status=200)
            self.assertEqual(response.context['object_list'].count(),
                             items.count(), 'The number of items in category %s is not correct' % category.value)
            for item in items:
                self.assertIn(item, response.context['object_list'])
                self.assertContains(response, item.name)

    # Gallery Search tests
    def test_global_gallery_search(self):
        """Tests the search functionality in galleries"""
        q = {'q': '+description searchterm2 searchterm1 -Eight'}
        ResourceFactory(published=True, desc='description with Eight', name='just noise')
        res1 = ResourceFactory(published=True, desc='description including searchterm1', liked=3, name='res1')
        res2 = ResourceFactory(published=True, desc='description including searchterm2', liked=6, name='res2')
        self.refresh_haystack()
        resources = Resource.objects.filter(published=True).exclude(desc__contains='Eight')\
                                    .filter(desc__contains='description').order_by('-liked')
        self.assertGreater(resources.count(), 0,
                           "Create a public resource which complies to the search query")
        response = self.assertGet('resources', query=q, status=200)

        self.assertEqual(
            [int(a.pk) for a in response.context['object_list']],
            [b.pk for b in resources])

        searchterms = [resource.name for resource in resources]

        for term in searchterms:
            self.assertContains(response, ">%s<" % term, 1)

        # The name raw appears in urls, so we look for a tagged name instead
        self.assertNotContains(response, '>Item Eight<')

    def test_user_gallery_search(self):
        """Test that we can search for a user's items in that user's global gallery"""
        owner = UserFactory(username='owner')
        ResourceFactory(user=owner, published=True, name='Resource Seven')
        ResourceFactory(user=owner, published=True, name='Resource Four')
        resources = Resource.objects.filter(user=owner, published=True).exclude(name__contains="Four")\
                                    .filter(name__contains="Seven").order_by('-liked')
        self.assertGreater(resources.count(), 0,
                           "Create a public resource which complies to the search query for user %s" % owner)
        self.refresh_haystack()

        q = {'q': 'Seven -Four'}
        response = self.assertGet('resources', username=owner.username, query=q, status=200)

        self.assertEqual(
            [int(a.pk) for a in response.context['object_list']],
            [b.pk for b in resources])

        searchterms = [resource.name for resource in resources]

        for term in searchterms:
            self.assertContains(response, ">%s<" % term, 1)

        # The name raw appears in urls, so we look for a tagged name instead
        self.assertNotContains(response, '>Resource Four<')

    def test_specific_gallery_search(self):
        """Test that we can search items in a specific gallery (not global or all items for user)"""
        visitor = UserFactory(username="visitor")
        owner = UserFactory(username='owner')
        gallery = GalleryFactory(user=owner)
        item_search = ResourceFactory(user=owner, published=True, name='Resource Seven', galleries=[gallery])
        item_exclude = ResourceFactory(user=owner, published=True, name='Resource Four', galleries=[gallery])

        self.client.force_login(visitor)
        self.refresh_haystack()

        q = {'q': 'Seven -Four'}
        response = self.assertGet('resources', username=owner.username, galleries=gallery.slug, query=q, status=200)

        self.assertEqual(int(response.context['object_list'][0].pk), item_search.pk)
        self.assertContains(response, ">%s<" % item_search.name, 1)

        # The name raw appears in urls, so we look for a tagged name instead
        self.assertNotContains(response, '>%s<' % item_exclude.name)

    def assertMoveResource(self, res, src, to, status=200, a=1, b=1, get=200, move='move'):
        """Assert the moving of resources between galleries"""
        kw = dict(pk=res.pk, status=get)
        if src is not None:
            kw['source'] = src and src.pk
        response = self.assertGet('resource.%s' % move, **kw)
        if get == 200:
            self.assertContains(response, res.name)
            # this distinguishes move from copy language-independently
            if move == 'copy':
                self.assertNotContains(response, str(src) + '</option>')
            elif move == 'move':
                self.assertContains(response, str(src) + '</option>')

        kw['status'] = status
        self.assertPost('resource.%s' % move, data={'target': to.pk}, **kw)
        src and self.assertInGallery(res, src, a)
        to and self.assertInGallery(res, to, b)

    def assertCopyResource(self, res, to, status=200, a=1, b=1, get=200):
        """Assert the copying of resources between galleries"""
        return self.assertMoveResource(res, None, to, status, a, b, get, 'copy')

    def assertInGallery(self, resource, gallery, is_in=True):
        """Test a gallery contains the given item (or does not contain)"""
        if is_in:
            self.assertIn(resource, gallery.items.all())
        else:
            self.assertNotIn(resource, gallery.items.all())

# ====== Gallery Move and Copy resources tests ====== #

    def test_move_item_to_gallery(self):
        """Make sure an item can be moved from one gallery to another by its
        owner, make sure template works correctly"""
        (src, to) = self.getObj(self.user.galleries.all(), count=2)
        resource = self.getObj(Resource, user=self.user)

        # add a resource which belongs to us to the gallery
        src.items.add(resource)

        # move that resource to another gallery
        self.assertMoveResource(resource, src, to, 200, 0, 1)

    def test_move_item_to_gallery_not_gal_owner(self):
        """Make sure that we cannot move items from our own gallery
        into a gallery which isn't ours, and not a gallery for a group we're in"""
        src = GalleryFactory(user=self.user, group=None)
        to = GalleryFactory(user=self.another_user, group=GroupFactory())
        resource = ResourceFactory(user=self.user)

        # add a resource which belongs to us to the gallery
        src.items.add(resource)

        # move that resource to a stranger's gallery
        self.assertMoveResource(resource, src, to, 403, 1, 0)

    def test_move_item_to_group_gallery_member(self):
        """Make sure that we can move items into a group gallery
        if we are a member (not owner) of the group"""
        src = GalleryFactory(user=self.user)
        to = GalleryFactory(user=self.another_user, group=self.groups[0])
        resource = self.getObj(Resource, user=self.user)

        # add a resource which belongs to us to the team gallery
        src.items.add(resource)

        # move that resource to another gallery
        self.assertMoveResource(resource, src, to, 200, 0, 1)

    def test_move_item_from_group_gallery_member(self):
        """
        Make sure that we can move our own items out of a group gallery if we
        are a member (not owner) of the group, but not other people's items.
        """
        src = GalleryFactory(user__username='another_user', group=self.groups[0])
        to = GalleryFactory(user=self.user, group=None)
        resource = ResourceFactory(user=self.user)

        # add a resource which belongs to us to the gallery
        src.items.add(resource)

        # move that resource to another gallery
        self.assertMoveResource(resource, src, to, 200, 0, 1)

        # add a resource which does *not* belong to us to the group gallery
        resource = ResourceFactory(user=src.user)
        src.items.add(resource)

        # try to move that resource to a gallery that is mine
        self.assertMoveResource(resource, src, to, 403, 1, 0)

    def test_move_strangers_item_from_group_gallery(self):
        """Make sure group members can move items out of group galleries"""
        # I can't move it to my own gallery (forbidden, because it's not my resource),
        # I can't move it to another user's gallery (forbidden, because it's not my gallery),
        # We can only check if moving it to another gallery belonging to the same group works,
        # and later that moving it to a gallery of another group doesn't
        # We really need a real 'delete' for this, despite the 'move' being funky...
        src = GalleryFactory(user=self.another_user, group=self.groups[0])
        to = GalleryFactory(user=self.another_user, group=self.groups[0])

        # add a resource which does *not* belong to us, but to someone
        # totally unrelated (e.g. a member that left the group),
        # to the source gallery,
        unrelated_user = UserFactory(username='unrelated_user')
        resource = ResourceFactory(user=unrelated_user)

        src.items.add(resource)

        # try to move that stranger's resource to another gallery belonging to the same group
        self.assertMoveResource(resource, src, to, 200, 1, 1)

        # second half: try to move that stranger's resource to another gallery
        # belonging to another group
        src.items.add(resource)
        to = self.getObj(Gallery, not_group_id=src.group.pk)

        # try to move a stranger's resource from a gallery belonging to a group I am in
        # to a gallery of a group I am not in
        self.assertMoveResource(resource, src, to, 403, 1, 0)

    def test_move_item_to_gallery_not_item_owner(self):
        """Make sure that we cannot move items around that don't belong to us
        (from stranger's gallery to our own)"""
        user = self.another_user
        src = GalleryFactory(user=user, group=TeamFactory().group)
        to = GalleryFactory(user=user, group=self.groups[0])

        # add a resource which belongs to someone else to that person's gallery
        resource = ResourceFactory(user=user)
        src.items.add(resource)

        # move that resource to another gallery
        self.assertMoveResource(resource, src, to, 403, 1, 0, get=403)

    def test_move_item_from_non_existent_gallery(self):
        """ensure we get a 404 if we try to move an item out of a gallery
        which does not exist"""
        to = GalleryFactory(user=self.user, group=TeamFactory().group)
        resource = ResourceFactory(user=self.user)

        # try to move the resource from nonexistant gallery to my gallery
        self.assertMoveResource(resource, 0, to, 404, 0, 0, get=404)

    def test_copy_item_to_gallery(self):
        """Make sure an item can be copied from one gallery to another by its owner,
        also when the item isn't in any gallery, make sure template works correctly"""
        (src, to) = self.getObj(self.user.galleries.all(), count=2)

        # add a resource which belongs to us to a gallery
        resource = self.getObj(self.user.resources.all())
        src.items.add(resource)

        self.assertCopyResource(resource, to, 200, 1, 1)

        # test the same for a resource that isn't in any gallery
        resource = self.getObj(self.user.resources.filter(galleries=None))
        self.assertCopyResource(resource, to, 200, 1, 1)

    def test_copy_item_to_gallery_not_gal_owner(self):
        """Make sure that we cannot copy items into a gallery which isn't ours,
        and not a gallery for a group we're in"""
        src = self.getObj(self.user.galleries.all())
        to = GalleryFactory(user__username='another')

        # add a resource which belongs to us to a gallery
        resource = self.getObj(self.user.resources.all())
        src.items.add(resource)

        self.assertCopyResource(resource, to, 403, 1, 0)

    def test_copy_item_to_group_gallery_member(self):
        """Make sure that we can copy own items into a group gallery
        if we are a member (not owner) of the group"""
        group = self.user.groups.first()
        self.assertIsNotNone(group)
        to = GalleryFactory(group=group)
        resource = self.getObj(self.user.resources.all())
        self.assertCopyResource(resource, to, 200, 1, 1)

    def test_copy_item_to_gallery_not_item_owner(self):
        """Make sure we cannot copy items that do not belong to us"""
        to = self.getObj(self.user.galleries.all())
        resource = ResourceFactory(user__username='another')
        self.assertCopyResource(resource, to, 403, 1, 0, get=403)

    # Gallery Edit tests
    def test_edit_my_gallery(self):
        """Make sure that we can change the name of our own gallery"""
        galleries = self.user.galleries.filter(group=None)
        self.assertGreater(galleries.count(), 0)
        gallery = galleries[0]
        oldname = gallery.name

        # check GET
        response = self.assertGet('gallery.edit', gallery_id=gallery.pk, status=200)
        self.assertIsInstance(response.context['form'], GalleryForm)
        self.assertContains(response, gallery.name)

        # check POST
        response = self.assertPost('gallery.edit', gallery_id=gallery.pk, data={"name": "New Name", "group": ""}, status=200)
        self.assertContains(response, "New Name")
        self.assertEqual(Gallery.objects.get(pk=gallery.pk).name, "New Name")
        self.assertEqual(Gallery.objects.filter(name=oldname).count(), 0)

    def test_edit_group_gallery_member_fail(self):
        """Make sure that people who do not own a team gallery, but who are members of that gallery's team,
           cannot change the name/team for that gallery"""
        not_owned_gallery = GalleryFactory(group=self.user.groups.first(), user__username='another')

        oldname = not_owned_gallery.name

        self.assertBoth('gallery.edit', gallery_id=not_owned_gallery.pk, data={"name": "New Name", "group": ""},
                        status=403)
        self.assertEqual(Gallery.objects.get(pk=not_owned_gallery.pk).name, oldname)

    def test_edit_group_gallery_owner(self):
        """Make sure that group owners can change the name/team of their group
        gallery"""
        gallery = GalleryFactory(group=self.user.groups.first(), user=self.user)
        oldname = gallery.name

        # check GET
        response = self.assertGet('gallery.edit', gallery_id=gallery.pk, status=200)
        self.assertIsInstance(response.context['form'], GalleryForm)
        self.assertContains(response, gallery.name)

        # check POST
        response = self.assertPost('gallery.edit', gallery_id=gallery.pk,
                                   data={"name": "New Name", "group": ""}, status=200)
        self.assertContains(response, "New Name")
        self.assertEqual(Gallery.objects.get(pk=gallery.pk).name, "New Name")
        self.assertEqual(Gallery.objects.filter(name=oldname).count(), 0)

    def test_edit_unrelated_gallery(self):
        """Make sure that everyone unrelated to a
        gallery cannot edit it"""
        gallery = GalleryFactory(user__username='different_user')
        oldname = gallery.name

        self.assertBoth('gallery.edit', gallery_id=gallery.pk, data={"name": "New Name", "group": ""}, status=403)
        self.assertEqual(Gallery.objects.get(pk=gallery.pk).name, oldname)

    # Gallery deletion tests
    def test_gallery_deletion_own_gallery(self):
        """Test if galleries can be deleted by owner"""
        galleries = Gallery.objects.filter(user=self.user)
        self.assertGreater(galleries.count(), 0,
                           "Create a gallery which belongs to user %s" % self.user)
        gallery = galleries[0]

        # check GET
        response = self.assertGet('gallery.delete', gallery_id=gallery.pk, status=200)
        self.assertContains(response, gallery.name)
        self.assertEqual(Gallery.objects.get(pk=gallery.pk), gallery)

        # check POST
        response = self.assertPost('gallery.delete', gallery_id=gallery.id, status=200)
        with self.assertRaises(Gallery.DoesNotExist):
            Gallery.objects.get(pk=gallery.pk)

    def test_gallery_deletion_group_gallery(self):
        """Make sure galleries can be deleted by group member"""
        gallery = GalleryFactory(user__username='different_user', group=self.user.groups.last())

        response = self.assertGet('gallery.delete', gallery_id=gallery.pk, status=200)
        self.assertContains(response, gallery.name)
        self.assertEqual(Gallery.objects.get(pk=gallery.pk), gallery)

        response = self.assertPost('gallery.delete', gallery_id=gallery.pk, status=200)
        with self.assertRaises(Gallery.DoesNotExist):
            Gallery.objects.get(pk=gallery.pk)

    def test_gallery_deletion_group_gallery_non_member(self):
        """Make sure galleries can't be deleted by someone unrelated to the gallery"""
        gallery = GalleryFactory(user__username='different_user', group=GroupFactory())
        self.assertBoth('gallery.delete', gallery_id=gallery.id, status=403)

    # Gallery RSS tests
    def test_gallery_rss_feed(self):
        """Make sure that the main gallery has the correct rss feed, also for
        categories, ordering, subgalleries"""
        # RSS Feed only shows things in the last month
        Resource.objects.all().update(created=now())
        ResourceFactory.create_batch(3, user=self.another_user)
        categories = Category.objects.filter(filterable=True)
        resources = Resource.objects.filter(published=True)

        self.assertGreater(resources.count(), 3, "Create some published resources!")
        response = self.assertGet('resources_rss')
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        self.assertContains(response, "</item>", resources.count())
        for resource in resources:
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

        # select a category
        cat = resources.filter(category__in=categories)[0].category
        cat_resources = Resource.objects.filter(published=True, category=cat)
        response = self.assertGet('resources_rss', category=cat.value)
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        self.assertContains(response, "</item>", cat_resources.count())
        for resource in cat_resources:
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

        # select a username, which is used for all the rest of test
        resources = Resource.objects.exclude(user=self.user) # prevent hassle with unpublished items visible to owner
        u = resources[0].user
        user_resources = resources.filter(user=u, published=True)
        response = self.assertGet('resources_rss', username=u.username)
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        self.assertContains(response, "</item>", user_resources.count())
        for resource in user_resources:
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

        # select a user + a category
        cat = user_resources[0].category
        u_c_resources = user_resources.filter(category=cat)
        response = self.assertGet('resources_rss', username=u.username, category=cat.value)
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        self.assertContains(response, "</item>", u_c_resources.count())
        for resource in u_c_resources:
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

        # select a user + a gallery
        GalleryFactory(user=self.another_user)
        user_galleries = u.galleries.all()
        self.assertGreater(user_galleries.count(), 0, "Create a gallery for user %s" % u.username)
        g = user_galleries[0]
        # add a resource to that gallery, so we have some contents
        g.items.add(user_resources[0])
        response = self.assertGet('resources_rss', username=u.username, galleries=g.slug)
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        self.assertContains(response, "</item>", g.items.count())
        for resource in user_resources.filter(galleries=g):
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

        # select user, gallery and category
        cat = user_resources[0].category
        response = self.assertGet('resources_rss', username=u.username, galleries=g.slug, category=cat.value)
        self.assertEqual(response['Content-Type'][:19], 'application/rss+xml')
        expected = user_resources.filter(galleries=g, category=cat)
        self.assertContains(response, "</item>", expected.count())
        for resource in expected:
            self.assertContains(response, resource.name)
            self.assertContains(response, resource.get_absolute_url())

class GalleryAnonTests(BaseCase):
    """Tests for AnonymousUser"""
    def test_view_all_resources_by_user(self):
        """Look at all uploads from someone, and see only public items"""
        user = UserFactory()
        resources = ResourceFactory.create_batch(2, published=True, user=user)
        response = self.assertGet('resources', username=user.username, status=200)
        self.assertContains(response, resources[0].name)
        self.assertContains(response, user.username)
        self.assertEqual(response.context['object_list'].count(), len(resources))

    def test_gallery_deletion_anon(self):
        """Make sure galleries can't be deleted AnonymousUser"""
        gallery = GalleryFactory()
        self.assertGet('gallery.delete', gallery_id=gallery.id, follow=False, status=302)
        self.assertPost('gallery.delete', gallery_id=gallery.id, status=403)
        self.assertEqual(Gallery.objects.get(pk=gallery.pk), gallery)

