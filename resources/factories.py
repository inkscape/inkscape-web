import factory
from django.utils.text import slugify

import resources.models

# Smallest valid PDF
MIN_PDF = b"""%PDF-1.
1 0 obj<</Pages 2 0 R>>endobj
2 0 obj<</Kids[3 0 R]/Count 1>>endobj
3 0 obj<</Parent 2 0 R>>endobj
trailer <</Root 1 0 R>>
"""


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = resources.models.Category
        django_get_or_create = ('slug', )

    name = factory.Faker('word')
    slug = factory.LazyAttribute(lambda o: slugify(o.name))

    @factory.post_generation
    def acceptable_licenses(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted is not None:
            self.acceptable_licenses.set(extracted)

class LicenseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = resources.models.License


class ResourceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = resources.models.Resource

    class Params:
        svg = factory.Trait(
            media_type="image/svg+xml",
            download=factory.django.FileField(data="<svg>Content</svg>",
                                              filename="file1.svg")
        )
        pdf = factory.Trait(
            download=factory.django.FileField(data=MIN_PDF,
                                              filename="file1.pdf")
        )

    user = factory.SubFactory('person.factories.UserFactory')
    license = factory.SubFactory(LicenseFactory)
    category = factory.SubFactory(CategoryFactory)
    name = factory.Faker('text', max_nb_chars=50)
    download = factory.django.FileField(data='some text', filename='file1.txt')
    published = True

    @factory.post_generation
    def galleries(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted is not None:
            self.galleries.set(extracted)


class GalleryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = resources.models.Gallery
    user = factory.SubFactory('person.factories.UserFactory')
    name = factory.Faker('text')
