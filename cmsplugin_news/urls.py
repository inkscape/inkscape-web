#
# Copyright 2015, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""News app which is intergrated into the django-cms system"""

from django.urls import re_path
from inkscape.url_utils import url_tree
from person import team_urls

from .feeds import NewsFeed, BlogFeed
from .views import (
    ArchiveIndexView, YearArchiveView, MonthArchiveView, DayArchiveView,
    DetailView, DetailNews, UploadMedia,
)

app_name = 'news'

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^$', ArchiveIndexView.as_view(), name='archive_index'),
    re_path(r'^unpublished/(?P<pk>\d+)/$', DetailNews.as_view(), name="item"),
    re_path(r'admin/upload/', UploadMedia.as_view(), name='upload'),

    url_tree(
        r'^(?P<year>\d{4})/',
        re_path(r'^$', YearArchiveView.as_view(), name='archive_year'),
        url_tree(
            r'^(?P<month>\d{2})/',
            re_path(r'^$', MonthArchiveView.as_view(), name='archive_month'),
            url_tree(
                r'^(?P<day>\d{2})/',
                re_path(r'^$', DayArchiveView.as_view(), name='archive_day'),
                re_path(r'^(?P<slug>[-\w]+)/$', DetailView.as_view(), name='detail'),
            ),
        ),
    ),
    re_path(r'^feed/$', NewsFeed(), name='rss'),
]

team_urls.urlpatterns += [
    url_tree(r"^blog/",
        re_path(r'^$', ArchiveIndexView.as_view(), name='team_blog_index'),
        re_path(r'^feed/$', BlogFeed(), name='team_blog_rss'),
        re_path(r'^(?P<slug>[-\w]+)/$', DetailNews.as_view(), name='team_blog_detail'),
    ),
]
