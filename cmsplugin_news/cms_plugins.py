#
# Copyright 2015, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom 
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#

from django.utils.translation import gettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from cmsplugin_news.models import LatestNewsPlugin, News
from cmsplugin_news import settings

from cms.utils import get_language_from_request
from django.db.models import Q

from .alert import NewNewsAlert


class CMSLatestNewsPlugin(CMSPluginBase):
    """
        Plugin class for the latest news
    """
    model = LatestNewsPlugin
    name = _('Latest news')
    render_template = "cmsplugin_news/latest_news.html"
    text_enabled = True

    def render(self, context, instance, placeholder):
        """
            Render the latest news
        """
        request = context['request']
        language = get_language_from_request(request)
        team = None
        if instance.group:
            team = instance.group.team
        qset = News.published.with_language(
            language,
            is_staff=False, #request.user.has_perm('cmsplugin_news.change_news'),
            team=team)
        latest = qset[:instance.limit]
        context.update({
            'team': team,
            'instance': instance,
            'latest': latest,
            'alert': NewNewsAlert.get_alert_type(),
            'placeholder': placeholder,
        })
        return context

if not settings.DISABLE_LATEST_NEWS_PLUGIN:
    plugin_pool.register_plugin(CMSLatestNewsPlugin)
