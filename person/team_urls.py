# -*- coding: utf-8 -*-
#
# Copyright 2013-2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
URLs that start with the *teamname pattern, and so are 'owned' by a specific team.
"""
from django.urls import include, re_path
from inkscape.url_utils import url_tree

from .views import (
    TeamDetail, EditTeam, AddMember, RemoveMember, WatchTeam, UnwatchTeam,
    TeamCharter, ChatWithTeam, MembershipRequestView, ContactAllTeam,
    TeamMemberList,
)

# '\*(?P<team>[^\/]+)/'
urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^$', TeamDetail.as_view(), name='team'),
    re_path(r'^edit/$', EditTeam.as_view(), name='team.edit'),
    re_path(r'^message/$', ContactAllTeam.as_view(), name='team.message'),
    re_path(r'^join/$', AddMember.as_view(), name='team.join'),
    re_path(r'^watch/$', WatchTeam.as_view(), name='team.watch'),
    re_path(r'^leave/$', RemoveMember.as_view(), name='team.leave'),
    re_path(r'^unwatch/$', UnwatchTeam.as_view(), name='team.unwatch'),
    re_path(r'^membership/$', MembershipRequestView.as_view(), name='team.membership'),
    re_path(r'^membership/(?P<pk>\d+)/$', MembershipRequestView.as_view(), name='team.membership'),
    re_path(r'^charter/$', TeamCharter.as_view(), name='team.charter'),
    re_path(r'^list.txt$', TeamMemberList.as_view(), name='team.list'),
    re_path(r'^elections/', include('elections.urls')),

    re_path(r'^chat/$', ChatWithTeam.as_view(), name='team.chat'),
    url_tree(
        r'^(?P<username>[^\/]+)/',
        re_path(r'^approve/$', AddMember.as_view(), name='team.approve'),
        re_path(r'^remove/$', RemoveMember.as_view(), name='team.remove'),
        re_path(r'^disapprove/$', RemoveMember.as_view(), name='team.disapprove'),
    ),
]
