#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom 
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Team and User Alerts
"""

from django.utils.translation import gettext_lazy as _

from django.utils.translation import get_language
from django.db.models.signals import m2m_changed

from alerts.base import BaseAlert
from .models import User, Team, TeamMembership, TeamApproval

class MessageToTeam(BaseAlert):
    name = _("Team Admin Message")
    desc = _("The team admin has sent a message.")
    info = _("When a team admins needs to send a message to all the members of a team.")
    object_name = "Team '{{ object.team }}' admin message"

    signal = None
    sender = Team
    subject = "{% trans 'Team message:' %} {{ subject }}"
    email_subject = "{% trans 'Team message:' %} {{ subject }}"
    email_footer = None

    subscribe_all = False
    subscribe_any = False
    subscribe_own = True

    def get_alert_users(self, instance):
        """Returns either admin or a list of peers depending on enrollment"""
        self.users = [ship.user for ship in instance.members]
        return self.users

    def post_send(self, *_users, **__):
        # XXX This would count _users but it's empty because of a bug in boed alerts
        return len(self.users)

class MembershipAdded(BaseAlert):
    name = _("Membership Report")
    desc = _("A list of new users for this week")
    info = _("Each week a report is sent out to the administrator about new joins.")

    signal = None
    sender = TeamMembership
    subject = "{% trans 'Membership report:' %} {{ instance.team }}"
    email_subject = "Membership report: {{ instance.team }}"
    object_name = "Report for {{ instance.team }}"
    default_batch = 'W'
    related_name = 'reports'

    subscribe_all = False
    subscribe_any = False
    subscribe_own = True

    def get_alert_users(self, instance):
        email = instance.team.list_email()
        if email:
            (user, _) = User.objects.get_or_create(email=email, username=f"list.{instance.team.slug}")
            # This forces the settings to be saved to get around the broken batch emailer
            settings = self.get_alert_type().settings.for_user(user)
            settings.irc = not self.default_irc
            settings.save()
            # Return the user (which is always the same one)
            return [user]
        return []

class RequestToJoinAlert(BaseAlert):
    name = _("Request to Join Team")
    desc = _("A user has asked to join a team.")
    info = _("When a user requests to join a team, but the team requires approval, a notification is sent to admins.")

    signal        = None
    sender        = TeamMembership
    subject       = "{% trans 'Team request:' %} {{ instance.team }}"
    email_subject = "{% trans 'Team request:' %} {{ instance.team }}"
    object_name   = "Team '{{ object.team }}' join request"

    subscribe_all = False
    subscribe_any = False
    subscribe_own = True

    def show_settings_now(self, user):
        """
        Show settings if the user is an admin for any team or is a peer in a
        peer enrolement team.
        """
        if user.admin_teams.count() > 0:
            return True
        for membership in user.memberships.filter(team__enrole='P'):
            if membership.joined and not membership.expired:
                return True
        return False

    def get_alert_users(self, instance):
        """Returns either admin or a list of peers depending on enrollment"""
        # XXX We could send join requests to all peers, but that could get really bad
        return [instance.team.admin]


class InviteToJoinAlert(BaseAlert):
    name     = _("Invitiaton to Join Team")
    desc     = _("A team member has invited you to join a team.")
    info     = _("When a team member invites you to join a team you get a message.")

    signal        = None
    sender        = TeamApproval
    subject       = "{% trans 'Team invitation:' %} {{ instance.team }}"
    email_subject = "{% trans 'Team invitation:' %} {{ instance.team }}"
    object_name   = "Team '{{ object.team }}' inivitiation"

    subscribe_all = False
    subscribe_any = False
    subscribe_own = True

    def show_settings_now(self, user):
        """Always hide settings"""
        return False

    def get_alert_users(self, instance):
        """Returns either admin or a list of peers depending on enrollment"""
        return [instance.target_user]
