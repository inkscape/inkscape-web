from datetime import timedelta

import factory
from django.contrib.auth.models import Permission
from django.utils import timezone

import person.models


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = person.models.User
        django_get_or_create = ('username', )

    username = 'tester'  # Seems the most used, but we should change it when we change tests
    email = factory.Faker('email')
    is_active = True

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted is not None:
            self.groups.set(extracted or [])

    @factory.post_generation
    def joined_teams(self, create, extracted, **kwargs):
        if not create:
            return
        for team in extracted or []:
            TeamMembershipFactory(user=self, team=team, joined = timezone.now() - timedelta(days=1))

    @factory.post_generation
    def permissions(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            self.user_permissions.add(*Permission.objects.filter(codename__in=extracted))


class TeamMembershipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = person.models.TeamMembership

    user = factory.SubFactory(UserFactory)


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = person.models.Group
        django_get_or_create = ('name',)

    name = factory.Faker('word')


class TeamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = person.models.Team
        django_get_or_create = ('group',)

    group = factory.SubFactory(GroupFactory)
    admin = factory.SubFactory(UserFactory, username="team_admin", email="admin@inkscape.org",
                               groups=factory.LazyAttribute(lambda obj: [obj.factory_parent.factory_parent.group]))
    email = factory.Faker('email')
