#
# Copyright 2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Record changes in team membership in a git repository
"""

import os
import sys
import json
#from datetime import timedelta

from django.conf import settings
from django.utils.timezone import now
from django.core.management import BaseCommand
from copy import deepcopy

from person.models import User, Team, TeamMembership

try:
    from git import Repo, Actor
except ImportError:
    Repo = None

REPOSITORIES = getattr(settings, 'GIT_LOG_REPOSITORIES', {})

# Each git repository is expected to be set up by a systems administrator
#
# GIT_LOG_REPOSITORIES = {
#     'team_slug': {
#         'path': os.path.join(DATA_DIR, 'gitlog', 'team_dir'),
#         'push': True, # Should commits be pushed
#         'name': 'MEMBERS',
#     },
# }

class DirNotFound(IOError):
    pass

class GitNotFound(IOError):
    pass

DEFAULT_ACTOR = Actor("Inkscape Website", "webmaster@inkscape.org")
def to_actor(user, default=DEFAULT_ACTOR):
    if user is None:
        return default
    if not isinstance(user, User):
        raise ValueError("Not a user instance!")
    # We keep this anonymous to protect our user's address informatin
    return Actor(user.name, f"{user.username}@noreply.inkscape.org")

class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        if not Repo:
            sys.stderr.write("PythonGit is not installed, please install and try again.\n")
            return

        for slug, options in REPOSITORIES.items():
            try:
                self.log_team(Team.objects.get(slug=slug), **options)
            except Team.DoesNotExist:
                sys.stderr.write(f"Can not find team '{slug}'\n")
            except TypeError:
                raise
                sys.stderr.write(f"Missing some required arguments in {slug}: {options}\n")
            except DirNotFound as err:
                sys.stderr.write(f"Can't find path to git repository! {err}\n")
            except GitNotFound as err:
                sys.stderr.write(f"Path is not a git repository! {err}\n")

    def _pad(self, rows):
        return dict([(row['id'], (x, row)) for x, row in enumerate(rows)])

    def log_team(self, team, path, push=False, name='MEMBERS'):
        if not os.path.isdir(path):
            raise DirNotFound(path)

        self.repo = Repo(path)
        if self.repo.bare:
            raise GitNotFound(path)

        self.repo.index.reset()
        if len(self.repo.index.diff(None)) > 0:
            sys.stderr.write("You have uncommitted changes in the repository, failing\n")
            return

        filename = os.path.join(path, name)

        # 0. Load existing data for comparison
        previous = list(self.load_file(filename))
        p_pad = self._pad(previous)

        self.save_file(filename, previous)
        if len(self.repo.index.diff(None)) > 0:
            self.commit(filename, previous, "Refactoring file format or cleaning data.")

        # 1. Prepare new membership data
        current = list(self.generate_for(team))
        c_pad = self._pad(current)

        # 2. Commit any/all user details changes (change of name, change of username etc)
        mod = False
        for source in current:
            _, target = p_pad.get(source['id'], (None, None))
            if target is not None:
                for field in ['mid', 'user', 'name']:
                    if field not in source:
                        raise KeyError(f"Missing key in source row: {source}")
                    elif field not in target:
                        raise KeyError(f"Missing key in target row: {target}")
                    if source[field] != target[field]:
                        mod = True
                        target[field] = source[field]
        if mod:
            self.commit(filename, previous, "User details modified by website")

        # 3. Commit each removal as a single commit
        for pid in list(p_pad):
            if pid not in c_pad:
                row = previous.pop(p_pad[pid][0])
                self.commit(filename, previous, self.get_removed_message(row))
                p_pad = self._pad(previous)

        # 4. Commit each addition as a single commit
        for cid, (x, row) in c_pad.items():
            if cid not in p_pad:
                previous.append(row)
                self.commit(filename, previous, self.get_added_message(row))

        # 5. Optionally push the repository to a configured remote
        if push:
            self.repo.git.push()

    def commit(self, filename, data, message):
        if isinstance(message, str):
            message = {'author': DEFAULT_ACTOR, 'dt': now(), 'msg': message}

        self.save_file(filename, data)

        self.repo.index.add(filename)
        self.repo.index.commit(
            message['msg'],
            author=message['author'],
            author_date=message['dt'],
            committer=DEFAULT_ACTOR,
            commit_date=now())

    def get_removed_message(self, row):
        """Generate a delete message for this row"""
        try:
            membership = TeamMembership.objects.get(pk=row['mid'])
        except TeamMembership.DoesNotExist:
            return "Membership disapeared from database! (yikes!)"

        if not membership.expired:
            if not membership.joined:
                return "Membership unjoined from database! (stop it!)"
            return "Membership in uncertain state, removed for unknown reason!"

        author = to_actor(membership.removed_by)
        if membership.user == membership.removed_by:
            msg = "Remove myself"
        elif membership.removed_by:
            msg = f"Remove user '{membership.user.username}'"
        else:
            msg = f"Automatically expired '{membership.user.username}'"

        return {'author': author, 'dt': membership.expired, 'msg': msg}

    def get_added_message(self, row):
        try:
            membership = TeamMembership.objects.get(pk=row['mid'])
        except TeamMembership.DoesNotExist:
            raise # This is very bad.
        
        invite = membership.get_invitation()
        if invite:
            msg = f"Invited to join on {invite.added}\n"
        elif membership.requested:
            msg = f"Requested to join on {membership.requested}\n"
        else:
            msg = f"Added by administrator on {membership.joined}\n"

        apr_msg = ""
        for apr in membership.approvals.filter(expired__isnull=True):
            io = '-+'[apr.approves]
            apr_msg += f" {io} {apr.acting_user.username} {apr.added}\n"
        if apr_msg:
            msg += "\nApprovals:\n" + apr_msg

        return {'author': to_actor(membership.added_by), 'dt': membership.joined, 'msg': msg}

    def generate_for(self, team):
        """Generates a formatted output for the given team"""
        # Members are all people who are full joined and not expired
        return [
            {
                'id': membership.user_id,
                'mid': membership.pk,
                'user': membership.user.username,
                'name': membership.user.name,
            }
            for membership in team.members.order_by('pk')
        ]

    def load_file(self, filename):
        """Load the data format"""
        if os.path.isfile(filename):
            try:
                with open(filename, 'r', encoding='utf8') as fhl:
                    # Strip null added by save_file
                    for row in json.loads(fhl.read()):
                        if row is not None:
                            yield row
            except Exception:
                pass
        return []

    def save_file(self, filename, data):
        """Format the output for maximum git efficiency"""
        with open(filename, 'w', encoding='utf8') as fhl:
            # Writing each row out allows us to control it's packing
            fhl.write("[\n  ")
            for row in data:
                fhl.write(json.dumps(row, indent=None, sort_keys=True, ensure_ascii=False))
                fhl.write(",\n  ")
            fhl.write("null\n]")
            fhl.flush()

