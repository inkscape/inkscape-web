# -*- coding: utf-8 -*-
#
# Copyright 2014-2017, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Customise oauth models"""

from django.utils.translation import gettext_lazy as _

from social_core.backends.google import GoogleOAuth2
from social_core.exceptions import AuthCanceled

class AuthBlacklisted(AuthCanceled):
    def __str__(self):
        return str(_(f"Your account is not allowed to register here: {self.response}"))

from .models import EmailBlacklist

class FilteredGoogleOAuth2(GoogleOAuth2):
    """
    Google OAuth, but with blacklist email servers applied.
    """
    def get_user_details(self, response):
        """Filter by email address"""
        details = super().get_user_details(response)
        for item in EmailBlacklist.objects.all().blacklists(details['email']):
            raise AuthBlacklisted(self, response=item.reason)
        return details
