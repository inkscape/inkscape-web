# -*- coding: utf-8 -*-
#
# Copyright 2013, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Views for both user and group actions and views"""

from django_registration.backends.activation.views import ActivationView as AV, RegistrationView

from django.urls import re_path
from django.views.generic.base import TemplateView
from django.contrib.auth.views import (
    LogoutView,
    PasswordResetView as Reset, PasswordResetConfirmView as Confirm,
    PasswordResetCompleteView as Complete, PasswordResetDoneView as ResetDone,
)
from inkscape.url_utils import url_tree

from .views import LoginView, EditProfile, TeamList, MyProfile, AgreeToCla, Welcome, UserAvatar, GetMessages
from .forms import RegisForm, PasswordForm

AC = TemplateView.as_view(template_name='django_registration/activation_complete.html')
RC = TemplateView.as_view(template_name='django_registration/registration_complete.html')
RK = TemplateView.as_view(template_name='django_registration/registration_closed.html')
RG = RegistrationView.as_view(form_class=RegisForm)
PWDCONFIRM = r'^(?P<uidb64>[0-9A-Za-z_\-]+?)/(?P<token>.+)/$'

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^$', MyProfile.as_view(), name='my_profile'),
    re_path(r'^cla/$', AgreeToCla.as_view(), name='agree_to_cla'),
    re_path(r'^edit/$', EditProfile.as_view(), name='edit_profile'),
    re_path(r'^welcome/$', Welcome.as_view(), name='welcome'),
    re_path(r'^login/', LoginView.as_view(), name='auth_login'),
    re_path(r'^logout/', LogoutView.as_view(), name='auth_logout'),
    re_path(r'^teams/$', TeamList.as_view(), name='teams'),
    re_path(r'^messages.json$', GetMessages.as_view(), name='messages.json'),
    url_tree(
        r'^pwd/',
        re_path(r'^$', Reset.as_view(form_class=PasswordForm), name='password_reset'),
        re_path(PWDCONFIRM, Confirm.as_view(), name='password_reset_confirm'),
        re_path(r'^done/$', Complete.as_view(), name='password_reset_complete'),
        re_path(r'^sent/$', ResetDone.as_view(), name='password_reset_done'),
    ),

    url_tree(
        r'^register/',
        re_path(r'^$', RG, name='auth_register'),
        re_path(r'^closed/$', RK, name='django_registration_disallowed'),
        re_path(r'^complete/$', RC, name='django_registration_complete'),
        re_path(r'^activate/(?P<activation_key>[^\/]+)/$',\
            AV.as_view(), name='django_registration_activate'),
        re_path(r'^activated/$', AC, name='django_registration_activation_complete'),
    ),

    re_path(r'^avatar/(?P<nick>[^\/]+?)\.?(?P<ext>svg|png|jpe?g)?$', UserAvatar.as_view(), name='user_avatar'),
]
