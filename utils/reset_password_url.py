#
# Martin Owens 2020, AGPLv3
#
# pylint: disable=wrong-import-position
"""
Generate a reset url for a given user
"""

import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

try:
    import inkscape.manage # pylint: disable=unused-import
except ImportError as err:
    sys.stderr.write("Could not run script! Is manage.py not in the current"\
        "working directory, or is the environment not configured?:\n"\
        "{:s}\n".format(str(err)))
    sys.exit(1)

from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.urls import reverse

from django.contrib.sites.models import Site
from person.models import User

def get_url(username):
    """Get a user's reset password"""
    try:
        if '@' in username:
            user = User.objects.get(email=sys.argv[-1])
        else:
            user = User.objects.get(username=sys.argv[-1])
    except User.DoesNotExist:
        sys.stderr.write(f"Could not find user {username}")
        sys.exit(2)
    email = user.email
    uid = urlsafe_base64_encode(force_bytes(user.pk))

    current_site = Site.objects.get()
    domain = current_site.domain
    token = default_token_generator.make_token(user)

    url = reverse('password_reset_confirm', args=[uid, token])

    print(f"User: {user} ({email})")
    print(f"Reset Link: https://{ domain }{ url }")

if __name__ == '__main__':
    get_url(sys.argv[-1])
