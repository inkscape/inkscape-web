#
# Copyright 2021 Martin Owens
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Generate a list of reminders and cache them to a file.
"""

import os
import sys
import json

from datetime import datetime, timezone, UTC
from django.conf import settings
from django.utils.timezone import now
from django.core.management import BaseCommand as DjangoCommand

OUTPUT = sys.stderr
if hasattr(settings, 'ROCKET_LOG'):
    OUTPUT = open(settings.ROCKET_LOG, 'a')

REMINDER_FILE = None
if hasattr(settings, 'ROCKET_REMINDERS'):
    REMINDER_FILE = settings.ROCKET_REMINDERS

SENT_DIR = getattr(settings, 'ROCKET_SENT_DIR', None)

class DjangoJSONDecoder(json.JSONDecoder):
    """Parses out items encoded by DjangoJSONEncoder"""
    def decode(self, string):
        return self._parse(super().decode(string))

    def _parse(self, item):
        if isinstance(item, list):
            return [self._parse(item) for item in item]
        elif isinstance(item, dict):
            return dict([(name, self._parse(value)) for name, value in item.items()])
        elif isinstance(item, str):
            try:
                return datetime.strptime(item, '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=UTC)
            except Exception:
                pass
        return item

class BaseCommand(DjangoCommand):
    help = __doc__
    error = staticmethod(lambda msg: Command.msg(msg, "!"))
    warn = staticmethod(lambda msg: Command.msg(msg, "*"))

    def event_dir_for(self, event, *args):
        if not SENT_DIR:
            raise IOError("No sent directory set!", "!")
        this_dir = os.path.join(SENT_DIR, str(event.pk))
        if not os.path.isdir(this_dir):
            os.makedirs(this_dir)
        return os.path.join(this_dir, *args)

    def sent_file_for(self, event, dt):
        return self.event_dir_for(event, dt.isoformat() + '.sent')

    def next_file_for(self, event):
        return self.event_dir_for(event, 'next.log')

    @staticmethod
    def msg(msg, code=None):
        dt = now()
        if code and msg:
            OUTPUT.write(f" [{code}] {dt} -- {msg}\n")
        elif msg:
            OUTPUT.write(f"{msg}\n")
        OUTPUT.flush()

    def handle(self, *args, **options):
        if not REMINDER_FILE:
            self.msg("No reminders file!", '!')
            return
        remdir = os.path.dirname(REMINDER_FILE)
        if not os.path.isdir(remdir):
            os.makedirs(remdir)
        try:
            self.function()
        except Exception as err:
            self.msg(f"Couldn't process: {err}", '!')
            raise

    def get_reminders(self):
        if os.path.isfile(REMINDER_FILE):
            with open(REMINDER_FILE, 'r') as fhl:
                try:
                    return json.loads(fhl.read(), cls=DjangoJSONDecoder)
                except:
                    pass
        return {}
