#
# Copyright 2014, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Main website urls, from here all other things sprong"""

from django.urls import include, re_path
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.contrib import admin

try:
    import debug_toolbar
except ImportError:
    debug_toolbar = None

from .views import ContactUs, ContactOk, SearchView, SearchJson, Authors

handler404 = 'inkscape.views.catch_bad_language'
handler500 = 'inkscape.views.server_error'

urlpatterns = [ # pylint: disable=invalid-name
    re_path(r'^social/', include('social_django.urls', namespace='social')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
  + static('/dl/', document_root=settings.MEDIA_ROOT)

if settings.ENABLE_DEBUG_TOOLBAR and debug_toolbar:
    urlpatterns.append(
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    )

urlpatterns += i18n_patterns(
    re_path(r'^contact/us/$', ContactUs.as_view(), name='contact'),
    re_path(r'^contact/ok/$', ContactOk.as_view(), name='contact.ok'),
    re_path(r'^search/$', SearchView(), name='search'),
    re_path(r'^search/json/$', SearchJson(), name='search.json'),
    re_path(r'^credits/$', Authors.as_view(), name='authors'),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^cog/', include('cog.urls')),
    re_path(r'^forums/', include('forums.urls')),
    re_path(r'^cals/', include('calendars.urls')),
    re_path(r'^budget/', include('budget.urls')),
    re_path(r'^releases?/', include('releases.urls')),
    re_path(r'^alerts/', include('alerts.urls')),
    re_path(r'^comments/', include('django_comments.urls')),
    re_path(r'^moderation/', include('moderation.urls')),
    re_path(r'captcha/', include('captcha.urls')),
    re_path(r'^news/', include('cmsplugin_news.urls')),
    re_path(r'^diff/', include('cmsplugin_diff.urls')),
    re_path(r'^doc/', include('docs.urls')),
    re_path(r'^~(?P<username>[^\/]+)/', include('person.user_urls')),
    re_path(r'^\*(?P<team>[^\/]+)/', include('person.team_urls')),
    re_path(r'^user/', include('person.urls')),
    re_path(r'^', include('resources.urls')),
    # This URL is Very GREEDY, it must go last!
    re_path(r'^', include('cms.urls')),
    prefix_default_language=False,
)
