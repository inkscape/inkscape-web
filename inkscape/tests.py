#
# Copyright 2016, Maren Hachmann <marenhachmann@yahoo.com>
#                 Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test breadcrumbs, caching keys and other core inkscape website functions.
"""

def reboot_alerts():
    """This patch undoes the alert init and redoes it."""
    from django.apps import apps
    from alerts.base import BaseAlert
    for item in BaseAlert.instances.values():
        if hasattr(item.sender, item.related_name):
             delattr(item.sender, item.related_name)
        type(item).slug = None
    apps.get_app_config('alerts').ready()
